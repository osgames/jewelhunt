//
// This file is part of the Shopkeeper game 
// in the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package shopkeeper;

import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.Clock;
import shopkeeper.ui.ShopFrame;

/**
 * Main class to run the shopkeeper game.
 * 
 * @author Hj. Malthaner
 */
public class Main 
{
    public static void main(String [] args)
    {
        try 
        {
            Shop shop = new Shop();
            ShopFrame shopFrame = new ShopFrame(shop);
            
            Clock.init(System.currentTimeMillis());
                    
            Thread clockThread = new Thread()
            {
                @Override
                public void run()
                {
                    while(true)
                    {
                        try {
                            sleep(100);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Clock.update(System.currentTimeMillis());
                    }
                }
            };

            clockThread.setDaemon(true);
            clockThread.start();
            shopFrame.show();
        }
        catch (Exception ex) 
        {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
}
