//
// This file is part of the Shopkeeper game 
// in the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package shopkeeper;

import java.util.ArrayList;
import java.util.Random;
import jewelhunt.game.item.Item;
import jewelhunt.game.item.ItemFactory;
import jewelhunt.game.player.Player;
import rlgamekit.objects.ArrayRegistry;
import rlgamekit.objects.Registry;

/**
 * All shop data
 * 
 * @author Hj. Malthaner
 */
public class Shop 
{
    private final ItemFactory factory;
    private final Registry <Item> allItems;
    public final ArrayList<Player> customers;
    public final Random rng = new Random();
    
    public Shop()
    {
        factory = new ItemFactory();
        customers = new ArrayList<Player>();
        allItems = new ArrayRegistry<Item>(1024);
                
        for(int i=0; i<5; i++)
        {
            Player customer = CustomerFactory.makeCustomer(factory, allItems, rng.nextInt(5));
            customers.add(customer);
        }
    }
}
