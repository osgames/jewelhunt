//
// This file is part of the Shopkeeper game 
// in the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package shopkeeper;

import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.item.ItemFactory;
import jewelhunt.game.player.Player;
import rlgamekit.objects.Registry;

/**
 * This factory will create players with items, so that they can be 
 * used as customers for the shop.
 * 
 * @author Hj. Malthaner
 */
public class CustomerFactory 
{
    public static Player makeCustomer(ItemFactory factory, Registry<Item> allItems, int level) 
    {
        Player customer = new Player(null, 0, 0, 0, null, null, 15, null);
        
        for(int i=0; i<12; i++)
        {
            Item item = factory.createRandomItem(allItems);
            if(item == null)
            {
                System.err.println("Got null item from factory.");
            }
            else
            {
                customer.inventory.add(new InventoryItem(item));
            }
        }
        
        return customer;
    }
}
