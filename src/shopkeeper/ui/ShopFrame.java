//
// This file is part of the Shopkeeper game 
// in the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package shopkeeper.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import jewelhunt.game.TextureCache;
import jewelhunt.game.player.Player;
import shopkeeper.Shop;

/**
 * Main frame to keep all of the shops UI panels.
 * 
 * @author Hj. Malthaner
 */
public class ShopFrame 
{
    private final JFrame frame;
    
    private final ShopPanel shopPanel;
    private final CustomerPanel customerPanel;
    
    private final TextureCache textureCache;
    private final StatusPanel statusPanel;
    
    public ShopFrame(Shop shop) throws IOException
    {
        frame = new JFrame("Shopkeeper Demo r002");        
        frame.getContentPane().setPreferredSize(new Dimension(1200, 700));
        frame.pack();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation((screenSize.width - frame.getWidth()) / 2, (screenSize.height - frame.getHeight()) / 2);
        
        textureCache = new TextureCache();
        textureCache.initialize(null);

        customerPanel = new CustomerPanel(this, textureCache);
        shopPanel = new ShopPanel(shop, this, textureCache);
        statusPanel = new StatusPanel();
        
        frame.getContentPane().setLayout(new BorderLayout());
        
        showPanel(shopPanel);
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void show() 
    {
        frame.setVisible(true);
    }

    void showCustomer(Player customer) 
    {
        customerPanel.setCustomer(customer);
        showPanel(customerPanel);
    }
    
    void showShop() 
    {
        showPanel(shopPanel);
    }
    
    private void showPanel(JPanel panel)
    {
        Container contentPane = frame.getContentPane();
        contentPane.removeAll();
        contentPane.add(panel, BorderLayout.CENTER);
        contentPane.add(statusPanel, BorderLayout.SOUTH);
        contentPane.validate();
        contentPane.repaint();
    }
}
