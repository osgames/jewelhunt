//
// This file is part of the Shopkeeper game 
// in the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package shopkeeper.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import jewelhunt.game.Texture;
import jewelhunt.game.TextureCache;
import jewelhunt.game.item.Item;
import jewelhunt.game.item.ItemInfoAssembly;

/**
 *
 * @author Hj. Malthaner
 */
public class ItemListCellRenderer extends JLabel implements ListCellRenderer<Object>
{
    private final TextureCache textureCache;

    ItemListCellRenderer(TextureCache textureCache) 
    {
        this.textureCache = textureCache;
        
        setFont(new Font(Font.SERIF, Font.PLAIN, 14));
        setOpaque(true);
        setBorder(new EmptyBorder(4, 4, 8, 4));
    }
    
    @Override
    public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus) 
    {
        Item item = (Item)value;
        int imageIndex = item.getInventoryImageIndex();
        Texture tex = textureCache.textures[imageIndex];
        
        if(tex == null)
        {
            System.err.println("Error! Item '" + item.countAndName() + "' has no image assigned!");
            System.err.println("imageIndex = " + imageIndex);
            System.exit(20);
        }
        
        BufferedImage image = new BufferedImage(128, tex.image.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics gr = image.getGraphics();
        gr.drawImage(tex.image, (128 - tex.image.getWidth()) / 2, 0, null);
        
        setText(makeItemText(item));
        setIcon(new ImageIcon(image));

         Color background;
         Color foreground;

         // check if this cell represents the current DnD drop location
         JList.DropLocation dropLocation = list.getDropLocation();
         if (dropLocation != null
                 && !dropLocation.isInsert()
                 && dropLocation.getIndex() == index) {

             background = Color.BLUE;
             foreground = Color.WHITE;

         // check if this cell is selected
         } else if (isSelected) {
             background = Color.LIGHT_GRAY;
             foreground = Color.BLACK;

         // unselected, and not the DnD drop location
         } else {
             background = Color.DARK_GRAY;
             foreground = Color.WHITE;
         }

         setBackground(background);
         setForeground(foreground);

         return this;        
    }
    
    private String makeItemText(Item item)
    {
        String result = "<html>";
        
        String [] lines = new String [32]; 
        int [] colors = new int [32]; 
        int lineCount = 0;
        int modCount = 0;

        int defaultColor = 0xDDEEFF;
        int stateColor = 
            item.calcItemState() <= 0.5 ? 0xFF7700 : 
                item.calcItemState() < 1.0 ? 0xDDDD44 : 
                    defaultColor;


        int [] lineRef = new int[] {lineCount, modCount};

        ItemInfoAssembly.fillDefaultItemInfo(item, lines, colors, lineRef, defaultColor, stateColor);

        result += item.countAndName() + "<br>";

        for(int line = 0; line < lineRef[0]; line++)
        {
            result += lines[line] + "<br>";
        }
        
        /*
        for(int line = 0; line < lineRef[1]; line++)
        {
            result += lines[line];
        }
        */
        
        return result + "</html>";
    }
    
}
