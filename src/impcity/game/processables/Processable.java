package impcity.game.processables;

import jewelhunt.game.map.Map;

/**
 *
 * @author Hj. Malthaner
 */
public interface Processable 
{
    public void process(Map map);    
}
