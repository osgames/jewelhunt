package impcity.game.ui;

import impcity.game.ImpCity;
import impcity.game.Tools;
import impcity.game.KeeperStats;
import java.io.IOException;
import java.util.ArrayList;
import jewelhunt.game.Clock;
import jewelhunt.game.Texture;
import jewelhunt.game.TextureCache;
import jewelhunt.ogl.GlTextureCache;
import jewelhunt.game.player.Player;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.ui.PixFont;
import jewelhunt.ui.TimedMessage;
import org.lwjgl.input.Mouse;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;

/**
 *
 * @author Hj. Malthaner
 */
public class GameDisplay
{
    private final static int selectedButtonColor = 0xFFFFDD77;
    
    private final Texture buttonBar;
    private final ImpCity game;
    private final PixFont font;

    private final Texture buttonText;
    private final Texture buttonDig;
    private final Texture buttonLair;
    private final Texture buttonFood;
    private final Texture buttonBook;
    private final Texture buttonTreasury;
    private final Texture buttonForge;
    private final Texture buttonWork;
    private final Texture buttonHeal;
    private final Texture buttonDemolish;
    private final Texture buttonImp;
    
    public static final int TAB_ROOMS_I = 1;
    public static final int TAB_ROOMS_II = 2;
    public static final int TAB_SPELLS = 3;
    
    private int tabSelected = TAB_ROOMS_I;
    
    private final ArrayList<TimedMessage> messages = new ArrayList<TimedMessage>();
    private final ArrayList<MessageHook> hookedMessageStack = new ArrayList<MessageHook>();
    private final IsoDisplay display;
    
    public UiDialog topDialog;
    
    
    
    public GameDisplay(ImpCity game, IsoDisplay display) throws IOException
    {
        this.game = game;
        this.display = display;
        this.font = display.font;
        
        TextureCache textureCache = display.textureCache;
        
        buttonBar = textureCache.loadTexture("/impcity/resources/ui/main_bar_bg.jpg", false);
        buttonDig = textureCache.loadTexture("/impcity/resources/ui/button_dig.png", true);
        buttonLair = textureCache.loadTexture("/impcity/resources/ui/button_lair.png", true);
        buttonFood = textureCache.loadTexture("/impcity/resources/ui/button_food.png", true);
        buttonBook = textureCache.loadTexture("/impcity/resources/ui/button_library.png", true);
        buttonTreasury = textureCache.loadTexture("/impcity/resources/ui/button_treasury.png", true);
        buttonForge = textureCache.loadTexture("/impcity/resources/ui/button_forge.png", true);
        buttonWork = textureCache.loadTexture("/impcity/resources/ui/button_work.png", true);
        buttonHeal = textureCache.loadTexture("/impcity/resources/ui/button_healing.png", true);
        buttonDemolish = textureCache.loadTexture("/impcity/resources/ui/button_demolish.png", true);
        buttonImp = textureCache.loadTexture("/impcity/resources/ui/button_imp.png", true);
        
        buttonText = textureCache.loadTexture("/impcity/resources/ui/button_text_short.png", true);
      
    }    
    
    public void addMessage(TimedMessage message)
    {
        messages.add(message);
    }

    public void selectTab(int tab)
    {
        tabSelected = tab;
    }
    
    public int getSelectedTab()
    {
        return tabSelected;
    }
    
    public void displayMore()
    {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        int left = calcMainUiBarLeft();
        
        IsoDisplay.drawTile(buttonBar, left, 0);
        
        IsoDisplay.drawTile(buttonText, left + 14, 14, tabSelected == -1 ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonText, left + 14, 37, tabSelected == -1 ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonText, left + 14, 60, tabSelected == TAB_SPELLS ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonText, left + 14, 83, tabSelected == TAB_ROOMS_I ? selectedButtonColor : 0xFFFFFFFF);
        
        font.drawStringScaled("Rooms I", tabSelected == TAB_ROOMS_I ? 0xFFFFFF : 0, left+60, 85, 0.5);
        font.drawStringScaled("Rooms II", tabSelected == TAB_SPELLS ? 0xFFFFFF : 0, left+64, 62, 0.5);
        font.drawStringScaled("Spells", tabSelected == TAB_SPELLS ? 0xFFFFFF : 0, left+64, 39, 0.5);
        font.drawStringScaled("?????", 0x000000, left+60, 16, 0.5);
        
        
        if(tabSelected == TAB_ROOMS_I)
        {
            displayRooms1Tab(left);
        }
        else if(tabSelected == TAB_ROOMS_II)
        {
            displayRooms2Tab(left);
        }
        else if(tabSelected == TAB_SPELLS)
        {
            displaySpellsTab(left);
        }
            

        // debug
        // font.drawStringScaled("Mouse: " + display.cursorI + "," + display.cursorJ, 0xFFFFFF, 20, 600, 0.5);

        
        font.drawStringScaled("Time: 00/" + twoDigits(Clock.days()) + "/" + twoDigits(Clock.hour()), 0xFFFFFF, 10, 100, 0.5);
        
        Player keeper = game.world.mobs.get(game.getPlayerKey());

        font.drawStringScaled("Lvl: Rookie", 0xDDD0C0, 20, 700, 0.6);
        font.drawStringScaled("Exp: " + keeper.stats.getCurrent(KeeperStats.EXPERIENCE), 0xFFF0E0, 20, 675, 0.6);
        font.drawStringScaled("Rep: " + calcReputationDisplay(keeper), 0xDDD0C0, 20, 650, 0.6);
        
        if(topDialog != null)
        {
            topDialog.display((display.displayWidth - topDialog.width) / 2, 
                              (display.displayHeight - topDialog.height)/ 2);
        }
        
        
        int yoff;
        
        for(TimedMessage message : messages)
        {
            yoff = 8 + (((int)(Clock.time() - message.time)) >> 3);
            font.drawStringScaled(message.message, message.color, message.x, message.y + yoff, 1.0 + yoff/120.0);

            if(yoff > 100)
            {
                message.message = null;
                message.time = 0;
            }
        }
        
        purgeOutdatedMessages();
        
        yoff = 8;
        
        for(MessageHook hook : hookedMessageStack)
        {
            int y = yoff;
            
            // Hajo: Is this message still falling?
            if(hook.yoff > y)
            {
                y = hook.yoff;
                hook.yoff -= 2;
            }
            else
            {
                // nope, it already sits
                // -> raise stack level.
                yoff += 32;
            }
            
            IsoDisplay.drawTile(display.textureCache.textures[hook.icon], display.displayWidth - 40, y);
            
        }
    }

    public int calcMainUiBarLeft()
    {
        return (display.displayWidth-buttonBar.image.getWidth())/2;
    }
    
    private void purgeOutdatedMessages() 
    {
        for(int i=messages.size()-1; i >=0; i--)
        {
            TimedMessage message = messages.get(i);
            if(message.message == null || message.time == 0)
            {
                messages.remove(i);
            }
        }
    }

    private void displayRooms1Tab(int left)
    {
        IsoDisplay.drawTile(buttonDig, left + 196, 16, Tools.selected == Tools.MARK_DIG ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonLair, left + 280, 16, Tools.selected == Tools.MAKE_LAIR ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonFood, left + 364, 16, Tools.selected == Tools.MAKE_FARM ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonBook, left + 448, 16, Tools.selected == Tools.MAKE_LIBRARY ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonWork, left + 532, 16, Tools.selected == Tools.MAKE_TREASURY ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonForge, left + 616, 16, Tools.selected == Tools.MAKE_FORGE ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonHeal, left + 700, 16, Tools.selected == Tools.MAKE_HOSPITAL ? selectedButtonColor : 0xFFFFFFFF);

        IsoDisplay.drawTile(buttonDemolish, left + 824, 16, Tools.selected == Tools.DEMOLISH ? selectedButtonColor : 0xFFFFFFFF);
        
        // Hajo: testing tooltips
        if(Mouse.getY() < 100)
        {
            int x = Mouse.getX();
            
            if(x > left + 196 && x < left + 196 + 80)
            {
                font.drawString("Mark a block for digging", 0xFFFFFFFF, left + 196 - 120, 124);
            }
            else if(x > left + 280 && x < left + 280 + 80)
            {
                font.drawString("Build lair space for your creatures", 0xFFFFFFFF, left + 280 - 170, 124);
            }
            else if(x > left + 364 && x < left + 364 + 80)
            {
                font.drawString("Convert floor to farmland", 0xFFFFFFFF, left + 364 - 130, 124);
            }
            else if(x > left + 448 && x < left + 448 + 80)
            {
                font.drawString("Set up a library", 0xFFFFFFFF, left + 448 - 70, 124);
            }
            else if(x > left + 532 && x < left + 532 + 80)
            {
                font.drawString("Make a workshop", 0xFFFFFFFF, left + 532 - 90, 124);
            }
            else if(x > left + 616 && x < left + 616 + 80)
            {
                font.drawString("Create a forge", 0xFFFFFFFF, left + 616 - 70, 124);
            }
            else if(x > left + 700 && x < left + 700 + 80)
            {
                font.drawString("Place a healing well", 0xFFFFFFFF, left + 700 - 110, 124);
            }
            else if(x > left + 824 && x < left + 824 + 80)
            {
                font.drawString("Revert a room to empty space", 0xFFFFFFFF, left + 824 - 210, 124);
            }
        }
    }
    
    private void displayRooms2Tab(int left)
    {
        IsoDisplay.drawTile(buttonDig, left + 196, 16, Tools.selected == Tools.MARK_DIG ? selectedButtonColor : 0xFFFFFFFF);
        IsoDisplay.drawTile(buttonTreasury, left + 532, 16, Tools.selected == Tools.MAKE_TREASURY ? selectedButtonColor : 0xFFFFFFFF);

        IsoDisplay.drawTile(buttonDemolish, left + 824, 16, Tools.selected == Tools.DEMOLISH ? selectedButtonColor : 0xFFFFFFFF);
        
        // Hajo: testing tooltips
        if(Mouse.getY() < 100)
        {
            int x = Mouse.getX();
            
            if(x > left + 196 && x < left + 196 + 80)
            {
                font.drawString("Mark a block for digging", 0xFFFFFFFF, left + 196 - 120, 124);
            }
            else if(x > left + 532 && x < left + 532 + 80)
            {
                font.drawString("Make a storage room", 0xFFFFFFFF, left + 532 - 90, 124);
            }
            else if(x > left + 824 && x < left + 824 + 80)
            {
                font.drawString("Revert a room to empty space", 0xFFFFFFFF, left + 824 - 210, 124);
            }
        }
    }

    private void displaySpellsTab(int left)
    {
        IsoDisplay.drawTile(buttonImp, left + 196, 16, Tools.selected == Tools.SPELL_IMP ? selectedButtonColor : 0xFFFFFFFF);

        // Hajo: testing tooltips
        if(Mouse.getY() < 100)
        {
            int x = Mouse.getX();
            
            if(x > left + 196 && x < left + 196 + 80)
            {
                font.drawString("Spawn a new imp", 0xFFFFFFFF, left + 196 - 120, 124);
            }
        }
    }
    
    private String twoDigits(int v)
    {
        if(v > 9)
        {
            return "" + v;
        }
        else
        {
            return "0" + v;
        }
    }

    private String calcReputationDisplay(Player keeper)
    {
        int rep = keeper.stats.getCurrent(KeeperStats.REPUTATION);
        
        String base;
        
        if(rep > 500)
        {
            base = "Outstanding";
        }
        else if(rep > 300)
        {
            base = "Excellent";
        }
        else if(rep > 100)
        {
            base = "Good";
        }
        else if(rep > -100)
        {
            base = "Fair";
        }
        else if(rep > -300)
        {
            base = "Poor";
        }
        else if(rep > -500)
        {
            base = "Bad";
        }
        else if(rep > -700)
        {
            base = "Very Bad";
        }
        else
        {
            base = "Rock bottom";
        }
        
        return base + " (" + rep + ")";
    }

    private static String calcLevelString(int level, int maxLevel)
    {
        String [] names = 
        {
"Rookie",
"Cave Digger",
"Flea Herder",

"Dungeon Leader",
"Creature Master"  
        };

        double f = (double)level/(double)maxLevel;
        
        return names[(int)(f * names.length)];
    
    }
    
    public final synchronized void showDialog(UiDialog dialog)
    {
        topDialog = dialog;
    }

    public void addHookedMessage(MessageHook hookedMessage) 
    {
        // Hajo: calaculate starting height - at least above the screen,
        // but with some distance to the highest former message
        
        int height = display.displayHeight;
        
        if(!hookedMessageStack.isEmpty())
        {
            int top = hookedMessageStack.get(hookedMessageStack.size()-1).yoff + 40;
            if(top > height) height = top;
        }

        hookedMessage.yoff = height;
        hookedMessageStack.add(hookedMessage);
    }

    void activateHookedMessage(int n) 
    {
        if(n >= 0 && n < hookedMessageStack.size())
        {
            MessageHook hookedMessage = hookedMessageStack.get(n);
            hookedMessageStack.remove(n);
            hookedMessage.activate(this);
        }
    }

}
