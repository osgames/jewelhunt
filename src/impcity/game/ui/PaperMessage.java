package impcity.game.ui;

import java.io.IOException;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.ui.PixFont;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Hj. Malthaner
 */
public abstract class PaperMessage extends UiDialog
{
    protected final PixFont font;
    protected String message;
    protected String leftButton;
    protected String rightButton;
    protected String headline;
    
    protected int messageYOffset = 0;
    
    public PaperMessage(IsoDisplay display, int width, int height,
            String headline,
            String message, String leftButton, String rightButton) throws IOException
    {
        super(display.textureCache, width, height);
        this.font = display.font;
        this.headline = headline;
        this.message = message;
        this.leftButton = leftButton;
        this.rightButton = rightButton;
    }
    
    @Override
    public void display(int x, int y)
    {
        super.display(x, y);
        
        int headlineWidth = (int)(font.getStringWidth(headline) * 0.8);
        font.drawStringScaled(headline, 0x203040, x + (width - headlineWidth) / 2, y + height - 70, 0.8);
        
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        GL11.glScissor(x+40, y+110, width-80, height-200);

        font.drawText(message, 0x403020, x+40, y+height - 120 + messageYOffset, width - 80, 0.6);
        
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
        
        font.drawStringScaled(leftButton, 0x203040, x + 40, y +60, 0.6);
        
        int rwidth = (int)(font.getStringWidth(rightButton) * 0.6);
        font.drawStringScaled(rightButton, 0x203040, x + width - 40 - rwidth, y + 60, 0.6);
    }
}
