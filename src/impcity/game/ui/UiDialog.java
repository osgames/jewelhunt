package impcity.game.ui;

import java.io.IOException;
import jewelhunt.game.Texture;
import jewelhunt.game.TextureCache;
import jewelhunt.ogl.GlTextureCache;
import jewelhunt.ogl.IsoDisplay;

/**
 *
 * @author Hj. Malthaner
 */
public abstract class UiDialog
{
    private static Texture messagePaperBg;

    public static final int COL_BLUE_INK = 0x203040;
    public static final int COL_RED_INK = 0x403020;
    
    public final int width;
    public final int height;
    
    public UiDialog(TextureCache textureCache, int width, int height) throws IOException
    {
        this.width = width;
        this.height = height;

        if(messagePaperBg == null)
        {
            messagePaperBg = textureCache.loadTexture("/impcity/resources/ui/paper_bg.png", true);
        }
    }
    
    public void display(int x, int y)
    {
        // IsoDisplay.drawTile(messagePaperBg, x, y, width, height, 0xFFFFFFFF);
        IsoDisplay.drawTile(messagePaperBg, x, y, width, height, 0xFFDDDDDD);
    }

    public abstract void mouseEvent(int buttonPressed, int buttonReleased, int mouseX, int mouseY);
}
