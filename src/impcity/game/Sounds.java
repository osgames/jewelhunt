package impcity.game;

/**
 * Sound sample indexes as constants.
 * 
 * @author Hj. Malthaner
 */
public class Sounds
{
    public static final int UI_BUTTON_CLICK = 0;
    public static final int MARK_DIG = 1;
    public static final int CLAIM_SQUARE = 2;
    public static final int MAKE_FARMLAND = 3;
    public static final int MAKE_LIBRARY = 4;
    public static final int UI_DESELECT = 5;
    public static final int CREATURE_ARRIVAL = 6;
    public static final int MAKE_TREASURY = 7;
    public static final int MAKE_LAIR = 8;
    public static final int MAKE_WORKSHOP = 9;
}
