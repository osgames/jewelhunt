package impcity.game.quests;

/**
 * Only treasure maps so far.
 * @author Hj. Malthaner
 */
public class QuestGenerator
{
    
    private static String [] mapQualities = 
    {
        "precise", 
        "tattered", 
        "sketchy",
        "complicated",
        "fretted",
        "faded",
        "fragmented",
        "partly burned",
        "eroded",
        "parts of a",
        "largely unintelligible",
        "lightly encrypted", 
        "heavily encrypted", 
    };
    
    private static int [] mapQualityFindDifficulties = 
    {
        1, 
        2, 
        3, 
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
    };
    
    private static String [] areaLocations = 
    {
        "in {0} great plain",
        "in {0} grassy plain",
        "in {0} valley",
        "in {0} bushland",
        "in {0} river bend",
        "in {0} lake",
        "in {0} cave",
        "in {0} hole",
        "in {0} gorge",
        "in {0} forest",
        "in {0} desert",
        "in {0} inactive volcano",
        "in {0} hollow tree trunk",
        "in {0} hilltop",
        "under {0} huge oak tree",
        "under {0} huge linden tree",
        "under {0} huge ash tree",
        "under {0} sacred fig tree",
        "under {0} statue",
        "at the foot of {0} massive crag",
        "near {0} scare crow",
        "near {0} obelisk",
        "near {0} ancient tree trunk",
    };
    
    private static String [] buildingLocations =
    {
        "tower",
        "temple",
        "palace",
        "stronghold",
        "fortress",
        "maze",
        "henge",
        "cemetery",
        "citadel",
        "grave",
        "tomb",
        "pyramid",
        "fountain",
        "magazin",
        "storehouse",
    };
        
      
    private static String [] buildingMods = 
    {
        "in {0}",
        "in {0} ruined",
        "in {0} old",
        "in {0} disused",
        "in {0} run down",
        "in {0} dark",
        "in {0} shining",
        "in {0} holy",
        "in {0} unholy",
        "in {0} abandoned",
        "in {0} cursed",
        "in {0} guarded",
    };
    
    private static String [] treasureMods = 
    {
        "promising",
        "big",
        "huge",
        "outstanding",
        "abundant"
    };

    private static int [] treasureModsSize = 
    {
        1, 
        2, 
        3, 
        4,
        5,
    };

    private static String [] distances =
    {
        "Your creatures can reach the place within some days.",
        "It'll probably take several days, maybe a week, to get there and back.",
        "Quite some journey, but within a few weeks your minions should be able to get there and back.",
        "It'll be a several months long ride into mostly unkown territory.",
        "The distance makes you wonder though, if you'll be still alive when your expedition returns.",
    };
    
    
    public static Quest makeTreasureQuest()
    {
        String [] intros =
        {
            "Your creatures discovered a",
            "Hard working creatures of yours found a",
            "Your creatures found a",
            "A group of your creatures deciphered a",
            "Your minions discovered a",
            "A minion of yours found a",
            "Your minions deciphered a",
            "Your librarians stumbled upon a",
            "Your library workers encountered a",
            "A freelancer, sub-hired by a lazy minion of yours, brought up a",
        };

        String [] tellVars =
        {
            "tells of",
            "describes",
            "unveils",
            "shows the location of",
        };
        
        Quest quest = new Quest();
        quest.seed = System.currentTimeMillis();
        
        StringBuilder text = new StringBuilder();
        int n;

        n = (int)(intros.length * Math.random());
        text.append(intros[n]);
        text.append(' ');
                
        n = (int)(mapQualities.length * Math.random());
        text.append(mapQualities[n]);
        quest.findingDifficulty = mapQualityFindDifficulties[n];

        text.append( " treasure map. The map ");
        
        n = (int)(tellVars.length * Math.random());
        text.append(tellVars[n]);
        text.append(' ');

        
        n = (int)(treasureMods.length * Math.random());
        text.append(treasureMods[n]);
        quest.treasureSize = treasureModsSize[n];
        text.append(" riches to be found ");

        boolean namedLocation = (Math.random() < 0.5);
        StringBuilder locationName = new StringBuilder();
        
        if(Math.random() < 0.5)
        {
            n = (int)(areaLocations.length * Math.random());
            locationName.append(areaLocations[n]);
            quest.locationIsBuilding = false;
        }
        else
        {        
            n = (int)(buildingMods.length * Math.random());
            locationName.append(buildingMods[n]);
            locationName.append(' ');

            n = (int)(buildingLocations.length * Math.random());
            locationName.append(buildingLocations[n]);
            
            quest.locationIsBuilding = true;
        }        
                
        int p = locationName.indexOf("{0}");
        if(namedLocation)
        {
            locationName.replace(p, p+3, "the");
            locationName.append(" of ");
            locationName.append(NameGenerator.makeLocationName(2, 5));
        }
        else
        {
            locationName.replace(p, p+3, "a");
        }
        quest.locationName = locationName.substring(p);

        text.append(locationName);
        text.append(". ");
        
        
        
        n = (int)(distances.length * Math.random());
        text.append(distances[n]);

        n += 2;
        quest.travelTime = 2 + n*n;
        
        quest.story = text.toString();
        
        return quest;
    }
 
    public static Quest makeTechnologyQuest()
    {
        String [] intros =
        {
            "Your creatures suggest",
            "Hard working creatures of yours think that",
            "A group of your creatures came up with the idea",
        };

        Quest quest = new Quest();
        quest.seed = System.currentTimeMillis();
        
        StringBuilder text = new StringBuilder();
        int n;

        n = (int)(intros.length * Math.random());
        text.append(intros[n]);

        text.append( " to research metallurgy. They ask you ");
        text.append(" to assemble a party and spy on the smiths of ");

        String locationName = NameGenerator.makeLocationName(2, 5);
        quest.locationName = locationName;

        text.append(locationName);
        text.append(" to learn bronze working. ");
        
        n = (int)(distances.length * Math.random());
        text.append(distances[n]);

        n += 2;
        quest.travelTime = 2 + n*n;
        quest.findingDifficulty = 1;
        quest.treasureSize = 1; // Todo: Metallurgy level should go here
        
        quest.story = text.toString();
        
        return quest;
    }
}
