package impcity.game;

/**
 * 
 * @author Hj. Malthaner
 */
public enum Tools 
{
    MARK_DIG,
    MAKE_LAIR,
    MAKE_FARM,
    MAKE_LIBRARY,
    MAKE_FORGE,
    MAKE_WORKSHOP,
    MAKE_HOSPITAL,
    MAKE_TREASURY,
    DEMOLISH,
    
    SPELL_IMP;

    public static Tools selected;
}
