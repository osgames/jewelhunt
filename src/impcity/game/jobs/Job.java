/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package impcity.game.jobs;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import jewelhunt.game.player.Player;

/**
 *
 * @author hjm
 */
public interface Job
{
    public void execute(Player worker);

    public void write(Writer writer) throws IOException;
    
    public void read(BufferedReader reader) throws IOException;

    public Point getLocation();

    public boolean isValid(Player worker);
}
