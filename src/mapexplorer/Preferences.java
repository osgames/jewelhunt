package mapexplorer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import mapexplorer.ui.SetupPanel;
import org.lwjgl.opengl.Display;

/**
 * Client preferences.
 * 
 * @author Hj. Malthaner
 */
public class Preferences 
{
    private static final Logger logger = Logger.getLogger(Preferences.class.getName());

    private JFrame setupFrame;
    
    public String name;
    public String server;
    public int port;
    public int colorBody;
    public int colorEyes;
    public int colorBackpack;
    private volatile boolean setupOpen;
    public int species;
    
    public boolean load()
    {
        final String userHome = System.getProperty("user.home");
        
        Properties props = new Properties();
        
        File file = new File(userHome, ".mapchat.properties");
        if(file.exists())
        {
            try 
            {
                props.load(new FileInputStream(file));
            }
            catch (IOException ex) 
            {
                logger.log(Level.SEVERE, null, ex);
                return false;
            }
        }
        
        name = props.getProperty("name");
        server = props.getProperty("server");
        port = props.getProperty("port") != null ? Integer.parseInt(props.getProperty("port")) : -1;
        colorBody = props.getProperty("color") != null ? Integer.parseInt(props.getProperty("color").substring(1), 16) : -1;
        
        if(name == null || server == null || port == -1)
        {
            name = "Dummy";
            server = "localhost";
            port = 13521;
            colorBody = 0xCCFF66;
            colorEyes = 0xFFFFFF;
            colorBackpack = 0xFFFFFF;
            
            setupFrame = new JFrame("Setup");
            SetupPanel setupPanel = new SetupPanel();
            setupPanel.setPreferences(this);
            
            ImagePanel imagePanel = new ImagePanel("/mapexplorer/resources/ui/paper_bordered.png");
            imagePanel.setLayout(new BorderLayout());
            imagePanel.add(setupPanel);
            setupFrame.add(imagePanel);
            setupFrame.setSize(460, 460);
            
            Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
            setupFrame.setLocation((size.width - setupFrame.getWidth()) / 2, 
                                   (size.height - setupFrame.getHeight()) / 2);
            setupFrame.setVisible(true);

            setupOpen = true;
            while(setupOpen)
            {
                Display.update();
                Display.sync(60);
            }
            
        }
        
        return true;
    }

    public void save()
    {
        
    }

    public void setupDone()
    {
        setupFrame.dispose();
        setupFrame = null;
        setupOpen = false;
    }
}
