package mapexplorer;

import java.io.File;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import jewelhunt.game.Clock;
import jewelhunt.game.GameInterface;
import jewelhunt.game.Texture;
import jewelhunt.game.TextureCache;
import jewelhunt.ogl.GlTextureCache;
import jewelhunt.game.World;
import jewelhunt.game.map.Map;
import jewelhunt.game.player.MovementJumping;
import jewelhunt.game.player.Player;
import jewelhunt.game.player.Species;
import jewelhunt.oal.SoundPlayer;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.ui.PostRenderHook;
import mapexplorer.net.client.MapExplororProxy;
import mapexplorer.ui.MapExplorerDisplay;
import org.lwjgl.LWJGLException;
import static org.lwjgl.opengl.GL11.glClearColor;
import rlgamekit.objects.Cardinal;

/**
 * Client core.
 * 
 * @author Hj. Malthaner
 */
public class Client implements PostRenderHook, GameInterface
{
    private static final String nameVersion = "Map Explorer r002";

    private static final Logger logger = Logger.getLogger(Client.class.getName());
    
    public final TextureCache textureCache;
    public final IsoDisplay display;
    public final SoundPlayer soundPlayer;
    public final MapExplorerDisplay chatDisplay;
    
    int mouseI, mouseJ;

    int playerKey;
    
    private final MapExplororProxy proxy;
    public final World world;
    private MapExplorerKeyHandler keyHandler;
    private final Map map;
    
    public Client() throws IOException, LWJGLException
    {
        Clock.init(System.currentTimeMillis());

        mouseI = -1;
        mouseJ = -1;

        world = new World();
        soundPlayer = new SoundPlayer();

        map = new Map(16, 16);
        proxy = new MapExplororProxy(world, map, this);

        textureCache = new TextureCache();
        display = new IsoDisplay(world.mobs, world.items, textureCache);
        display.create();        

        chatDisplay = new MapExplorerDisplay(this, display);
    }

    public void initialize() throws LWJGLException, IOException
    {
        Preferences prefs = new Preferences();
        prefs.load();
        
        Player player = new Player(world, 96, 128, Species.GLOBOS_RC_BASE, map, null, 45, new MovementJumping());
        
        proxy.connect(prefs.server, prefs.port);
        proxy.start();
        
        playerKey = proxy.login(prefs, "");
        world.mobs.put(playerKey, player);
        player.setKey(playerKey);

        player.visuals.color = prefs.colorBody;
        
        
        display.map = player.gameMap;
        display.map.recalculateBlockedAreas(display.textureCache.textures);
        display.centerOn(player);

        final Texture intro = textureCache.loadTexture("/impcity/resources/ui/dance_of_rebirth_by_shiroikuro.jpg", false);
        
        textureCache.initialize(new TextureCache.LoaderCallback() {

            @Override
            public void update(String msg)
            {
                splash(intro, msg);
            }
        });
        
        
        splash(intro, "Preparing map ...");

        logger.log(Level.INFO, "Map loaded. Size={0}x{1}", new Object[]{display.map.getWidth(), display.map.getHeight()});
                
        display.setTitle(nameVersion);
        
        soundPlayer.init();
        
        String PATH = "/impcity/resources/sfx/";

        String [] sampleFiles = new String []
        {
            PATH + "click.wav",
            PATH + "wosh.wav",
            PATH + "wipwap.wav",
            PATH + "magic_farmland.wav",
            PATH + "magic_library.wav",
            PATH + "deselect.wav",
            PATH + "arrival.wav",
            PATH + "magic_treasury.wav",
            PATH + "magic_lair.wav",
            PATH + "magic_workshop.wav",
        };

        splash(intro, "Loading sounds ...");
        
        if(!soundPlayer.loadSamples(sampleFiles))
        {
            logger.log(Level.SEVERE, "Error while loading sound data.");
        }
        
        chatDisplay.addMessageToLog("System", 0, 1, "Welcome to " + nameVersion + "!");
        
        glClearColor(0.3f, 0.4f, 0.2f, 1.0f);
    }
    
    @Override
    public World getWorld()
    {
        return world;
    }

    @Override
    public int getPlayerKey()
    {
        return playerKey;
    }
    
    
    private void run()
    {
        String [] realms = new String [0];
        try 
        {
            realms = proxy.listRealms();
        }
        catch (IOException ex) 
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        chatDisplay.listAllRealms(realms);
        
        display.postRenderHook = this;
        display.mouseHandler = new MapExplorerMouseHandler(this, chatDisplay, display);
        keyHandler = new MapExplorerKeyHandler(this, display, chatDisplay);
        display.keyHandler = keyHandler;
        display.run();
    }
    
    private void update()
    {
        try
        {
            Set<Cardinal> keys = world.mobs.keySet();

            for(Cardinal key : keys)
            {
                Player mob = world.mobs.get(key.intValue());

                if(mob.getPath() != null)
                {
                    boolean lastStep = mob.advance(soundPlayer);
                    
                    if(lastStep && key.intValue() == playerKey)
                    {
                        // Hajo: the player of this client reached the end of their path
                        // -> now we need to sync the orientation to the server, because
                        // the server is not doing any walking calculations.
                        
                        proxy.arriveAt(playerKey, mob.location.x, mob.location.y);
                    }
                }

                mob.update(proxy);
            }

            Player player = world.mobs.get(playerKey);
            if(player != null)
            {
                display.centerOn(player.location.x+9, player.location.y+9, 
                                 player.iOff, player.jOff);
            }
        } 
        catch(ConcurrentModificationException cmex)
        {
            // Hajo: this can happen while loading the map ...
            // Todo: find a way to handle this cleanly.
            logger.log(Level.INFO, cmex.getMessage());
        }
    }
    
    
    public void destroy()
    {
        display.destroy();
        soundPlayer.destroy();
    }
    
    @Override
    public void displayMore() 
    {
        update();
        chatDisplay.setMessages(keyHandler.keyBuffer.toString());
        chatDisplay.display();
    }

    void walkPlayer()
    {
        // System.err.println("Move T0: " + System.currentTimeMillis());
        
        Player player = world.mobs.get(playerKey);
        player.walkTo(proxy, mouseI, mouseJ);
    }
    
    private void splash(Texture intro, String msg)
    {
        display.clear();
        IsoDisplay.drawTile(intro, 1200 - intro.image.getWidth(), 0);
        display.font.drawString(nameVersion, 0xFFFFFF, 220, 440);
        display.font.drawStringScaled(msg, 0xDDDDDD, 220, 400, 0.8);
        display.update();
    }

    public static void main(String[] args)
    {
        try 
        {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) 
            {
                if ("Nimbus".equals(info.getName())) 
                {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) 
        {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }        
        
        Client client = null;
        try
        {
            client = new Client();
            client.initialize();
            client.run();
        } 
        catch (LWJGLException ex)
        {
            logger.log(Level.SEVERE, ex.toString(), ex);
        } 
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, ex.toString(), ex);
        } 
        finally
        {
            if(client != null)
            {
                try
                {
                    client.destroy();
                }
                catch (Exception ex)
                {
                    logger.log(Level.SEVERE, ex.toString(), ex);
                } 
            }
        }
    }

    void broadcastMessage(String message)
    {
        // Hajo: check for client commands
        try 
        {
            if(message.startsWith("/upload"))
            {                
                selectAndUploadMap(message);
            }
            else if(message.startsWith("/list"))
            {                
                String [] realms = proxy.listRealms();
                chatDisplay.listAllRealms(realms);
            }
            else
            {
                proxy.broadcastMessage(message);
            }
        }
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    private void selectAndUploadMap(final String message) throws IOException 
    {
        Thread chooserThread = new Thread()
        {
            @Override
            public void run()
            {
                int p = message.indexOf(' ');
                String mapName = null;
                if(p > 5)
                {
                    mapName = message.substring(p+1);
                }
                
                
                JFileChooser jfc = new JFileChooser();
                int r = jfc.showDialog(null, "Upload Map");
                if(r == JFileChooser.APPROVE_OPTION)
                {
                    File mapFile = jfc.getSelectedFile();
                    Map newMap = new Map(16, 16);

                    try 
                    {
                        if(mapName == null)
                        {
                            mapName = mapFile.getName();
                        }
                        
                        newMap.load(mapFile);
                        chatDisplay.addMessageToLog("System", 0, 1, "Uploading map '" + mapName +"' size " + newMap.getWidth() + "x" + newMap.getHeight());
                        proxy.uploadMap(newMap, mapName);
                        
                    } catch (IOException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        };
        
        chooserThread.start();
    }
    
    void joinRealm(int realmNumber) 
    {
        proxy.joinRealm(realmNumber);
    }

    void pickItem()
    {
        Player player = world.mobs.get(playerKey);
        if(player != null)
        {
            try 
            {
                proxy.pickItem(player.location.x, player.location.y);
            }
            catch (IOException ex) 
            {
                logger.log(Level.SEVERE, null, ex);
            }
        }        
    }

    void requestInventory() 
    {
        try 
        {
            proxy.loadInventory();
        }
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    void sendInventoryToServer() 
    {
        Player player = world.mobs.get(playerKey);
        if(player != null)
        {
            try 
            {
                proxy.sendIventory(player.inventory);
            }
            catch (IOException ex) 
            {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }
}