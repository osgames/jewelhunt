package mapexplorer.ui;

import java.awt.Color;
import javax.swing.Icon;
import javax.swing.colorchooser.AbstractColorChooserPanel;

/**
 *
 * @author Hj. Maltaner
 */
public class PlayerColorChooserPanel extends AbstractColorChooserPanel
{

    public PlayerColorChooserPanel() 
    {
        
        
        int rgb = Color.HSBtoRGB(1f, 1f, 1f);
        
    }

    
    
    @Override
    public void updateChooser() 
    {
    }

    @Override
    protected void buildChooser() 
    {
    }

    @Override
    public String getDisplayName() 
    {
        return "Player colors";
    }

    @Override
    public Icon getSmallDisplayIcon() 
    {
        return null;
    }

    @Override
    public Icon getLargeDisplayIcon() 
    {
        return null;
    }
    
}
