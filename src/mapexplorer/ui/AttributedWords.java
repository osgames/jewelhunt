package mapexplorer.ui;

import java.awt.Rectangle;
import java.util.ArrayList;
import jewelhunt.ui.PixFont;

/**
 * Several lines of attributed words
 * @author Hj. Malthaner
 */
public class AttributedWords 
{
    private final ArrayList<AttributedWord> line;
    private final ArrayList<WordPosition> positions;
    
    public AttributedWords()
    {
        line = new ArrayList<AttributedWord>(8);
        positions = new ArrayList<WordPosition>(8);
    }
    
    public void addWord(AttributedWord word)
    {
        line.add(word);
    }
    
    
    public void addLine(String textWithMarkup, int rgba)
    {
        boolean done = false;
        AttributedWord word;

        int p0 = 0;
        
        while(!done)
        {
            int p1 = textWithMarkup.indexOf("[[", p0);

            if(p1 > 0)
            {
                int p2 = textWithMarkup.indexOf("]]", p1);

                word = new AttributedWord();
                word.text = textWithMarkup.substring(p0, p1);
                word.rgba = rgba;

                // new color
                String color = textWithMarkup.substring(p1+2, p2-2);
                
                System.err.println("word='" + word.text + "'");
                System.err.println("new color=" + color);
            
                p0 = p2+2;
            }
            else
            {
                // no markup anymore
                word = new AttributedWord();
                word.text = textWithMarkup.substring(p0);
                word.rgba = rgba;
                done = true;
                System.err.println("word=" + word.text);
            }
        }        
    }
    
    public void display(PixFont font, int x, int y, double scale)
    {
        positions.clear();
        int h = font.getLetterHeight();
        
        int runx = x;
        for(AttributedWord word : line)
        {
            int w = font.drawStringScaled(word.text, word.rgba, runx, y, scale);
            
            WordPosition position = new WordPosition(word);
            position.area = new Rectangle(runx, y, w, h);
            positions.add(position);

            runx += w;
        }
    }
    
    public AttributedWord getWordAt(int x, int y)
    {
        for(WordPosition position : positions)
        {
            // System.err.println("t=" + position.word.text + " x=" + x + " y=" + y + " area=" + position.area);
            
            if(position.area.contains(x, y))
            {
                return position.word;
            }
        }
        
        return null;
    }
    
    private class WordPosition
    {
        public Rectangle area;
        public AttributedWord word;

        private WordPosition(AttributedWord word) 
        {
            this.word = word;
        }
    }
}
