package mapexplorer.ui;

/**
 * A piece of text with common attributes
 * @author Hj. Malthaner
 */
public class AttributedWord 
{
    public String text;
    public int rgba;
}
