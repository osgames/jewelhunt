package mapexplorer.ui;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.Clock;
import jewelhunt.game.Texture;
import jewelhunt.game.TextureCache;
import jewelhunt.game.World;
import jewelhunt.game.player.Player;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.ui.GameDisplay;
import jewelhunt.ui.PixFont;
import mapexplorer.Client;
import org.lwjgl.input.Mouse;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;

/**
 * Display layer over the map display for all game-specific
 * display functions.
 * 
 * @author Hj. Malthaner
 */
public class MapExplorerDisplay 
{
    private static final Logger logger = Logger.getLogger(MapExplorerDisplay.class.getName());

    private final Texture uiBackground;
    private final Texture paperSheet;
    private final Texture flourishCorner;

    public boolean showInventory;
    
    private final Client client;
    private final PixFont blackFont32;
    private final PixFont whiteFont32;
    
    private final GameDisplay gameDisplay;
    
    private String input;
    
    private final ArrayList <AttributedWords> messageLog;
    
    public boolean showRealmList;
    public String [] realmChoices;    
    private final IsoDisplay display;
    
    public MapExplorerDisplay(Client client, IsoDisplay display) throws IOException 
    {
        this.messageLog = new ArrayList<AttributedWords>(32);
        this.client = client;
        this.display = display;
        
        TextureCache textureCache = display.textureCache;
        
        uiBackground = textureCache.loadTexture("/mapexplorer/resources/ui/ui_background.png", true);
        paperSheet = textureCache.loadTexture("/mapexplorer/resources/ui/paper_bordered.png", true);
        flourishCorner = textureCache.loadTexture("/mapexplorer/resources/ui/arvin61r58-corner_flourish.png", true);
        input = "";
        
        blackFont32 = new PixFont("/mapexplorer/resources/ui/serif_black_32");
        whiteFont32 = new PixFont("/mapexplorer/resources/ui/serif_white_32");
        // blackFont16 = new PixFont("/mapexplorer/resources/ui/serif_black_16");
        
        gameDisplay = new GameDisplay(client, display);
    }

    public void setMessages(String messages)
    {
        this.input = messages;
    }
    
    public void display()
    {
        World world = client.getWorld();
        Player player = world.mobs.get(client.getPlayerKey());
        
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        IsoDisplay.drawTile(uiBackground, 0, 0, display.displayWidth, 768, 0xFFFFFFFF);
        // IsoDisplay.drawTile(paperSheet, 0, 196, displayWidth, 196, 0xFFFFFFFF);

        displayCorners(0, 0, display.displayWidth, 190, 32, 3);

        
        blackFont32.drawStringScaled(">", 0xFF000000, 30, 22, 0.5);
        int w = whiteFont32.drawStringScaled(input, 0xFFFFFFFF, 38, 22, 0.5);
        
        // Hajo: fake cursor
        if(((Clock.time() >> 9) & 1) != 0)
        {
            IsoDisplay.fillRect(38 + w + 2, 22, 2, 17, 0xFFFFFFFF);
        }
        
        if(showRealmList)
        {
            displayRealmList();
        }
        
        if(showInventory)
        {
            gameDisplay.displayInventory(player);
        }
        
        for(int i=0; i<messageLog.size(); i++)
        {
            AttributedWords line = messageLog.get(i);
            line.display(whiteFont32, 30, 50 + i*20, 0.5);
            
            // font.drawStringScaled(messageLog.get(i), 0xFF221100, 30, 50 +  i*20, 0.5);
        }
        
        // button texts
        
        whiteFont32.drawStringScaled("Inventory", 0xFFFFFFFF, display.displayWidth-80, 200, 0.5);
    }

    private void displayRealmList() 
    {
        int left = 300;
        int top = 700;
        displayDialogBody(left, top-600, 500, 600);
        
        whiteFont32.drawStringScaled("Available Realms", 0xFFFFFFFF, left + 150, top - 60, 0.8);
        
        int mouseX = Mouse.getX();
        int mouseY = Mouse.getY();
        
        for(int i=0; i<realmChoices.length; i++)
        {
            int x = left + 30;
            int y = top - 100 - i*20;
            
            if(mouseX > x && mouseX < x+250 && mouseY > y && mouseY < y+26)
            {
                IsoDisplay.fillRect(x-4, y-4, 230, 26, 0xAA505050);
            }
            
            whiteFont32.drawStringScaled(realmChoices[i], 0xFFFFFFFF, x+10, y, 0.6);
        }
    }
    
    public void listAllRealms(String[] realms) 
    {
        showRealmList = true;
        
        realmChoices = new String[realms.length];
        
        for(int i=0; i<realms.length; i++)
        {
            char choice = (char)('a' + i);
            realmChoices[i] = "" + choice + ") " + realms[i];
        }
    }

    public void addMessageToLog(String name, int color, int type, String message) 
    {
        AttributedWord nameWord = new AttributedWord();
        nameWord.text = name + ": ";
        nameWord.rgba = colorFromNameColor(color);
        
        AttributedWord messageWord = new AttributedWord();
        messageWord.text = message;
        messageWord.rgba = colorFromMessageType(type);

        AttributedWords line = new AttributedWords();
        line.addWord(nameWord);
        line.addWord(messageWord);
        
        messageLog.add(0, line);
        
        if(messageLog.size() > 7)
        {
            messageLog.remove(7);
        }
    }

    private int colorFromMessageType(int type) 
    {
        switch(type)
        {
            case 1:
                return 0xFFFFFFFF;                
            default:
                return 0xFFF0FEFF;                
        }
    }

    private int colorFromNameColor(int rgba) 
    {
        if(rgba == 0)
        {
            return 0xFFFDDBB;                
        }

        return rgba;                
    }

    public void handleMouseClick(int x, int y, int button)
    {
        for (AttributedWords line : messageLog) 
        {
            AttributedWord word = line.getWordAt(x, y);
            if(word != null)
            {
                // System.err.println("Word clicked: " + word.text);
                
                if(word.text.startsWith("http://"))
                {
                    try 
                    {
                        // Seems to be an url
                        Desktop desktop = Desktop.getDesktop();
                        desktop.browse(new URI(word.text));
                    }
                    catch (URISyntaxException ex) 
                    {
                        Logger.getLogger(MapExplorerDisplay.class.getName()).log(Level.SEVERE, null, ex);
                    } 
                    catch (IOException ex) 
                    {
                        Logger.getLogger(MapExplorerDisplay.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                break;
            }
        }
    }
    
    private void displayDialogBody(int x, int y, int w, int h)
    {
        int size = 64;
        int margin = 8;
        
        // body
        // IsoDisplay.fillRect(x, y, w, h, 0xFFF5E7D4);
        IsoDisplay.drawTile(paperSheet, x, y, w, h, 0xFFFFFFFF);

        // borders
        IsoDisplay.fillRect(x, y, w, 1, 0xFF000000);
        IsoDisplay.fillRect(x, y, 1, h, 0xFF000000);
        IsoDisplay.fillRect(x, y+h-1, w, 1, 0xFF000000);
        IsoDisplay.fillRect(x+w-1, y, 1, h, 0xFF000000);
        
        displayCorners(x, y, w, h, size, margin);
    }

    private void displayCorners(int x, int y, int w, int h, int size, int margin) 
    {      
        // top left
        IsoDisplay.drawTile(flourishCorner, x+size+margin, y+h-size-margin, -size, size, 0.9f, 1.0f, 0.8f, 1.0f);

        // top right
        IsoDisplay.drawTile(flourishCorner, x+w-size-margin, y+h-size-margin, size, size, 0.9f, 1.0f, 0.8f, 1.0f);

        // bottom left
        IsoDisplay.drawTile(flourishCorner, x+size+margin, y+size+margin, -size, -size, 0.9f, 1.0f, 0.8f, 1.0f);

        // bottom right
        IsoDisplay.drawTile(flourishCorner, x+w-size-margin, y+size+margin, size, -size, 0.9f, 1.0f, 0.8f, 1.0f);
    }
}
