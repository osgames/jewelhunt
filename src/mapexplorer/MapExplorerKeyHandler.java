package mapexplorer;

import jewelhunt.ogl.IsoDisplay;
import jewelhunt.ui.KeyHandler;
import mapexplorer.ui.MapExplorerDisplay;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author Hj. Malthaner
 */
public class MapExplorerKeyHandler implements KeyHandler
{
    private final Client client;
    private final IsoDisplay display;
    private final MapExplorerDisplay gameDisplay;

    public final StringBuilder keyBuffer;

    public MapExplorerKeyHandler(Client game, IsoDisplay display, MapExplorerDisplay gameDisplay)
    {
        this.client = game;
        this.display = display;
        this.gameDisplay = gameDisplay;
        this.keyBuffer = new StringBuilder();
    }
    
    @Override
    public void processKeyboard()
    {
        if(Keyboard.next())
        {
            if(Keyboard.getEventKeyState() == true && !Keyboard.isRepeatEvent())
            {
                if(gameDisplay.showRealmList)
                {
                    char key = Keyboard.getEventCharacter();
                    if(key >= 'a' && key <= 'z')
                    {
                        int realmNumber = key - 'a';
                        
                        client.joinRealm(realmNumber);
                        
                        gameDisplay.showRealmList = false;
                        gameDisplay.addMessageToLog("System", 0, 1, "You are joining realm '" + gameDisplay.realmChoices[realmNumber].substring(3) + "'.");
                    }
                }
                else
                {                
                    char key = Keyboard.getEventCharacter();
                    if(key >= 32)
                    {
                        keyBuffer.append(key);
                    }

                    int keyCode = Keyboard.getEventKey();

                    if(keyCode == Keyboard.KEY_BACK)
                    {
                        keyBuffer.deleteCharAt(keyBuffer.length()-1);
                    }
                    else if(keyCode == Keyboard.KEY_RETURN)
                    {
                        client.broadcastMessage(keyBuffer.toString());
                        keyBuffer.delete(0, keyBuffer.length());
                    }
                    else if(keyCode == Keyboard.KEY_ESCAPE)
                    {
                        client.sendInventoryToServer();
                        client.chatDisplay.showInventory = false;
                    }
                    
                    if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || 
                       Keyboard.isKeyDown(Keyboard.KEY_RCONTROL))
                    {
                        // Hajo: control keys
                        if(keyCode == Keyboard.KEY_G)
                        {
                            client.pickItem();
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean collectString(StringBuilder buffer)
    {
        return true;
    }
}