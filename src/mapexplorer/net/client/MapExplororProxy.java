package mapexplorer.net.client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.World;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.map.Map;
import jewelhunt.game.player.MobVisuals;
import jewelhunt.game.player.MovementJitter;
import jewelhunt.game.player.MovementJumping;
import jewelhunt.game.player.Player;
import jewelhunt.game.player.Species;
import jewelhunt.net.Server;
import mapexplorer.Client;
import mapexplorer.Preferences;
import mapexplorer.net.common.Commands;
import mapexplorer.net.common.Marshaller;
import mapexplorer.net.common.StreamTransport;
import mapexplorer.net.common.Transport;
import rlgamekit.pathfinding.Path;

/**
 * Translates method calls into network transports.
 * 
 * @author Hj. Malthaner
 */
public class MapExplororProxy implements Server
{
    private static final Logger logger = Logger.getLogger(MapExplororProxy.class.getName());

    private Socket socket;
    private Transport transport;
    private final World world;
    private final Map map;

    private ByteBuffer syncResult;
    private final Client client;
    
    public MapExplororProxy(World world, Map map, Client client) throws IOException
    {
        this.client = client;
        this.world = world;
        this.map = map;
        
        syncResult = null;
    }
    
    @Override
    public void shutdown() 
    {

    }
    
    synchronized public int login(Preferences prefs, String password)
            throws IOException
    {
        byte [] nameBytes = prefs.name.getBytes(Charset.forName("UTF-8"));
        byte [] passBytes = password.getBytes(Charset.forName("UTF-8"));
        
        ByteBuffer buf = ByteBuffer.allocate(5 + nameBytes.length + passBytes.length + 16);
        
        buf.put(Commands.LOGIN);
        buf.putShort((short)nameBytes.length);
        buf.put(nameBytes);
        buf.putShort((short)passBytes.length);
        buf.put(passBytes);
        buf.putInt(prefs.colorBody);
        buf.putInt(prefs.colorEyes);
        buf.putInt(prefs.colorBackpack);
        buf.putInt(prefs.species);
        
        transport.send(buf);
        
        try {
            wait();
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    
        int key = syncResult.getInt();
        syncResult = null;

        logger.log(Level.INFO, "{0} logged in as player #{1}", new Object[]{prefs.name, key});
        
        return key;
    }
    
    @Override
    public void moveTo(int playerKey, int x, int y) 
    {
        ByteBuffer buf = ByteBuffer.allocate(5);
        buf.put(Commands.MOVE_TO);
        buf.putShort((short)x);
        buf.putShort((short)y);
        
        try 
        {
            transport.send(buf);
        }
        catch (IOException ex) 
        {
            Logger.getLogger(MapExplororProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void arriveAt(int playerKey, int x, int y) 
    {
        Player player = world.mobs.get(playerKey);
        player.visuals.getDisplayCode();
        
        try 
        {
            setDisplayCode(player.visuals.getDisplayCode(), 1);
        }
        catch (IOException ex) 
        {
            Logger.getLogger(MapExplororProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void runJobs() 
    {
    }

    @Override
    public Map loadMap(int playerKey, String mapName) 
    {
        return null;
    }
    
    synchronized public String [] listRealms() throws IOException 
    {
        ByteBuffer buf = ByteBuffer.allocate(1);
        
        buf.put(Commands.LIST_REALMS);
        
        transport.send(buf);
        
        try {
            wait();
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        }

        int count = syncResult.getInt();
        String [] result = new String [count];
        
        for(int i=0; i<count; i++)
        {
            result[i] = readString(syncResult);
        }
        
        syncResult = null;
        
        return result;
    }
    
    public void joinRealm(int realmId) 
    {
        ByteBuffer buf = ByteBuffer.allocate(3);
        buf.put(Commands.JOIN_REALM);
        buf.putShort((short)realmId);
        
        try 
        {
            transport.send(buf);
        }
        catch (IOException ex) 
        {
            Logger.getLogger(MapExplororProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void broadcastMessage(String message) throws IOException 
    {
        byte [] messageBytes = message.getBytes(Charset.forName("UTF-8"));
        
        ByteBuffer buffer = ByteBuffer.allocate(1 + 2 + message.length());
        buffer.put(Commands.BROADCAST_MESSAGE);
        buffer.putShort((short)messageBytes.length);
        buffer.put(messageBytes);
        
        transport.send(buffer);
    }

    public void pickItem(int x, int y) throws IOException
    {
        ByteBuffer buffer = ByteBuffer.allocate(1 + 4);
        buffer.put(Commands.PICK_ITEM);
        buffer.putShort((short)x);
        buffer.putShort((short)y);
        
        transport.send(buffer);
    }
    
    public void setDisplayCode(int displayCode, int broadcast) throws IOException
    {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.put(Commands.SET_VISUAL);
        buffer.putShort((short)displayCode);
        buffer.put((byte)broadcast);
        
        transport.send(buffer);
    }

    public void loadInventory() throws IOException 
    {
        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put(Commands.LIST_INVENTORY);
        
        transport.send(buffer);
    }

    public void start() 
    {
        CommandPoller poller = new CommandPoller();
        Thread t = new Thread(poller);
        t.setDaemon(true);
        t.start();
    }
    
    private synchronized void setSyncResult(ByteBuffer result)
    {
        syncResult = result;
        notify();
    }

    private String readString(ByteBuffer buffer) 
    {
        int length = buffer.getShort();
        byte [] stringBytes = new byte[length];
        buffer.get(stringBytes);
        
        String result = null;
        try 
        {
            result = new String(stringBytes, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    private byte [] writeString(String string)
    {
        byte [] bytes = string.getBytes(Charset.forName("UTF-8"));        
        return bytes;
    }

    public void connect(String server, int port) throws IOException 
    {
        socket = new Socket(server, port);
        transport = new StreamTransport(socket.getInputStream(), socket.getOutputStream());
    }

    public void uploadMap(Map map, String name) throws IOException 
    {
        ByteArrayOutputStream mapOut = new ByteArrayOutputStream(8192);
        map.save(mapOut);
        mapOut.flush();
        byte [] mapData = mapOut.toByteArray();

        byte [] nameBytes = writeString(name);
        
        ByteBuffer buffer = ByteBuffer.allocate(1 + 2 + nameBytes.length + mapData.length);
        buffer.put(Commands.UPLOAD_MAP);
        buffer.putShort((short)nameBytes.length);
        buffer.put(nameBytes);
        buffer.put(mapData);
        
        transport.send(buffer);
    }

    public void sendIventory(ArrayList<InventoryItem> inventory) throws IOException 
    {
        byte [] data = Marshaller.marshallInventory(inventory);
        
        ByteBuffer buffer = ByteBuffer.allocate(1 + 4 + data.length);

        buffer.put(Commands.INVENTORY_ITEMS);
        buffer.putInt(inventory.size());
        buffer.put(data);
        transport.send(buffer);
    }


    
    private class CommandPoller implements Runnable
    {
        @Override
        public void run() 
        {
            do
            {
                try 
                {
                    ByteBuffer buffer = transport.receive();

                    int what = buffer.get();
                    logger.log(Level.INFO, "Got command: {0}", what);

                    switch(what)
                    {
                        case Commands.LOGIN_RESULT:
                            processLoginResult(buffer);
                            break;
                        case Commands.MOB_PATH:
                            setMobPath(buffer);
                            break;
                        case Commands.MAP_DATA:
                            receiveMap(buffer);
                            break;
                        case Commands.PLAYER_JOIN:
                            playerJoin(buffer);
                            break;
                        case Commands.PLAYER_LEAVE:
                            playerLeave(buffer);
                            break;
                        case Commands.LIST_REALMS:
                            processRealmList(buffer);
                            break;
                        case Commands.ADD_MESSAGE:
                            addMessage(buffer);
                            break;
                        case Commands.PLACE_ITEM_MAP:
                            placeItem(buffer);
                            break;
                        case Commands.PLACE_ITEM_INVENTORY:
                            addItemToInventory(buffer);
                            break;
                        case Commands.INVENTORY_ITEMS:
                            updateAndShowInventory(buffer);
                            break;
                        case Commands.SET_VISUAL:
                            setVisual(buffer);
                            break;
                        default:
                            logger.log(Level.INFO, "Unknown command type: {0}", what);
                    }
                }
                catch (IOException ex) 
                {
                    logger.log(Level.SEVERE, null, ex);
                }
            } while(true);
            
        }

        private void setMobPath(ByteBuffer pathBuffer) 
        {
            // System.err.println("Move T3: " + System.currentTimeMillis());
            
            int playerKey = pathBuffer.getInt();
            int length = pathBuffer.getShort();
            
            Path path = new Path();
            
            for(int i=0; i<length; i++)
            {
                int px = pathBuffer.getShort();
                int py = pathBuffer.getShort();
                
                path.addStep(px, py);
            }
            
            Player player = world.mobs.get(playerKey);
            player.setPath(path);
        }
        
        private void receiveMap(ByteBuffer buffer) throws IOException
        {
            byte [] data = buffer.array();

            int playerCount = buffer.getShort();
            int itemCount = buffer.getShort();
            int mobDataLength = buffer.getShort();
            int itemDataLength = buffer.getShort();
            int mapDataLength = buffer.getShort();
            
            ByteArrayInputStream in = new ByteArrayInputStream(data, 11, mobDataLength);
            
            logger.log(Level.INFO, "Loading {0} players from realm", playerCount);
            
            world.mobs.clear();
            
            InputStreamReader reader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(reader);
            
            for(int i=0; i<playerCount; i++)
            {
                logger.log(Level.INFO, "Loading player #{0}", i);
                Player mob = new Player(world, 0, 0, 0, map, null, 45, new MovementJumping());
                mob.read(bufferedReader, null);
                
                logger.log(Level.INFO, "Registering player with key{0}", mob.getKey());
                world.mobs.put(mob.getKey(), mob);
                
                calcVisuals(mob, bufferedReader);
                calcMovementPattern(mob);
            }
            in.close();

            ByteArrayInputStream itemsIn = new ByteArrayInputStream(data, 11 + mobDataLength, itemDataLength);
            InputStreamReader isr = new InputStreamReader(itemsIn);
            BufferedReader itemReader = new BufferedReader(isr);

            for(int i=0; i<itemCount; i++)
            {
                logger.log(Level.INFO, "Loading item #{0}", i);

                Item item = new Item(itemReader, world.itemFactory.itemCatalog);

                world.items.put(item.getRegistryKey(), item);
            }
            
            ByteArrayInputStream mapIn = new ByteArrayInputStream(data, 11 + mobDataLength + itemDataLength, mapDataLength);
            
            map.load(mapIn);

            client.chatDisplay.addMessageToLog("System", 0, 1, "Join successful.");
        }

        private void processLoginResult(ByteBuffer buffer) 
        {
            setSyncResult(buffer);
        }

        private void playerJoin(ByteBuffer buffer) throws IOException 
        {
            byte [] playerData = buffer.array();

            ByteArrayInputStream in = new ByteArrayInputStream(playerData, 1, playerData.length - 1);
            
            InputStreamReader reader = new InputStreamReader(in);
            BufferedReader bufferedReader = new BufferedReader(reader);
            
            Player mob = new Player(world, 0, 0, 0, map, null, 45, new MovementJumping());
            mob.read(bufferedReader, null);

            calcVisuals(mob, bufferedReader);
            calcMovementPattern(mob);
            
            logger.log(Level.INFO, "Registering joining player with key{0}", mob.getKey());
            mob.visuals.name = mob.name; // show name
            world.mobs.put(mob.getKey(), mob);
        }

        private void playerLeave(ByteBuffer buffer) 
        {
            int key = buffer.getShort();
            
            logger.log(Level.INFO, "Clearing leaving player with key{0}", key);

            Player mob = world.mobs.get(key);
            map.setMob(mob.location.x, mob.location.y, 0);
            
            world.mobs.remove(key);
            
        }

        private void processRealmList(ByteBuffer buffer) 
        {
            setSyncResult(buffer);
        }

        private void addMessage(ByteBuffer buffer) 
        {
            int type = buffer.getShort();
            int argb = buffer.getInt();
            
            String name = readString(buffer);
            String message = readString(buffer);
            
            client.chatDisplay.addMessageToLog(name, argb, type, message);
        }

        private void placeItem(ByteBuffer buffer) throws IOException 
        {
            int x = buffer.getShort();
            int y = buffer.getShort();
            int n = buffer.get();
            
            if(n==0)
            {
                map.setItem(x, y, 0);
            }
            else
            {    
                byte [] data = buffer.array();
                ByteArrayInputStream in = new ByteArrayInputStream(data, 6, data.length-6);
                InputStreamReader isr = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(isr);

                Item item = new Item(reader, world.itemFactory.itemCatalog);

                world.items.put(item.getRegistryKey(), item);
                map.dropItem(x, y, item.getRegistryKey());
            }
        }

        private void addItemToInventory(ByteBuffer buffer) throws IOException 
        {
            int playerKey = buffer.getInt();
            
            Player mob = world.mobs.get(playerKey);
            
            byte [] data = buffer.array();
            ByteArrayInputStream in = new ByteArrayInputStream(data, 5, data.length-5);
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader reader = new BufferedReader(isr);

            Item item = new Item(reader, world.itemFactory.itemCatalog);

            mob.addToInventory(new InventoryItem(item), 1, 1);
        }

        private void updateAndShowInventory(ByteBuffer buffer) throws IOException 
        {
            Player player = world.mobs.get(client.getPlayerKey());

            Marshaller.unmarshallInventory(buffer, player.inventory, world.itemFactory);
            
            client.chatDisplay.showInventory = true;
        }
        
        private void setVisual(ByteBuffer buffer) 
        {
            int playerKey = buffer.getInt();
            int displayCode = buffer.getShort();
            
            Player mob = world.mobs.get(playerKey);
            mob.visuals.setDisplayCode(displayCode);
        }

        private void calcMovementPattern(Player mob) 
        {
            if(mob.getSpecies() == Species.GLOBOS_RC_BASE)
            {
                
            }
            else
            {
                mob.pattern = new MovementJitter(8, 1 << 16);
            }
        }

        private void calcVisuals(Player mob, BufferedReader bufferedReader) throws IOException 
        {
            mob.visuals.name = mob.name; // show name
            
            String line = bufferedReader.readLine();
            int species = Integer.parseInt(line);
            mob.visuals.setDisplayCode(species);

            line = bufferedReader.readLine();
            mob.visuals.color = 0xFF000000 | Integer.parseInt(line);

            for(int i=0; i<mob.visuals.equipmentOverlaysColors.length; i++)
            {
                line = bufferedReader.readLine();
                mob.visuals.equipmentOverlaysColors[i] = 0xFF000000 | Integer.parseInt(line);
            }
            
            // Hajo: some parts are not recolored
            if(species == Species.GLOBOS_RC_BASE)
            {
                mob.visuals.equipmentOverlays[MobVisuals.OVERLAY_EYES] = species + 16;
                mob.visuals.equipmentOverlays[MobVisuals.OVERLAY_BACKPACK] = species + 24;
            }
        }

    }
}
