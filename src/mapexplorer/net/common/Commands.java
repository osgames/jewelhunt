package mapexplorer.net.common;

/**
 * Command constants.
 * 
 * @author Hj. Malthaner
 */
public class Commands 
{
    // Commands from server to client
    public static final byte LOGIN_RESULT = 70;
    public static final byte MAP_DATA = 71;
    public static final byte MOB_PATH = 72;
    public static final byte PLAYER_JOIN = 73;
    public static final byte PLAYER_LEAVE = 74;
    public static final byte ADD_MESSAGE = 75;
    public static final byte PLACE_ITEM_MAP = 76;
    public static final byte PLACE_ITEM_INVENTORY = 77;
    public static final byte SET_VISUAL = 78;
    public static final byte INVENTORY_ITEMS = 79;
    
    // Commands from client to server
    public static final byte LOGIN = 31;
    public static final byte LOGOUT = 32;
    public static final byte LIST_REALMS = 33;
    public static final byte JOIN_REALM = 34;
    public static final byte MOVE_TO = 35;
    public static final byte BROADCAST_MESSAGE = 36;
    public static final byte UPLOAD_MAP = 37;
    public static final byte PICK_ITEM = 38;
    public static final byte SYNC_VISUAL = 39;
    public static final byte LIST_INVENTORY = 40;
}
