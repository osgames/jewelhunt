package mapexplorer.net.common;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.ItemFactory;

/**
 * Conversion helper for game-data to network-data and back
 * @author Hj. Malthaner
 */
public class Marshaller 
{

    public static byte[] marshallInventory(ArrayList<InventoryItem> inventory) throws IOException 
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(8192);
        OutputStreamWriter writer = new OutputStreamWriter(baos);
        
        for(InventoryItem iinv : inventory)
        {
            iinv.write(writer);
        }
        
        writer.flush();
        
        byte [] data = baos.toByteArray();
        
        return data;
    }

    public static void unmarshallInventory(ByteBuffer buffer, 
                                           ArrayList<InventoryItem> inventory,
                                           ItemFactory itemFactory) throws IOException 
    {
        int itemCount = buffer.getInt();
        inventory.clear();

        byte [] data = buffer.array();
        ByteArrayInputStream bais = new ByteArrayInputStream(data, 5, data.length-5);
        BufferedReader reader = new BufferedReader(new InputStreamReader(bais));

        for(int i=0; i<itemCount; i++)
        {
            InventoryItem iinv = new InventoryItem(reader, itemFactory.itemCatalog);
            inventory.add(iinv);
        }

    }
    
}
