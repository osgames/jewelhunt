package mapexplorer.net.common;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 *
 * @author Hj. Malthaner
 */
public interface Transport 
{
    public void send(ByteBuffer data) throws IOException;
    public ByteBuffer receive() throws IOException;
}
