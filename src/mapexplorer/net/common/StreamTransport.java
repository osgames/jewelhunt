package mapexplorer.net.common;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Stream based data transport.
 * 
 * @author Hj. Malthaner
 */
public class StreamTransport implements Transport
{
    private final OutputStream out;
    private final InputStream in;
    
    public StreamTransport(InputStream in, OutputStream out)
    {
        this.in = in;
        this.out = out;
    }

    /**
     * Max data packet size is 1<<16
     * @param dataBuffer The data to send
     * @throws IOException In case of problems
     */
    @Override
    synchronized public void send(ByteBuffer dataBuffer) throws IOException 
    {
        byte [] data = dataBuffer.array();
        int length = data.length;
        
        out.write(length >> 8);
        out.write(length & 255);
        out.write(data);
        out.flush();
    }

    /**
     * Reads one complete command.
     * @return ByteBuffer with command data
     * 
     * @throws IOException in case of problems.
     */
    @Override
    public ByteBuffer receive() throws IOException 
    {
        // Hajo: Try to read all data of one command
    
        int length = read16(in);
        
        byte [] data = new byte[length];
        int actual = 0;
        
        do
        {
            actual += in.read(data, actual, length-actual);
            
            // System.err.println("actual=" + actual + " length=" + length);
        }
        while(actual < length);
        
        return ByteBuffer.wrap(data);
    }
    
    private int read16(InputStream in) throws IOException
    {
        int v1, v2;

        v1 = in.read();
        if(v1 == -1) throw new EOFException("End of input stream reached.");
        
        v2 = in.read();
        if(v2 == -1) throw new EOFException("End of input stream reached.");
        
        return (v1 << 8) | v2;
    }
}
