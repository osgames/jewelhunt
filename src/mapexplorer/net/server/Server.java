package mapexplorer.net.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.item.ItemFactory;
import jewelhunt.game.map.Map;

/**
 * Server core.
 *
 * @author Hj. Malthaner
 */
public class Server
{
    private static final Logger logger = Logger.getLogger(Server.class.getName());

    final ArrayList <Realm> realms = new ArrayList<Realm>(32);
    final ArrayList <ServerClient> clients;    
    final ItemFactory itemFactory;

    private final ServerSocket serverSocket;
    private int newPlayerId = 0;
    
    public Server(int port) throws IOException
    {
        clients = new ArrayList<ServerClient>(32);
        serverSocket = new ServerSocket(port);
        logger.log(Level.INFO, "Socket created.");
        
        itemFactory = new ItemFactory();
        
        buildDefaultRealm();
    }
    
    public void acceptConnections()
    {
        boolean done = false;
        while(!done)
        {
            try
            {
                logger.log(Level.INFO, "Waiting for client.");
                Socket clientSocket = serverSocket.accept();

                ServerClient client = new ServerClient(this, clientSocket);
                clients.add(client);
                client.start();
            }
            catch(Exception ex)
            {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    public void shutdown()
    {
        
    }
    
    public int newPlayerId()
    {
        newPlayerId ++;
        
        if(newPlayerId >= Integer.MAX_VALUE - 2)
        {
            newPlayerId = 1;
        }
        
        return newPlayerId;
    }
    
    public static void main(String[] args)
    {
        try
        {
            Server server = new Server(13521);
            server.acceptConnections();
        }
        catch (IOException ex)
        {
            logger.log(Level.SEVERE, ex.toString(), ex);
        }
    }

    void removeClient(ServerClient client) 
    {
        clients.remove(client);
    }

    private void buildDefaultRealm() 
    {
        Map map = new Map(16, 16);

        InputStream in  = Class.class.getResourceAsStream("/mapexplorer/resources/nature.map");
        try 
        {
            map.load(in);
        }
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }

        Realm realm = new Realm();
        realm.map = map;
        realm.name = "Outdoor";
        realms.add(realm);
    }
}
