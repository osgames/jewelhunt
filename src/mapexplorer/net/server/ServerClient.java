package mapexplorer.net.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.InventoryGrid;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.map.Map;
import jewelhunt.game.player.MobVisuals;
import jewelhunt.game.player.MovementJumping;
import jewelhunt.game.player.Player;
import jewelhunt.net.server.FindPath;
import mapexplorer.net.common.Commands;
import mapexplorer.net.common.Marshaller;
import mapexplorer.net.common.StreamTransport;
import mapexplorer.net.common.Transport;
import rlgamekit.objects.Cardinal;
import rlgamekit.pathfinding.Path;

/**
 * Server side logic and data for each client.
 * 
 * @author Hj. Malthaner
 */
class ServerClient 
{
    private static final Logger logger = Logger.getLogger(ServerClient.class.getName());

    private final Socket socket;
    private final Transport transport;
    private final Server server;
    
    private Player player;
    private Realm realm;
    
    ServerClient(Server server, Socket clientSocket) throws IOException 
    {
        this.server = server;
        this.socket = clientSocket;
        
        this.transport = new StreamTransport(socket.getInputStream(), socket.getOutputStream());
    }
    
    public void start()
    {
        SocketReader socketReader = new SocketReader();
        Thread thread = new Thread(socketReader);
        thread.setDaemon(true);
        thread.start();
    }
    
    private void shutdown()
    {
        if(realm != null) realm.clients.remove(this);
        server.removeClient(this);

        ByteBuffer buffer = ByteBuffer.allocate(3);
        buffer.put(Commands.PLAYER_LEAVE);
        buffer.putShort((short)player.getKey());
        try 
        {
            broadcast(realm, buffer);
        }
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    private String readString(ByteBuffer buffer) 
    {
        int length = buffer.getShort();
        byte [] stringBytes = new byte[length];
        buffer.get(stringBytes);
        
        String result = null;
        try 
        {
            result = new String(stringBytes, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    private byte [] writeString(String string)
    {
        byte [] bytes = string.getBytes(Charset.forName("UTF-8"));        
        return bytes;
    }

    private void broadcast(Realm realm, ByteBuffer result) throws IOException 
    {
        if(realm != null)
        {
            for(ServerClient client : realm.clients)
            {
                client.transport.send(result);
            }
        }
    }
    
    private void broadcast(Realm realm, ByteBuffer result, int radius) throws IOException 
    {
        int x = player.location.x;
        int y = player.location.y;
        
        for(ServerClient client : realm.clients)
        {
            int dx = client.player.location.x;
            int dy = client.player.location.y;
            
            int distance = Math.max(Math.abs(dx - x), Math.abs(dy - y));
            
            if(distance <= radius)
            {
                client.transport.send(result);
            }
        }
    }
    
    private void broadcastPlayerJoin(Realm realm, Player player) throws IOException 
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(out);
        player.write(writer);
        writer.write("" + player.visuals.getDisplayCode() + "\n");
        writer.write("" + player.visuals.color + "\n");
        
        for(int i=0; i<player.visuals.equipmentOverlaysColors.length; i++)
        {
            writer.write("" + player.visuals.equipmentOverlaysColors[i] + "\n");
        }
        
        writer.flush();
        out.flush();
        
        byte [] data = out.toByteArray();
        
        ByteBuffer result = ByteBuffer.allocate(1 + data.length);
        result.put(Commands.PLAYER_JOIN);
        result.put(data);
        
        broadcast(realm, result);
    }
    
    private class SocketReader implements Runnable
    {
        @Override
        public void run() 
        {
            boolean done = false;
            do
            {
                try
                {
                    logger.log(Level.INFO, "Waiting for input ...");
                    ByteBuffer buffer = transport.receive();

                    int what = buffer.get();
                    logger.log(Level.INFO, "Got command: {0}", what);

                    switch(what)
                    {
                        case Commands.LOGIN:
                            login(buffer);
                            break;
                        case Commands.JOIN_REALM:
                            joinRealm(buffer);
                            break;
                        case Commands.MOVE_TO:
                            moveTo(buffer);
                            break;
                        case Commands.LIST_REALMS:
                            listRealms(buffer);
                            break;
                        case Commands.BROADCAST_MESSAGE:
                            broadcastMessage(buffer);
                            break;
                        case Commands.UPLOAD_MAP:
                            receiveMap(buffer);
                            break;
                        case Commands.PICK_ITEM:
                            pickItem(buffer);
                            break;
                        case Commands.SET_VISUAL:
                            setOrientation(buffer);
                            break;
                        case Commands.LIST_INVENTORY:
                            listInventory(buffer);
                            break;
                        case Commands.INVENTORY_ITEMS:
                            saveInventory(buffer);
                            break;
                        default:
                            logger.log(Level.INFO, "Unknown command type: {0}", what);
                    }
                }
                catch(SocketException sox)
                {
                    logger.log(Level.INFO, sox.toString(), sox);
                    done = true;
                }
                catch(IOException iox)
                {
                    logger.log(Level.INFO, iox.toString(), iox);
                    done = true;
                }
                catch(Exception ex)
                {
                    logger.log(Level.SEVERE, ex.toString(), ex);
                }
            } while(!done);
            
            logger.log(Level.INFO, "Closing connection");
            shutdown();
        }

    }
    
    public void login(ByteBuffer buffer) throws IOException
    {
        String name = readString(buffer);
        String pass = readString(buffer);
        int colorBody = buffer.getInt();
        int colorEyes = buffer.getInt();
        int colorBackpack = buffer.getInt();
        int species = buffer.getInt();
        
        logger.log(Level.INFO, "Login attempt from player: {0}", name);
        player = new Player(null, 96, 128, species, null, null, 45, new MovementJumping());
        int key = server.newPlayerId();
        player.setKey(key);
        player.name = name;
        player.visuals.color = colorBody;
        player.visuals.equipmentOverlaysColors[MobVisuals.OVERLAY_EYES] = colorEyes;
        player.visuals.equipmentOverlaysColors[MobVisuals.OVERLAY_BACKPACK] = colorBackpack;
                
        ByteBuffer result = ByteBuffer.allocate(5);
        result.put(Commands.LOGIN_RESULT);
        result.putInt(key);

        logger.log(Level.INFO, "{0} logged in with key {1}", new Object[]{name, key}) ;
        
        transport.send(result);
    }
    
    private void listRealms(ByteBuffer buffer) throws IOException 
    {
        int count = server.realms.size();

        byte [] [] list = new byte [count] [];
        int size = 0;

        for(int i=0; i<count; i++)
        {
            list [i] = writeString(server.realms.get(i).name);
            size += list[i].length + 2;
        }

        ByteBuffer result = ByteBuffer.allocate(5 + size);
        result.put(Commands.LIST_REALMS);
        result.putInt(count);

        for(int i=0; i<count; i++)
        {
            result.putShort((short)list[i].length);
            result.put(list[i]);
        }

        transport.send(result);
    }

    public void joinRealm(ByteBuffer buffer) throws IOException
    {
        int realmId = buffer.getShort();
        realm = server.realms.get(realmId);

        broadcastPlayerJoin(realm, player);
        
        // after broadcast, add player and send map and all mobs
        // back to the new player
        realm.clients.add(this);
        
        ByteArrayOutputStream mobOut = new ByteArrayOutputStream(8192);

        OutputStreamWriter writer = new OutputStreamWriter(mobOut);
        for(ServerClient client : realm.clients)
        {
            client.player.write(writer);
            writer.write("" + client.player.visuals.getDisplayCode() + "\n");
            writer.write("" + client.player.visuals.color + "\n");
            for(int i=0; i<player.visuals.equipmentOverlaysColors.length; i++)
            {
                writer.write("" + player.visuals.equipmentOverlaysColors[i] + "\n");
            }
        }
        
        writer.flush();


        ByteArrayOutputStream itemsOut = new ByteArrayOutputStream(8192);

        writer = new OutputStreamWriter(itemsOut);
        
        Set <Cardinal> itemKeys = realm.items.keySet();
        for(Cardinal itemKey : itemKeys)
        {
            Item item = realm.items.get(itemKey.intValue());
            item.write(writer);
        }
        
        writer.flush();
        
        
        ByteArrayOutputStream mapOut = new ByteArrayOutputStream(8192);
        realm.map.save(mapOut);
        
        mapOut.flush();   
        
        byte [] mobData = mobOut.toByteArray();
        byte [] itemData = itemsOut.toByteArray();
        byte [] mapData = mapOut.toByteArray();

        System.err.println("Sending " + 
                (mobData.length + itemData.length + mapData.length) + 
                " bytes of realm data.");

        ByteBuffer sendBuffer = ByteBuffer.allocate(mapData.length + itemData.length + mobData.length + 1 + 10);
        sendBuffer.put(Commands.MAP_DATA);
        sendBuffer.putShort((short)realm.clients.size());
        sendBuffer.putShort((short)itemKeys.size());
        sendBuffer.putShort((short)mobData.length);
        sendBuffer.putShort((short)itemData.length);
        sendBuffer.putShort((short)mapData.length);
        sendBuffer.put(mobData);
        sendBuffer.put(itemData);
        sendBuffer.put(mapData);
        transport.send(sendBuffer);
        
        System.err.println("Done.");
    }

    public void placeItem(Item item, int x, int y) throws IOException
    {
        if(item == null)
        {
            ByteBuffer result = ByteBuffer.allocate(1 + 4 + 1);
            result.put(Commands.PLACE_ITEM_MAP);
            result.putShort((short)x);
            result.putShort((short)y);
            result.put((byte)0);
            
            realm.map.setItem(x, y, 0);
            
            broadcast(realm, result);
        }
        else
        {
            ByteArrayOutputStream out = new ByteArrayOutputStream(8192);
            OutputStreamWriter writer = new OutputStreamWriter(out);
            item.write(writer);
            writer.flush();

            byte [] itemData = out.toByteArray();

            ByteBuffer result = ByteBuffer.allocate(1 + 4 + 1 + itemData.length);
            result.put(Commands.PLACE_ITEM_MAP);
            result.putShort((short)x);
            result.putShort((short)y);
            result.put((byte)1);
            result.put(itemData);

            realm.map.setItem(x, y, item.getRegistryKey());

            broadcast(realm, result);
        }
    }    
    
    public void addItemToInventory(Item item) throws IOException
    {
        player.addToInventory(new InventoryItem(item), InventoryGrid.WIDTH, InventoryGrid.HEIGHT);
        
        ByteArrayOutputStream out = new ByteArrayOutputStream(8192);
        OutputStreamWriter writer = new OutputStreamWriter(out);
        item.write(writer);
        writer.flush();

        byte [] itemData = out.toByteArray();

        ByteBuffer result = ByteBuffer.allocate(1 + 4 + itemData.length);
        result.put(Commands.PLACE_ITEM_INVENTORY);
        result.putInt(player.getKey());
        result.put(itemData);

        broadcast(realm, result);
    }
    
    
    
    private void moveTo(ByteBuffer buffer) throws IOException 
    {
        // System.err.println("Move T1: " + System.currentTimeMillis());
                
        int sx = player.location.x;
        int sy = player.location.y;
        int dx = buffer.getShort();
        int dy = buffer.getShort();
        
        FindPath finder = new FindPath(player.getKey(), realm.map, sx, sy, dx, dy);
        finder.run();
        Path path = finder.path;
        
        // System.err.println("Move T2: " + System.currentTimeMillis());
        
        int length = path.length();
        
        ByteBuffer result = ByteBuffer.allocate(length*4 + 7);

        System.err.println("Moving player " + player.getKey() + " from " + sx + ", " + sy + " to " + dx + ", " + dy + 
                " -> " + length  + " steps");
        
        result.put(Commands.MOB_PATH);
        result.putInt(player.getKey());
        result.putShort((short)length);
        
        short x = -1, y = -1;
        for(int i=0; i<length; i++)
        {
            Path.Node node = path.getStep(i);
            x = (short)node.x;
            y = (short)node.y;
            result.putShort(x);
            result.putShort(y);
        }
        
        player.location.x = x;
        player.location.y = y;
        
        broadcast(realm, result);

        System.err.println("Move T2: " + System.currentTimeMillis());
    }
    
    private void broadcastMessage(ByteBuffer buffer) throws IOException 
    {
        int length = buffer.getShort();
        byte [] messageBytes = new byte[length];
        buffer.get(messageBytes);

        String message = new String(messageBytes, "UTF-8");
        
        if(message.startsWith("/item"))
        {
            // Testing .. create an item and transport it to the client
        
            Item item = server.itemFactory.createItem("coins_copper", realm.items);
            placeItem(item, player.location.x, player.location.y);
        }
        else
        {
            byte [] nameBytes = writeString(player.name);

            ByteBuffer result = ByteBuffer.allocate(1 + 2 + 4 + 2 + length + 2 + nameBytes.length);

            result.put(Commands.ADD_MESSAGE);
            result.putShort((short)0); // message type. 0 = user message
            result.putInt(player.visuals.color);
            result.putShort((short)nameBytes.length);
            result.put(nameBytes);
            result.putShort((short)length);
            result.put(messageBytes);

            broadcast(realm, result, 35);
        }
    }

    private void receiveMap(ByteBuffer buffer) throws IOException
    {
        byte [] mapData = buffer.array();

        String mapName = readString(buffer);

        int dataIndex = buffer.position();

        logger.log(Level.INFO, "Receiving map '" + mapName + "' dataOffset=" + dataIndex);
        
        ByteArrayInputStream in = new ByteArrayInputStream(mapData, dataIndex, mapData.length-dataIndex);
        Map map = new Map(16, 16);
        map.load(in);
        
        Realm newRealm = new Realm();
        newRealm.name = mapName;
        newRealm.map = map;
        
        server.realms.add(newRealm);
        
        sendMessage("New realm created for '" + mapName + "'");
    }

    private void sendMessage(String message) throws IOException 
    {
        byte [] nameBytes = writeString("System");
        byte [] messageBytes = writeString(message);
        
        ByteBuffer result = ByteBuffer.allocate(1 + 2 + 4 + 2 + nameBytes.length + 2 + messageBytes.length);
        
        result.put(Commands.ADD_MESSAGE);
        result.putShort((short)1); // message type. 1 = system message
        result.putInt(0); // System messages only have a dummy color here.
        result.putShort((short)nameBytes.length);
        result.put(nameBytes);
        result.putShort((short)messageBytes.length);
        result.put(messageBytes);
        
        broadcast(realm, result, 0);
    }

    private void pickItem(ByteBuffer buffer) throws IOException 
    {
        int x = buffer.getShort();
        int y = buffer.getShort();

        int key = realm.map.getItem(x, y);
        Item item = realm.items.get(key);
        
        if(item != null)
        {
            placeItem(null, x, y);
            addItemToInventory(item);
            
            sendMessage("You got " + item.aCountAndName() + ".");
        }
    }
    
    private void setOrientation(ByteBuffer buffer) throws IOException 
    {
        int displayCode = buffer.getShort();
        int broadcast = buffer.get();

        player.visuals.setDisplayCode(displayCode);
        
        if(broadcast == 1)
        {
            ByteBuffer result = ByteBuffer.allocate(1 + 4 + 2);

            result.put(Commands.SET_VISUAL);
            result.putInt(player.getKey());
            result.putShort((short)displayCode);
            
            broadcast(realm, result);
        }
    }
    
    private void listInventory(ByteBuffer buffer) throws IOException 
    {
        byte [] data = Marshaller.marshallInventory(player.inventory);
        ByteBuffer result = ByteBuffer.allocate(1 + 4 + data.length);
        
        result.put(Commands.INVENTORY_ITEMS);
        result.putInt(player.inventory.size());
        result.put(data);
        
        transport.send(result);
    }

    private void saveInventory(ByteBuffer buffer) throws IOException 
    {
        Marshaller.unmarshallInventory(buffer, player.inventory, server.itemFactory);
    }
}
