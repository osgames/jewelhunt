package mapexplorer.net.server;


import java.nio.channels.SocketChannel;

/**
 * Based on http://rox-xmlrpc.sourceforge.net/niotut/
 */
public class ServerDataEvent
{
    public Server server;
    public SocketChannel socket;
    public byte[] data;

    public ServerDataEvent(Server server, SocketChannel socket, byte[] data)
    {
        this.server = server;
        this.socket = socket;
        this.data = data;
    }

    public ServerDataEvent(Server server, SocketChannel socket, String responseData)
    {
        this(server, socket, responseData.getBytes());
    }
}