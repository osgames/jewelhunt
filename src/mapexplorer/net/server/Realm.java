package mapexplorer.net.server;

import java.util.ArrayList;
import jewelhunt.game.item.Item;
import jewelhunt.game.map.Map;
import rlgamekit.objects.ArrayRegistry;

/**
 * A realm consists of a map with its connected clients
 * 
 * @author Hj. Malthaner
 */
public class Realm 
{
    String name;
    Map map;
    public final ArrayList <ServerClient> clients;

    public final ArrayRegistry<Item> items;
    
    public Realm()
    {
        clients = new ArrayList <ServerClient> (32);
        items = new ArrayRegistry<Item>(1024);
    }
}
