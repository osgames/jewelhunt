package mapexplorer;

import java.util.logging.Logger;
import jewelhunt.game.map.Map;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.ui.MouseHandler;
import mapexplorer.ui.MapExplorerDisplay;
import org.lwjgl.input.Mouse;

/**
 *
 * @author Hj. Malthaner
 */
public class MapExplorerMouseHandler implements MouseHandler
{
    public static final Logger logger = Logger.getLogger(MapExplorerMouseHandler.class.getName());
    
    private boolean lastButtonState = false;
    private final Client client;
    private final IsoDisplay display;

    private final MapExplorerDisplay gameDisplay;

    private int buttonPressed;
    private int buttonReleased;

    public MapExplorerMouseHandler(Client game, 
                            MapExplorerDisplay gameDisplay,
                            IsoDisplay display)
    {
        this.client = game;
        this.gameDisplay = gameDisplay;
        this.display = display;
    }
    
    @Override
    public void processMouse()
    {
        int mx = (Mouse.getX() - display.centerX - 108);
        int my = (Mouse.getY() - display.centerY - 108) * 2;

        int mmi = -mx - my;
        int mmj = mx - my;

        // System.err.println("mmi = " + mmi + " mmj = " + mmj);

        client.mouseI = mmi * Map.SUB / 216;
        client.mouseJ = mmj * Map.SUB / 216;

        display.cursorI = client.mouseI;
        display.cursorJ = client.mouseJ;

        // System.err.println("mi = " + mouseI + " mj = " + mouseJ);

        while(Mouse.next())
        {
            int button = Mouse.getEventButton();
            boolean buttonState = Mouse.getEventButtonState();
            buttonPressed = 0;
            buttonReleased = 0;

            if(buttonState != lastButtonState)
            {
                if(buttonState)
                {
                    buttonPressed = button + 1;
                }
                else
                {
                    buttonReleased = button + 1;
                }
                
                lastButtonState = buttonState;
            }
            
            int mouseY = Mouse.getY();

            if(gameDisplay.showRealmList)
            {
                if(buttonPressed == 1) 
                {
                    handleRealmList();
                }
            }
            else
            {
                if(mouseY < 196)
                {
                    // This was a click into the message area
                    if(buttonPressed > 0)
                    {
                        handleMessageClick(buttonPressed);
                    }
                }
                else
                {
                    // This was a click into the map area
                    if(buttonPressed == 1)
                    {
                        boolean buttonHit = checkTopLevelButtons();
                        
                        if(!buttonHit)
                        {
                            client.walkPlayer();
                        }
                    }
                }
            }
        }
    }    

    private void handleRealmList() 
    {
        int top = 600;
        
        int y = Mouse.getY();
        
        int line = (top - y) / 20;
        
        
        if(line >= 0 && line < gameDisplay.realmChoices.length)
        {
            gameDisplay.showRealmList = false;
            client.joinRealm(line);
        }
    }

    private void handleMessageClick(int button) 
    {
        gameDisplay.handleMouseClick(Mouse.getX(), Mouse.getY(), button);
    }

    private boolean checkTopLevelButtons() 
    {
        int mx = Mouse.getX();
        int my = Mouse.getY();
        
        int inventoryX = display.displayWidth - 94;
        int inventoryY = 196;
        
        boolean hit = false;
        
        if(mx >= inventoryX && mx < inventoryX + 94 && my >= inventoryY && my < inventoryY+32)
        {
            client.requestInventory();
            hit = true;
        }
            
        return hit;
    }
}