package mapexplorer;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Hj Malthaner
 */
class ImagePanel extends JPanel
{
    private final Image img;
    
    public ImagePanel(String imagePath)
    {
        Image temp = null;
        try 
        {
            InputStream in = getClass().getResourceAsStream(imagePath);
            temp = ImageIO.read(in);
        } catch (IOException ex) {
            Logger.getLogger(ImagePanel.class.getName()).log(Level.SEVERE, null, ex);
        }

        img = temp;
    }

    @Override
    public void paint(Graphics gr)
    {
        gr.drawImage(img, 0, 0, getWidth(), getHeight(), null);
        super.paintComponents(gr);
    }
}
