package mapexplorer.test;

import mapexplorer.ui.AttributedWords;

/**
 *
 * @author Hj. Malthaner
 */
public class TestMarkupText 
{
    public static void main(String [] args)
    {
        String textWithMarkup =
                "Normal [[rgba=FFFF0000]]red[[rgba=FF0000FF]] blue[[rgba=FF00FF00]] green";
    
        AttributedWords text = new AttributedWords();
        text.addLine(textWithMarkup, 0xFF000000);
    }
}
