//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.ogl;

/**
 *
 * @author Hj. Malthaner
 */
public interface Drawable
{
    public void display(IsoDisplay display, int x, int y);    
}
