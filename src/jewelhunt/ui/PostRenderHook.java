//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.ui;

/**
 *
 * @author Hj. Malthaner
 */
public interface PostRenderHook 
{
    public void displayMore();
}
