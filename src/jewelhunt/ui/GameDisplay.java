//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.ui;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import jewelhunt.game.Clock;
import jewelhunt.game.GameInterface;
import jewelhunt.game.InventoryGrid;
import jewelhunt.game.Texture;
import jewelhunt.game.TextureCache;
import jewelhunt.game.World;
import jewelhunt.game.combat.Damage;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.item.ItemInfoAssembly;
import jewelhunt.game.map.MapTransitions;
import jewelhunt.game.player.Player;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.uikit.Dialog;
import jewelhunt.uikit.Trigger;
import static org.lwjgl.opengl.GL11.GL_DST_COLOR;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;


/**
 * OpenGL driven game display
 * 
 * @author Hj. Malthaner
 */
public class GameDisplay implements PostRenderHook
{
    private final NumberFormat nf = NumberFormat.getInstance();
    
    // display dialog control flags
    public boolean showInventory;
    public boolean showCharacter;
    public boolean showMerchant;
    public boolean showMogrisphere;
    
    public final int rightDialogX = 600;
    public final int rightDialogY = 20;
    
    public final int leftDialogX = 100;
    public final int leftDialogY = 20;
    
    final Dialog inventoryDialog;
    final Dialog merchantDialog;
    private final Dialog characterDialog;
    final Dialog mogrisphereDialog;
    
    private final Texture inventoryBg;
    private final Texture characterBg;
    private final Texture merchantBg;
    private final Texture mogrisphereBg;
    private final Texture lightVignette;
    private final Texture uiBarBg;
    
    private final PixFont font;
    private final GameInterface game;
    private final IsoDisplay display;
    private InventoryItem itemForPopup;
    private int itemForPopupX;
    private int itemForPopupY;

    private final int [] itemColors = new int [4];
    
    private Trigger inventoryHighlightTrigger = null;
    private int inventoryHighlightGridI = -1;
    private int inventoryHighlightGridJ = -1;
    private int inventoryHighlightGridWidth = 0;
    private int inventoryHighlightGridHeight = 0;
    private final ArrayList<TimedMessage> messages = new ArrayList<TimedMessage>();
    
    Player currentMerchant;
    
    public GameDisplay(GameInterface game, IsoDisplay display) throws IOException
    {
        this.game = game;
        this.display = display;
        this.font = display.font;
        
        TextureCache textureCache = display.textureCache;
        
        inventoryBg = textureCache.loadTexture("/jewelhunt/resources/inventory_bg.jpg", false);
        characterBg = textureCache.loadTexture("/jewelhunt/resources/character_bg.jpg", false);
        merchantBg = textureCache.loadTexture("/jewelhunt/resources/merchant_bg.jpg", false);
        mogrisphereBg = textureCache.loadTexture("/jewelhunt/resources/mogrisphere_bg.jpg", false);
        lightVignette = textureCache.loadTexture("/jewelhunt/resources/vignette.png", true);
        uiBarBg = textureCache.loadTexture("/jewelhunt/resources/ui_bar_bg.png", true);

        characterDialog = new Dialog(font);
        characterDialog.parse(this.getClass().getResourceAsStream("/jewelhunt/resources/character_ui.def"));

        inventoryDialog = new Dialog(font);
        inventoryDialog.parse(this.getClass().getResourceAsStream("/jewelhunt/resources/inventory_ui.def"));
    
        merchantDialog = new Dialog(font);
        merchantDialog.parse(this.getClass().getResourceAsStream("/jewelhunt/resources/merchant_ui.def"));

        mogrisphereDialog = new Dialog(font);
        mogrisphereDialog.parse(this.getClass().getResourceAsStream("/jewelhunt/resources/mogri_ui.def"));

        itemColors[0] = Colors.TEXT_WARM;
        itemColors[1] = Colors.BLUE.brighter().getRGB();
        itemColors[2] = Colors.GREEN2.brighter().getRGB();
        itemColors[3] = Colors.YELLOW.brighter().getRGB();
    }
    
    public void addMessage(TimedMessage message)
    {
        messages.add(message);
    }
    
    @Override
    public void displayMore()
    {
        World world = game.getWorld();
        Player player = world.mobs.get(game.getPlayerKey());
        
        // Hajo: super-simple vignette light falloff effect
        if(player.gameMap.darkness != 0)
        {
            glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
            int lightRad = 500;
            
            int cx = display.displayWidth / 2;
            int cy = display.displayHeight / 2;
            int lw = (cx * lightRad) >> 8;
            int lh = (cy * lightRad) >> 8;
            
            // Hajo: yellowish torch/lantern light
            IsoDisplay.drawTile(lightVignette, cx - lw, cy - lh, lw * 2, lh * 2, 
                                player.gameMap.darkness + 0xFFEEDD);
        }
        
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        if(!showCharacter && !showInventory)
        {
            if(world.getCurrentMap() == MapTransitions.MapId.VILLAGE_1)
            {
                // font.drawString("The Hunt For The Lost Rainbow Jewels", Colors.TEXT_WARM, 10, displayHeight - 50);


                font.drawStringScaled("Walkaround demo. Click a spot to move there.", Colors.TEXT_COLD, 10, display.displayHeight - 50, 0.8);
                // font.drawStringScaled("Keys:", Colors.TEXT_COLD, 10, displayHeight - 120, 0.8);
                font.drawStringScaled("C: Toggle character sheet", Colors.TEXT_COLD, 10, display.displayHeight - 110, 0.6);
                font.drawStringScaled("I: Toggle the inventory", Colors.TEXT_COLD, 10, display.displayHeight - 134, 0.6);
                font.drawStringScaled("TAB: Toggle map item labels", Colors.TEXT_COLD, 10, display.displayHeight - 158, 0.6);
                font.drawStringScaled("ESC: Quit", Colors.TEXT_COLD, 10, display.displayHeight - 182, 0.6);

/*
                font.drawStringScaled("Temp.:", Colors.TEXT_WARM, displayWidth - 250, displayHeight - 50, 0.8);
                font.drawStringScaled("Static:", Colors.TEXT_WARM, displayWidth - 250, displayHeight - 78, 0.8);
                font.drawStringScaled("Ground:", Colors.TEXT_WARM, displayWidth - 250, displayHeight - 106, 0.8);
                font.drawStringScaled("Air:", Colors.TEXT_WARM, displayWidth - 250, displayHeight - 134, 0.8);

                font.drawStringScaled("14°C", Colors.TEXT_COLD, displayWidth - 150, displayHeight - 50, 0.8);
                font.drawStringScaled("Uncharged", Colors.TEXT_COLD, displayWidth - 150, displayHeight - 78, 0.8);
                font.drawStringScaled("Solid, moist", Colors.TEXT_COLD, displayWidth - 150, displayHeight - 106, 0.8);
                font.drawStringScaled("Humid", Colors.TEXT_COLD, displayWidth - 150, displayHeight - 134, 0.8);
*/                
            }
            
            // font.drawStringScaled("v0.04", 0xFFFFFF, displayWidth - 70, 16, 0.5);
        }
        
        IsoDisplay.drawTile(uiBarBg, 1, 0);

        IsoDisplay.drawTile(display.textureCache.textures[980], 1010, 120);
        IsoDisplay.drawTile(display.textureCache.textures[977], 1080, 120);
        
        String lifeString = "" + (player.stats.getCurrent(Player.I_VIT) >> 8) + "/" + player.calculateMaxLife();
        font.drawStringScaled(lifeString, Colors.TEXT_WARM, 30, 150, 0.6);
        String manaString = "" + (player.stats.getCurrent(Player.I_MANA) >> 8) + "/" + player.calculateMaxMana();
        font.drawStringScaled(manaString, Colors.TEXT_WARM, 120, 150, 0.6);


        // left hand side dialogs
        if(showCharacter)
        {
            displayCharacter(player);
        }
        else if(showMerchant)
        {
            displayMerchant(player);
        }
        else if(showMogrisphere)
        {
            displayMogrisphere(player);
        }
        
        // right hand side dialogs
        if(showInventory)
        {
            displayInventory(player);
        }
        
        if(itemForPopup != null)
        {
            displayItemPopup(itemForPopup.item);
        }

        for(TimedMessage message : messages)
        {
            int yoff = 8 + (((int)(Clock.time() - message.time)) >> 3);
            font.drawStringScaled(message.message, message.color, message.x, message.y + yoff, 1.0 + yoff/120.0);

            if(yoff > 100)
            {
                message.message = null;
                message.time = 0;
            }
        }
        
        purgeOutdatedMessages();
    }
    
    public void openMerchantDialog(Player merchant)
    {
        currentMerchant = merchant;
        showInventory = true;
        showMerchant = true;
        showMogrisphere = false;
        showCharacter = false;
    }
    
    private void displayMerchant(Player player) 
    {
        IsoDisplay.drawTile(merchantBg, leftDialogX, leftDialogY);

        // grid must be first trigger
        Trigger grid = merchantDialog.triggers.get(0);
        
        if(currentMerchant != null)
        {
            if(inventoryHighlightTrigger == grid)
            {
                displayInventoryHighlight(grid, leftDialogX, leftDialogY, InventoryGrid.MERCHANT_WIDTH, InventoryGrid.MERCHANT_HEIGHT);
            }
            
            displayInventoryItems(currentMerchant, grid, leftDialogX, leftDialogY);
        }
    }
    
    public void openMogrispeherDialog()
    {
        showInventory = true;
        showMerchant = false;
        showMogrisphere = true;
        showCharacter = false;
    }

    private void displayMogrisphere(Player player) 
    {
        IsoDisplay.drawTile(mogrisphereBg, leftDialogX, leftDialogY);
        font.drawStringScaled("Transmogrify Items", 0xFFFFFFFF, 244, 568, 0.8);
    }

    public void displayInventory(Player player) 
    {
        IsoDisplay.drawTile(inventoryBg, rightDialogX, rightDialogY);
    
        // grid must be first trigger
        Trigger grid = inventoryDialog.triggers.get(0);

        if(inventoryHighlightTrigger == grid)
        {
            displayInventoryHighlight(grid, rightDialogX, rightDialogY, InventoryGrid.WIDTH, InventoryGrid.HEIGHT);
        }
        
        displayInventoryItems(player, grid, rightDialogX, rightDialogY);
        
        for(int n=1; n<inventoryDialog.triggers.size(); n++)
        {
            Trigger t = inventoryDialog.triggers.get(n);
            
            for(int i=0; i<player.equipment.slotNames.length; i++)
            {
                if(player.equipment.slotNames[i].equals(t.key))
                {
                    Item item = player.equipment.getItem(i);
                    if(item != null)
                    {
                        IsoDisplay.fillRect(rightDialogX + t.area.x, rightDialogY + t.area.y, 
                                            t.area.width, t.area.height,
                                            0xFF332244);

                        Texture tex = display.textureCache.textures[item.getInventoryImageIndex()];

                        IsoDisplay.drawTile(tex, 
                                         rightDialogX + t.area.x + (t.area.width - tex.image.getWidth())/2,
                                         rightDialogY + t.area.y + (t.area.height - tex.image.getHeight())/2,
                                         item.getHue());
                    }
                }
            }
        }        

        /*
        font.drawStringScaled("Copper: ", 0, inventoryX+20, inventoryY + 655, 0.6);
        font.drawStringScaled("Silver: ", 0, inventoryX+20, inventoryY + 635, 0.6);
        font.drawStringScaled("Gold: ", 0, inventoryX+20, inventoryY + 615, 0.6);
        // font.drawStringScaled("Mithril: ", 0, inventoryX+20, inventoryY + 595, 0.6);
        
        font.drawStringScaled("" + player.copperCoins, 0, inventoryX+95, inventoryY + 655, 0.6);
        font.drawStringScaled("" + player.silverCoins, 0, inventoryX+95, inventoryY + 635, 0.6);
        font.drawStringScaled("" + player.goldCoins, 0, inventoryX+95, inventoryY + 615, 0.6);
        // font.drawStringScaled("" + player.goldCoins, 0, inventoryX+95, inventoryY + 595, 0.6);
        */
    
        font.drawStringScaled("Burden: ", Colors.TEXT_WARM, rightDialogX+27, rightDialogY + 640, 0.6);
        font.drawStringScaled("Money: ", Colors.TEXT_WARM, rightDialogX+27, rightDialogY + 615, 0.6);
        
        int str = player.stats.getCurrent(Player.I_STR);
        int maxBurden = 100 + str * 3;
        int currBurden = player.calculateTotalBurden();
        
        nf.setMaximumFractionDigits(1);
        
        font.drawStringScaled(nf.format(currBurden) + " / " + maxBurden + "", Colors.TEXT_COLD, rightDialogX+98, rightDialogY + 640, 0.6);
        font.drawStringScaled("" + player.calcMoney() + " Coins", Colors.TEXT_COLD, rightDialogX+98, rightDialogY + 615, 0.6);
    }
    
    private void displayItemPopup(Item item)
    {
        final int defaultColor = 0xDDEEFF;

        int width = 240;
        
        String [] lines = new String [32]; 
        int [] colors = new int [32]; 
        int lineCount = 0;
        int lineSpace = 28;
        int modCount = 0;

        // Hajo: collect popup contents
        
        int stateColor = 
                item.calcItemState() <= 0.5 ? 0xFF7700 : 
                    item.calcItemState() < 1.0 ? 0xDDDD44 : 
                        defaultColor;
        
        if(showMerchant)
        {
            // Hajo: we are trading .. so the item price should be shown as well
            double merchantFactor;
            
            if(itemForPopupX > rightDialogX)
            {
                merchantFactor = item.calcSellUsedFactor();
                colors[lineCount] = 0x88FF33;
                lines[lineCount++] = "Sell for " + item.calcSalesPrice(merchantFactor) + " copper";
            }
            else
            {
                // buying new/repaired items
                merchantFactor = 1.5;
                colors[lineCount] = 0xFF8833;
                lines[lineCount++] = "Buy for " + item.calcSalesPrice(merchantFactor) + " copper";
            }
        }
        
        lines[lineCount++] = item.countAndName();
        
        String itemClass = item.getItemClass();
        
        // System.err.println(itemClass);
        
        if("gem".equals(itemClass))
        {
            int [] lineRef = new int[] {lineCount, modCount};
            ItemInfoAssembly.fillGemItemInfo(item, lines, colors, lineRef,
                                                 defaultColor, stateColor);
            lineCount = lineRef[0];
            modCount = lineRef[1];
        }
        else
        {
            int [] lineRef = new int[] {lineCount, modCount};
            ItemInfoAssembly.fillDefaultItemInfo(item, lines, colors, lineRef, 
                                                 defaultColor, stateColor);
            lineCount = lineRef[0];
            modCount = lineRef[1];
        }

        double fontScale = 0.8;
        for(int i=0; i < lineCount; i++)
        {
            int w = 16 + (int)(font.getStringWidth(lines[i]) * fontScale);
            if(w > width) width = w;
        }
        
        // Hajo: make sure that popup is fully shown on screen
        
        int xoff = 0;
        
        if(itemForPopupX + width > display.displayWidth - 10)
        {
            xoff = display.displayWidth - 10 - itemForPopupX - width;
        }

        int height = lineCount * lineSpace + (int)(lineSpace * 1.4 + 0.5);
        int yoff = lineCount * lineSpace - 4;

        if(itemForPopupY > display.displayHeight - 15 - height)
        {
            yoff = display.displayHeight - 55 - itemForPopupY;
        }
        
        // Hajo: now paint the popup background
        
        IsoDisplay.fillRect(itemForPopupX + xoff, itemForPopupY + yoff - height + 38, width, height, 0xCC000000);

        // Hajo: headline info is different when trading
        if(showMerchant)
        {
            font.drawStringCentered(lines[0], colors[0], itemForPopupX + xoff, itemForPopupY + yoff, width, 0.8);
            yoff -= lineSpace * 1.2;

            colors[1] = itemColors[Math.min(itemColors.length-1, modCount)];
        }
        else
        {
            font.drawStringCentered(lines[0], itemColors[Math.min(itemColors.length-1, modCount)], itemForPopupX + xoff, itemForPopupY + yoff, width, 0.8);
            yoff -= lineSpace * 1.2;
        }
        
        // Hajo: show rest of the item info
        for(int i=1; i < lineCount; i++)
        {
            font.drawStringCentered(lines[i], colors[i], itemForPopupX + xoff, itemForPopupY + yoff, width, 0.6);
            yoff -= lineSpace;            
        }
    
        // Hajo: dump stats to stdout
        /*
        System.out.println("\n= " + lines[0] + " =");
        for(int i=1; i < lineCount; i++)
        {
            System.out.println(lines[i]);
        }
        */
    }


    void setItemForPopup(InventoryItem found, int x, int y)
    {
        if(found != itemForPopup)
        {
            this.itemForPopupX = x;
            this.itemForPopupY = y;
            this.itemForPopup = found;
        }
    }

    void setInventoryHighlight(Trigger grid, int gridI, int gridJ, int width, int height)
    {
        inventoryHighlightTrigger = grid;
        inventoryHighlightGridI = gridI;
        inventoryHighlightGridJ = gridJ;
        inventoryHighlightGridWidth = width;
        inventoryHighlightGridHeight = height;
    }
    
    private void displayCharacter(Player player)
    {
        IsoDisplay.drawTile(characterBg, leftDialogX, leftDialogY);
        
        HashMap <String, String> valueMap = characterDialog.valueMap;
        
        valueMap.put("name", "Name Goes Here");
        
        valueMap.put("str", "" + player.calculateTotalStr());        
        valueMap.put("dex", "" + player.calculateTotalDex());
        valueMap.put("wis", "" + player.calculateTotalWis());
        valueMap.put("int", "" + player.calculateTotalInt());
        valueMap.put("def", "" + player.calculateTotalDef() + " / " + player.calculateTotalBlock() + "%");
        valueMap.put("vit", "" + player.calculateMaxLife());
        valueMap.put("mana", "" + player.calculateMaxMana());
        valueMap.put("stealth", "" + player.calculateStealth());
        valueMap.put("res", "" + player.calculateBaseRes());
        valueMap.put("devi", "" + player.calculateDeviceSkill());
        valueMap.put("dodge", "" + player.calculateTotalDodge() + "%");
        
        Damage damage = player.calculateTotalDamage();        
        valueMap.put("dmg", "" + damage.physicalL + " - "  + damage.physicalH) ;
        
        int [] resistances = player.calculateTotalResistances();
        
        valueMap.put("res_phys", "" + resistances[0]);
        valueMap.put("res_fire", "" + resistances[1]);
        valueMap.put("res_ice", "" + resistances[2]);
        valueMap.put("res_light", "" + resistances[3]);
        valueMap.put("res_fear", "" + resistances[5]);
        valueMap.put("res_chaos", "" + resistances[6]);
        
        characterDialog.display(leftDialogX, leftDialogY);
    }

    private void displayInventoryItems(Player player, Trigger grid, int xoff, int yoff) 
    {
        for(InventoryItem item : player.inventory)
        {
            int n = item.item.getInventoryImageIndex();
            
            int x = xoff + grid.area.x + item.area.x*32;
            int y = yoff + grid.area.y + item.area.y*32; 
            
            IsoDisplay.fillRect(x, y, item.area.width*32, item.area.height*32, Colors.INVENTORY_BACKGROUND);
            
            item.displayItemStack(display, x, y, n);
        }
    }

    private void displayInventoryHighlight(Trigger grid, int xoff, int yoff, int width, int height) 
    {
        
        if(inventoryHighlightGridI >= 0 && inventoryHighlightGridJ >= 0 && 
           inventoryHighlightGridI < width && inventoryHighlightGridJ < height)
        {
            int x = xoff + grid.area.x + inventoryHighlightGridI*32;
            int y = yoff + grid.area.y + inventoryHighlightGridJ*32; 
        
            IsoDisplay.fillRect(x, y, inventoryHighlightGridWidth*32, inventoryHighlightGridHeight*32, Colors.INVENTORY_HIGHLIGHT);
        }
    }


    private void purgeOutdatedMessages() 
    {
        for(int i=messages.size()-1; i >=0; i--)
        {
            TimedMessage message = messages.get(i);
            if(message.message == null || message.time == 0)
            {
                messages.remove(i);
            }
        }
    }
}
