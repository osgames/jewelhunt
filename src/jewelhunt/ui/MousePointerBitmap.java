//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.ui;

import jewelhunt.game.Texture;

/**
 * Game specific mouse pointers.
 * 
 * @author Hj. Malthaner
 */
public class MousePointerBitmap 
{
    public Texture tex;
    public int grabX, grabY;
    public int hue;
}
