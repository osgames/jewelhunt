//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.ui;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.Game;
import jewelhunt.game.InventoryGrid;
import jewelhunt.game.World;
import jewelhunt.game.combat.Damage;
import jewelhunt.game.combat.DamageAttack;
import jewelhunt.game.combat.magic.NovaFireDrawable;
import jewelhunt.game.combat.magic.Spell;
import jewelhunt.game.combat.magic.SpellFactory;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.item.ItemFactory;
import jewelhunt.game.player.Player;
import jewelhunt.ogl.IsoDisplay;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author Hj. Malthaner
 */
public class GameKeyHandler implements KeyHandler
{
    private static final Logger logger = Logger.getLogger(GameKeyHandler.class.getName());

    private final Game game;
    private final IsoDisplay display;
    private final GameDisplay gameDisplay;
    

    public GameKeyHandler(Game game, IsoDisplay display, GameDisplay gameDisplay)
    {
        this.game = game;
        this.display = display;
        this.gameDisplay = gameDisplay;
    }
    
    @Override
    public void processKeyboard()
    {
        // display.setShowItemNames(Keyboard.isKeyDown(Keyboard.KEY_LMENU) || Keyboard.isKeyDown(Keyboard.KEY_RMENU));

        if(Keyboard.next())
        {
            if(Keyboard.getEventKeyState() == true && !Keyboard.isRepeatEvent())
            {
                if(Keyboard.getEventKey() == Keyboard.KEY_I)
                {
                    gameDisplay.showInventory = !gameDisplay.showInventory;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_C)
                {
                    gameDisplay.showCharacter = !gameDisplay.showCharacter;
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_TAB)
                {
                    display.setShowItemNames(!display.getShowItemNames());
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_INSERT)
                {
                    makeSpecialItem();
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_M)
                {
                    testSpellEffect();
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_S)
                {
                    save();
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_L)
                {
                    load();
                }
                else if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
                {
                    display.quit();
                }
            }
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_LEFT))
        {
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
        {
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_PRIOR))
        {
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_NEXT))
        {
        }
    }

    @Override
    public boolean collectString(StringBuilder buffer)
    {
        return true;
    }

    /** for item debugging */
    private void makeSpecialItem() 
    {
        World world = game.getWorld();
        ItemFactory factory = world.itemFactory;
        
        // Item item = factory.createRandomItem(world.items, null, null, null, 5.0);
        Item item = factory.createRandomItem(world.items, null, null, null, 1.5);
        // Hajo: todo - reasonable socket limits
        factory.addSockets(item, 6);

        Player player = world.mobs.get(game.getPlayerKey());
        InventoryItem inv = new InventoryItem(item);

        player.addToInventory(inv, InventoryGrid.WIDTH, InventoryGrid.HEIGHT);
    }

    private void testSpellEffect() 
    {
        World world = game.getWorld();
        Player player = world.mobs.get(game.getPlayerKey());
        
        Damage damage = new Damage(player, 0, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0);
        Spell nova = SpellFactory.makeNovaTypeSpell(world, player, new DamageAttack(world, player, damage), new NovaFireDrawable());
        player.visuals.spells.add(nova);
    }

    private void load() 
    {
        World world = game.getWorld();
        Player player = world.mobs.get(game.getPlayerKey());

        String userHome = System.getProperty("user.home");
        File folder = new File(userHome, ".jewelhunt");

        try 
        {
            player.load(folder, world.itemFactory.itemCatalog);
        }
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    private void save() 
    {
        World world = game.getWorld();
        Player player = world.mobs.get(game.getPlayerKey());

        String userHome = System.getProperty("user.home");
        File folder = new File(userHome, ".jewelhunt");

        try 
        {
            player.save(folder);
        }
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }
}