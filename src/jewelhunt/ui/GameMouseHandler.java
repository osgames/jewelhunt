//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.ui;

import impcity.game.Features;
import java.util.logging.Logger;
import jewelhunt.game.Clock;
import jewelhunt.game.Direction;
import jewelhunt.game.Game;
import jewelhunt.game.InventoryGrid;
import jewelhunt.game.Texture;
import jewelhunt.game.World;
import jewelhunt.game.animation.Animation;
import jewelhunt.game.animation.MeleeAttackAnimation;
import jewelhunt.game.combat.Attack;
import jewelhunt.game.combat.Damage;
import jewelhunt.game.combat.DamageAttack;
import jewelhunt.game.combat.magic.NovaFireDrawable;
import jewelhunt.game.combat.magic.NovaFrostDrawable;
import jewelhunt.game.combat.magic.Spell;
import jewelhunt.game.combat.magic.SpellFactory;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.map.Map;
import jewelhunt.game.map.MapTransitions;
import jewelhunt.game.player.Player;
import jewelhunt.game.player.Species;
import jewelhunt.oal.SoundPlayer;
import jewelhunt.ogl.HotspotMap;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.uikit.MouseEvent;
import jewelhunt.uikit.Trigger;
import org.lwjgl.input.Mouse;
import rlgamekit.equipment.Equipment;


/**
 *
 * @author Hj. Malthaner
 */
public class GameMouseHandler implements MouseHandler
{
    public static final Logger logger = Logger.getLogger(GameMouseHandler.class.getName());
    
    private boolean lastButtonState = false;
    private final Game game;
    private final IsoDisplay display;
    private final SoundPlayer soundPlayer;

    private InventoryItem draggedItem;
    private final GameDisplay gameDisplay;
    private InventoryItem hoverItem;
    private int buttonPressed;
    private int buttonReleased;
    
    public GameMouseHandler(Game game, 
                            GameDisplay gameDisplay,
                            IsoDisplay display,
                            SoundPlayer soundPlayer)
    {
        this.game = game;
        this.gameDisplay = gameDisplay;
        this.display = display;
        this.soundPlayer = soundPlayer;
        
        setDefaultMousePointer();
    }
    
    @Override
    public void processMouse()
    {
        int mx = (Mouse.getX() - display.centerX - 108);
        int my = (Mouse.getY() - display.centerY - 108) * 2;

        int mmi = -mx - my;
        int mmj = mx - my;

        // System.err.println("mmi = " + mmi + " mmj = " + mmj);

        game.mouseI = mmi * Map.SUB / 216;
        game.mouseJ = mmj * Map.SUB / 216;

        display.cursorI = game.mouseI;
        display.cursorJ = game.mouseJ;

        // System.err.println("mi = " + mouseI + " mj = " + mouseJ);

        while(Mouse.next())
        {
            Player player = game.getWorld().mobs.get(game.getPlayerKey());
            
            int button = Mouse.getEventButton();
            boolean buttonState = Mouse.getEventButtonState();
            buttonPressed = 0;
            buttonReleased = 0;

            if(buttonState != lastButtonState)
            {
                if(buttonState)
                {
                    buttonPressed = button + 1;
                }
                else
                {
                    buttonReleased = button + 1;
                }
                
                lastButtonState = buttonState;
            }

            // Hajo: if the merchant dialog is closed, also negtaive x-coordinates
            // must be handed to the inventory, otherwisw the "drop item" feature
            // doesn't work (requires dragging an item left outside the dialog)
            if(gameDisplay.showInventory && 
              ((gameDisplay.showMerchant && Mouse.getX() > gameDisplay.rightDialogX) || !gameDisplay.showMerchant) &&
              ((gameDisplay.showMogrisphere && Mouse.getX() > gameDisplay.rightDialogX) || !gameDisplay.showMogrisphere))
            {
                handlePlayerInventory(button);
            }
            else if(gameDisplay.showMerchant && Mouse.getX() < gameDisplay.rightDialogX)
            {
                int money = player.calcMoney();
                handleMerchantInventory(button, money);
            }
            else if(gameDisplay.showMogrisphere && Mouse.getX() < gameDisplay.rightDialogX)
            {
                handleMogriInventory(player, button);
            }
            else if(buttonPressed == 1)
            {
                handleLeftClick(player);
            }
            else if(buttonPressed == 2)
            {
                // Hajo: No magic in towns.
                if(game.getWorld().getCurrentMap() != MapTransitions.MapId.VILLAGE_1)
                {
                    // triggerFireNova(game.getWorld(), player);
                    triggerFrostNova(game.getWorld(), player);
                }
            }
            
            if(buttonPressed == 0)
            {
                handleNoClickActions();
            }
        }
/*
        if(Mouse.isButtonDown(1))
        {
            magicTest(game.getWorld().mobs.get(game.getPlayerKey()));
        }
*/        
    }
    
    private boolean scanForMerchant(int mouseI, int mouseJ, int radius) 
    {
        for(int jj=-radius; jj<=radius; jj++)
        {
            for(int ii=-radius; ii<=radius; ii++)
            {
                int tileId = display.map.getItem(mouseI+ii, mouseJ+jj);

                // System.err.println("tileId = " + tileId);

                if(tileId == Map.DECO_GENERAL_MERCHANT)
                {
                    Player merchant = new Player(game.getWorld(), game.mouseI, game.mouseJ, Species.GLOBOS_BASE, display.map, null, 0, null);
                    fillGeneralStore(merchant);
                    gameDisplay.openMerchantDialog(merchant);
                    return true;
                }
                else if(tileId == Map.DECO_ARMORY_MERCHANT)
                {
                    Player merchant = new Player(game.getWorld(), game.mouseI, game.mouseJ, Species.GLOBOS_BASE, display.map, null, 0, null);
                    fillSmithStore(merchant);
                    gameDisplay.openMerchantDialog(merchant);
                    return true;
                }
            }
        }
        return false;
    }
    
    private Player scanForEnemy(World world, int playerKey, int mouseI, int mouseJ, int radius) 
    {
        for(int jj=-radius; jj<=radius; jj++)
        {
            for(int ii=-radius; ii<=radius; ii++)
            {
                int mobKey = display.map.getMob(mouseI+ii, mouseJ+jj);

                if(mobKey != 0 && mobKey != playerKey)
                {
                    Player mob = world.mobs.get(mobKey);
                    
                    // still alive?
                    if(mob.stats.getCurrent(Player.I_VIT) >= 0)
                    {
                        return mob;
                    }
                }
            }
        }
        return null;
    }

    private void handlePlayerInventory(int button) 
    {
        MouseEvent event = new MouseEvent(Mouse.getX() - gameDisplay.rightDialogX,
                                Mouse.getY() - gameDisplay.rightDialogY, 
                                buttonPressed, 
                                buttonReleased);

        Trigger grid = gameDisplay.inventoryDialog.triggers.get(0);
                
        int gridI;
        int gridJ;
        
        MousePointerBitmap displayMousePointer = display.getMousePointer();
        if(draggedItem == null)
        {
            gridI = (event.mouseX - grid.area.x) / 32;
            gridJ = (event.mouseY - grid.area.y) / 32;

            gameDisplay.setInventoryHighlight(grid, gridI, gridJ, 1, 1);
        }
        else
        {
            gridI = (event.mouseX - grid.area.x - displayMousePointer.grabX + 16) / 32;
            gridJ = (event.mouseY - grid.area.y - displayMousePointer.grabY + 16) / 32;

            gameDisplay.setInventoryHighlight(grid, gridI, gridJ, draggedItem.area.width, draggedItem.area.height);
        }
        
        Player player = game.getWorld().mobs.get(game.getPlayerKey());

        setItemPopup(player, gridI, gridJ, event);
        
        Trigger trigger = gameDisplay.inventoryDialog.processMouseEvent(event);
        
        if(trigger == null)
        {
            // Hajo: drop to ground?
            if(buttonPressed == 1 && draggedItem != null && Mouse.getX() < gameDisplay.rightDialogX)
            {
                int regKey = draggedItem.item.getRegistryKey();
                player.gameMap.dropItem(player.location.x, player.location.y, regKey);
                draggedItem = null;
                setDefaultMousePointer();
                soundPlayer.play(0, 1.0f);
            }
        }
        else
        {
            System.err.println("trigger=" + trigger.key);
            
            if("grid".equals(trigger.key))
            {
                handleInventoryGrid(0,
                                    player, event, trigger, 
                                    gridI, gridJ,
                                    InventoryGrid.WIDTH, InventoryGrid.HEIGHT,
                                    button);
            }
            else
            {
                handleEquipment(player.equipment, event, trigger);
            }
        }
    }

    /**
     * Way too complex method ...
     * 
     * @param playerMoney The amount of money the player currently has
     * @param owner The owner of the inventory to handle
     * @param event The triggered event
     * @param trigger The trigger for the event
     * @param gridI Inventory grid horizontal index
     * @param gridJ Inventory grid vertical index
     * @param width Inventory width
     * @param height Inventory height
     */
    private void handleInventoryGrid(int playerMoney,
                                     Player owner, MouseEvent event, Trigger trigger,
                                     int gridI, int gridJ, int width, int height,
                                     int button)
    {
        if(draggedItem == null)
        {
            if(hoverItem != null)
            {
                handleInventoryItemClick(event, trigger, owner, playerMoney, button);
            }
        }
        else
        {
            if(gridI >= 0 && 
               gridJ >= 0 && 
               gridI + draggedItem.area.width <= width && 
               gridJ + draggedItem.area.height <= height)
            {
                draggedItem.area.x = gridI;
                draggedItem.area.y = gridJ;

                InventoryItem overlappedItem = null;


                // Hajo: check for intersecting items

                // Hajo: if there is only one intersecting item we can switch
                //       if there are more than one, we can't drop
                //       if there are non, we can drop immediately

                int intersections = 0;

                for(InventoryItem item : owner.inventory)
                {
                    if(item.area.intersects(draggedItem.area))
                    {
                        intersections ++;
                        overlappedItem = item;
                    }
                }

                if(intersections == 0)
                {
                    owner.inventory.add(draggedItem);

                    if(owner == gameDisplay.currentMerchant)
                    {
                        sellUsedItem(draggedItem);
                    }                    
                    
                    draggedItem = null;
                    setDefaultMousePointer();
                    
                    soundPlayer.play(0, 1.0f);
                }
                else if(intersections == 1)
                {
                    // keep IDE happy
                    if(overlappedItem != null)
                    {
                        // Hajo: gem drop on socketed item?
                        if("gem".equals(draggedItem.item.getItemClass()) &&
                            overlappedItem.item.getSocketCount() > 0 &&
                            owner.getKey() == game.getPlayerKey())
                        {
                            handleSocketDrop(event, gridI, gridJ, overlappedItem);
                        }  // Hajo: drop on stack? try to add
                        else if(overlappedItem.item.getName().equals(draggedItem.item.getName()) &&
                           overlappedItem.item.getCurrentStackSize() < overlappedItem.item.getMaxStackSize())
                        {
                            int count = overlappedItem.item.getCurrentStackSize();
                            count += draggedItem.item.getCurrentStackSize();
                            overlappedItem.item.setCurrentStackSize(count);

                            if(owner == gameDisplay.currentMerchant)
                            {
                                sellUsedItem(draggedItem);
                            }                    
                            
                            draggedItem = null;
                            setDefaultMousePointer();
                        }
                        else
                        {
                            boolean ok = true;
                            
                            if(owner == gameDisplay.currentMerchant)
                            {
                                // too expensive?
                                if(hoverItem.item.calcSalesPrice(1.5) > playerMoney) ok = false;
                            }

                            if(ok)
                            {
                                if(owner == gameDisplay.currentMerchant)
                                {
                                    sellUsedItem(draggedItem);
                                    buyNewItem(overlappedItem);
                                }                    

                                // swap items
                                owner.inventory.remove(overlappedItem);
                                owner.inventory.add(draggedItem);

                                draggedItem = overlappedItem;
                                display.getMousePointer().tex = 
                                        display.textureCache.textures[draggedItem.item.getInventoryImageIndex()];
                            }
                            else
                            {
                                showTooExpensiveMessage();
                            }
                        }
                    }
                    
                    soundPlayer.play(0, 1.0f);
                }
            }
        }
    }

    private void handleEquipment(Equipment<Item> equipment, MouseEvent event, Trigger trigger)
    {
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            String slot = equipment.slotNames[i];

            // System.err.println("testing slot=" + slot);
            if(slot.equals(trigger.key))
            {
                System.err.println("equipment slot=" + slot);

                if (draggedItem == null)
                {
                    Item item = equipment.getItem(i);
                    if(item != null)
                    {
                        draggedItem = new InventoryItem(item);
                        equipment.setItem(i, null);
                        soundPlayer.play(1, 1.0f);
                    }
                }
                else
                {
                    // Hajo: check slot constraints
                    String itemClass = slot.toLowerCase();
                    if("weapon".equals(itemClass))
                    {
                        itemClass = "blade, axe, mace";
                    }
                    else if("leg armor".equals(itemClass))
                    {
                        itemClass = "legwear";
                    }
                    
                    if(itemClass.contains(draggedItem.item.getItemClass()))
                    {
                        Item oldItem = equipment.getItem(i);

                        equipment.setItem(i, draggedItem.item);
                        soundPlayer.play(0, 1.0f);

                        if(oldItem == null)
                        {
                            draggedItem = null;
                        }
                        else
                        {
                            draggedItem = new InventoryItem(oldItem);
                        }
                    }
                    else
                    {
                        // Hajo: could not drop, so nothing should
                        // be changed
                        return;
                    }
                }
                
                if(draggedItem == null)
                {
                    setDefaultMousePointer();
                }
                else
                {
                    setCustomMousePointer(event, trigger);
                }
                break;
            }
        }
    }

    private void setItemPopup(Player player, int gridI, int gridJ, MouseEvent event) 
    {
        InventoryItem nextHoverItem = null;
        
        for(InventoryItem item : player.inventory)
        {
            if(item.area.x <= gridI && item.area.x + item.area.width > gridI &&
               item.area.y <= gridJ && item.area.y + item.area.height > gridJ)
            {
                nextHoverItem = item;
                break;
            }
        }

        if(nextHoverItem == null)
        {
            for(Trigger trigger : gameDisplay.inventoryDialog.triggers)
            {
                if(trigger.area.contains(event.mouseX, event.mouseY))
                {
                    for(int i=0; i<player.equipment.slotNames.length; i++)
                    {
                        String slot = player.equipment.slotNames[i];

                        if(slot.equals(trigger.key))
                        {
                            Item item = player.equipment.getItem(i);
                            if(item != null)
                            {
                                nextHoverItem = new InventoryItem(item);
                            }
                            break;
                        }
                    }
                }
            }
        }
        
        if(draggedItem == null && nextHoverItem != null)
        {
            if(hoverItem == null)
            {
                hoverItem = nextHoverItem;
                gameDisplay.setItemForPopup(hoverItem, Mouse.getX(), Mouse.getY());
            }
            else if(hoverItem.item != nextHoverItem.item)
            {
                hoverItem = nextHoverItem;
                gameDisplay.setItemForPopup(hoverItem, Mouse.getX(), Mouse.getY());
            }
        }
        else
        {
            hoverItem = null;
            gameDisplay.setItemForPopup(null, Mouse.getX(), Mouse.getY());
        }
    }


    private void setDefaultMousePointer()
    {
        MousePointerBitmap mp = new MousePointerBitmap();
        mp.tex = display.textureCache.textures[Features.CURSOR_HAND];
        mp.grabX = 0;
        mp.grabY = mp.tex.image.getHeight()-1;
        mp.hue = 0xFFFFFFFF;
        display.setMousePointer(mp);
    }

    private void setCustomMousePointer(MouseEvent event, Trigger trigger)
    {
        int n = draggedItem.item.getInventoryImageIndex();
        int count = draggedItem.item.getCurrentStackSize();
        Texture tex = display.textureCache.getStackTexture(n, count);
        
        MousePointerBitmap mp = new MousePointerBitmap();
        mp.tex = tex;
        mp.grabX = event.mouseX - trigger.area.x - draggedItem.area.x * 32;
        mp.grabY = event.mouseY - trigger.area.y - draggedItem.area.y * 32 - 4;
        mp.hue = draggedItem.item.getHue();
        display.setMousePointer(mp);
    }

    private void handleNoClickActions()
    {
        int mx = Mouse.getX();
        int my = Mouse.getY();
        
        // lower bar
        if(my < 120)
        {
            if(mx < 200)
            {
                if(!gameDisplay.showMerchant)
                {
                    gameDisplay.showCharacter = true;
                }
            }
            if(mx > display.displayWidth - 200)
            {
                gameDisplay.showInventory = true;
            }
        }
        else
        {
            // above lower bar
            if(mx < 80)
            {
                gameDisplay.showCharacter = false;
                gameDisplay.showMerchant = false;
            }
            if(mx > display.displayWidth - 80)
            {
                gameDisplay.showInventory = false;
                gameDisplay.showMerchant = false;
                gameDisplay.showMogrisphere = false;
            }
            
        }
    }

    private void fillSmithStore(Player merchant) 
    {
        logger.entering("GameMouseHandler", "fillSmithStore");
    
        makeItems(merchant, 4, null, "boot", "metal", 0.8);
        makeItems(merchant, 4, null, "glove", "metal", 0.8);
        makeItems(merchant, 4, null, "legwear", "metal", 0.8);
        makeItems(merchant, 4, null, "shield", "metal", 0.8);
        makeItems(merchant, 4, null, "armor", "metal", 0.8);
        makeItems(merchant, 4, null, "helm", "metal", 0.8);
        makeItems(merchant, 4, null, "axe", null, 0.8);
        makeItems(merchant, 4, null, "mace", "metal", 0.8);
        makeItems(merchant, 4, null, "blade", "metal", 0.8);
    }

    private void fillGeneralStore(Player merchant) 
    {
        logger.entering("GameMouseHandler", "fillGeneralStore");
    
        makeItems(merchant, 4, null, "boot", "leather", 0.8);
        makeItems(merchant, 4, null, "glove", "leather", 0.8);
        makeItems(merchant, 4, null, "legwear", "leather", 0.8);
        makeItems(merchant, 4, null, "shield", "leather", 0.8);
        makeItems(merchant, 4, null, "shield", "wood", 0.8);
        makeItems(merchant, 4, null, "armor", "leather", 0.8);
        makeItems(merchant, 4, null, "helm", "leather", 0.8);
        makeItems(merchant, 4, null, "mace", "wood", 0.8);
        makeItems(merchant, 4, null, "instrument", "wood", 0.8);
    }
    
    private void makeItems(Player merchant, int n, String treasureClass, String itemClass, String material, double magic)
    {
        World world = game.getWorld();
        
        for(int i=0; i<n; i++)
        {
            Item item = world.itemFactory.createRandomItem(world.items, treasureClass, itemClass, material, magic);
            
            if(item != null)
            {
                item.setCurrentDurability(item.getBaseDurability());
                InventoryItem invItem = new InventoryItem(item);        
                merchant.addToInventory(invItem, InventoryGrid.MERCHANT_WIDTH, InventoryGrid.MERCHANT_HEIGHT);            
            }
        }
    }
    
    private void handleMerchantInventory(int button, int playerMoney) 
    {
        MouseEvent event = new MouseEvent(Mouse.getX() - gameDisplay.leftDialogX,
                                Mouse.getY() - gameDisplay.leftDialogY, 
                                buttonPressed, 
                                buttonReleased);

        Trigger grid = gameDisplay.merchantDialog.triggers.get(0);
                
        int gridI;
        int gridJ;
        
        MousePointerBitmap displayMousePointer = display.getMousePointer();
        if(draggedItem == null)
        {
            gridI = (event.mouseX - grid.area.x) / 32;
            gridJ = (event.mouseY - grid.area.y) / 32;

            gameDisplay.setInventoryHighlight(grid, gridI, gridJ, 1, 1);
        }
        else
        {
            gridI = (event.mouseX - grid.area.x - displayMousePointer.grabX + 16) / 32;
            gridJ = (event.mouseY - grid.area.y - displayMousePointer.grabY + 16) / 32;

            gameDisplay.setInventoryHighlight(grid, gridI, gridJ, draggedItem.area.width, draggedItem.area.height);
        }
        
        setItemPopup(gameDisplay.currentMerchant, gridI, gridJ, event);
        
        Trigger trigger = gameDisplay.merchantDialog.processMouseEvent(event);
        
        if(trigger == null)
        {
            // something to do here?
        }
        else
        {
            System.err.println("trigger=" + trigger.key);
            
            if("merchantgrid".equals(trigger.key))
            {
                handleInventoryGrid(playerMoney,
                                    gameDisplay.currentMerchant, 
                                    event, trigger, 
                                    gridI, gridJ, 
                                    InventoryGrid.MERCHANT_WIDTH, InventoryGrid.MERCHANT_HEIGHT,
                                    button);
            }
        }
    }

    private void handleMogriInventory(Player player, int button)
    {
        MouseEvent event = new MouseEvent(Mouse.getX() - gameDisplay.leftDialogX,
                                Mouse.getY() - gameDisplay.leftDialogY, 
                                buttonPressed, 
                                buttonReleased);

        Trigger grid = gameDisplay.mogrisphereDialog.triggers.get(0);
                
        int gridI;
        int gridJ;
        
        MousePointerBitmap displayMousePointer = display.getMousePointer();
        if(draggedItem == null)
        {
            gridI = (event.mouseX - grid.area.x) / 32;
            gridJ = (event.mouseY - grid.area.y) / 32;

            gameDisplay.setInventoryHighlight(grid, gridI, gridJ, 1, 1);
        }
        else
        {
            gridI = (event.mouseX - grid.area.x - displayMousePointer.grabX + 16) / 32;
            gridJ = (event.mouseY - grid.area.y - displayMousePointer.grabY + 16) / 32;

            gameDisplay.setInventoryHighlight(grid, gridI, gridJ, draggedItem.area.width, draggedItem.area.height);
        }
        
        // Todo
        // setItemPopup(player, gridI, gridJ, event);
        
        Trigger trigger = gameDisplay.mogrisphereDialog.processMouseEvent(event);
        
        if(trigger == null)
        {
            // something to do here?
        }
        else
        {
            System.err.println("trigger=" + trigger.key);
            
            if("grid".equals(trigger.key))
            {
                handleInventoryGrid(0,
                                    player, 
                                    event, trigger, 
                                    gridI, gridJ, 
                                    InventoryGrid.MERCHANT_WIDTH, InventoryGrid.MERCHANT_HEIGHT,
                                    button);
            }
        }
    }

    private void sellUsedItem(InventoryItem draggedItem)
    {
        Item item = draggedItem.item;
        Player player = game.getWorld().mobs.get(game.getPlayerKey());
        
        int price = item.calcSalesPrice(item.calcSellUsedFactor());
        player.addMoney(price);
        
        // Hajo: sold items become repaired, merchants only keep
        // good items for sale. Does this code belong here?
        item.setCurrentDurability(item.getBaseDurability());
    }
    
    private void buyNewItem(InventoryItem draggedItem)
    {
        Item item = draggedItem.item;
        Player player = game.getWorld().mobs.get(game.getPlayerKey());
        
        int price = item.calcSalesPrice(1.5);
        player.subMoney(price);
    }

    private void showTooExpensiveMessage()
    {
        TimedMessage message = 
                new TimedMessage(
                    "Not enough money!",
                    0xFF8833,
                    Mouse.getX(),
                    Mouse.getY(),
                    Clock.time()
                );
        gameDisplay.addMessage(message);
    }

    private void magicTest(Player mob)
    {
        for(int n=1; n < 5; n++)
        {
            int particle = 1994 + (int)(Math.random() * 7);
            int xoff = (int)(Math.random() * 30);
            int yoff = (int)(Math.random() * 20) - 10;
            yoff += (int)(Math.random() * 20) - 10;
            
            mob.visuals.frontParticles.addParticle(xoff - Math.abs(yoff), 15 + yoff/2, 7, 0, 70, particle, 0xFFFFFFFF);
        }
    }

    private void handleLeftClick(Player player) 
    {
        HotspotMap.Hotspot hotSpot = display.hotspotMap.findHotspot(Mouse.getX(), Mouse.getY());
        
        if(hotSpot != null)
        {
            game.mouseI = hotSpot.mapI;
            game.mouseJ = hotSpot.mapJ;
            game.walkPlayer();
        }
        else
        {
            boolean merchantOpen = false;

            // Hajo: only open if placer is close enough
            if(Math.abs(player.location.x - game.mouseI) < 12 && Math.abs(player.location.y - game.mouseJ) < 12)
            {
                merchantOpen = scanForMerchant(game.mouseI, game.mouseJ, 2);                
            }

            if(!merchantOpen)
            {
                int dx = Math.abs(game.mouseI - player.location.x);
                int dy = Math.abs(game.mouseJ - player.location.y);

                if(dx + dy > 6) // todo: reach
                {
                    game.walkPlayer();
                }
                else
                {
                    Player mob = scanForEnemy(game.getWorld(), player.getKey(), game.mouseI, game.mouseJ, 2);
                    if(mob == null)
                    {
                        game.walkPlayer();
                    }
                    else
                    {
                        // Hajo: turn towards enemy

                        dx = (int)Math.signum(mob.location.x - player.location.x);
                        dy = (int)Math.signum(mob.location.y - player.location.y);

                        player.visuals.setDisplayCode(player.getSpecies() + Direction.dirFromVector(dx, dy));

                        Damage damage = player.calculateTotalDamage();
                        Attack attack = new DamageAttack(game.getWorld(), player, damage);

                        Animation animation = new MeleeAttackAnimation(player, attack, mob);
                        player.visuals.animation = animation;
                    }
                }
            }
        }
    }
    
    private void triggerFireNova(World world, Player player) 
    {
        Damage damage = new Damage(player, 0, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0);
        Spell nova = SpellFactory.makeNovaTypeSpell(world, player, 
                                                    new DamageAttack(world, player, damage), 
                                                    new NovaFireDrawable());
        player.visuals.spells.add(nova);
        
        int mana = player.stats.getCurrent(Player.I_MANA);
        mana -= nova.cost() << 8;
        player.stats.setCurrent(Player.I_MANA, mana);
    }
    
    private void triggerFrostNova(World world, Player player) 
    {
        Damage damage = new Damage(player, 0, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0);
        Spell nova = SpellFactory.makeNovaTypeSpell(world, player, 
                                                    new DamageAttack(world, player, damage),
                                                    new NovaFrostDrawable());
        player.visuals.spells.add(nova);
        
        int mana = player.stats.getCurrent(Player.I_MANA);
        mana -= nova.cost() << 8;
        player.stats.setCurrent(Player.I_MANA, mana);
    }

    private void handleSocketDrop(MouseEvent event, int gridI, int gridJ, 
                                  InventoryItem overlappedItem) 
    {
        Trigger grid = gameDisplay.inventoryDialog.triggers.get(0);        
        
        int rmx = event.mouseX - grid.area.x - overlappedItem.area.x * 32;
        int rmy = event.mouseY - grid.area.y - overlappedItem.area.y * 32;
        
        // Hajo: grid has mirrored y axis
        // rmy = overlappedItem.area.height * 32 - rmy;
        
        int socket = overlappedItem.calcSocketIndex(rmx, rmy);
        if(socket >= 0)
        {
            overlappedItem.item.setSocketItem(socket, draggedItem.item);
            draggedItem = null;
            setDefaultMousePointer();
        }
    }

    private void handleInventoryItemClick(MouseEvent event, Trigger trigger, Player owner, int playerMoney, int button) 
    {
        if(button == 0)
        {
            // Hajo: left mouse button -> take the item
            boolean ok = true;

            if(owner == gameDisplay.currentMerchant)
            {
                // too expensive?
                if(hoverItem.item.calcSalesPrice(1.5) > playerMoney) ok = false;
            }

            if(ok)
            {
                owner.inventory.remove(hoverItem);
                draggedItem = hoverItem;
                buyNewItem(draggedItem);
                setCustomMousePointer(event, trigger);
                soundPlayer.play(1, 1.0f);
            }
            else
            {
                showTooExpensiveMessage();
            }
        }
        else
        {
            // Hajo: right mouse button -> activate the item
            activateItem(hoverItem);
        }
    }

    private void activateItem(InventoryItem item) 
    {
        if("mogrisphere".equals(item.item.getKey()))
        {
            gameDisplay.openMogrispeherDialog();
        }
    }
}    
