//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.uikit;

/**
 *
 * @author Hj. Malthaner
 */
public interface ActionCallback 
{
    public void activate(MouseEvent event);
}
