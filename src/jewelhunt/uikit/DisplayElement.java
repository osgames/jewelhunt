//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.uikit;

import java.awt.Rectangle;
import jewelhunt.ui.PixFont;

/**
 *
 * @author Hj. Malthaner
 */
public class DisplayElement 
{
    public PixFont font;
    public final Rectangle area;
    public String value = "";
    
    public String key;

    public DisplayElement()
    {
        area = new Rectangle();
    }
    
    void display(int xpos, int ypos) 
    {
    }

    void handleMouseEvent(MouseEvent event)
    {
    }
    
    void handleKeyEvent(KeyEvent event)
    {
    }
}
