//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.uikit;

/**
 *
 * @author Hj. Malthaner
 */
class KeyEvent 
{
    public final char key;
    public final int code;

    KeyEvent(char key, int code) 
    {
        this.key = key;
        this.code = code;
    }
    
}
