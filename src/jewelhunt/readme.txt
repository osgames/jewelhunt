The Hunt for the lost Rainbow Jewels
-----------------------------------------

The rainbow jewels have been abducted from the temple of the rainbow colors, and the world will slowly fade to gray if you can't retrieve them.

The project is in alpha stage, so there is only proof of concept code, no real game yet.

The current release is mostly a walkaround demo. There is the village map, where you can visit vendors to trade items, the village underground, inhabitated by imps, who do not like visitors from above, an early version of the rainbow temple map, west of the village and an early desert map with a pyramid, east of the village.

You can't enter the temple or the pyramid, both locations have no interior maps yet.

The underground map has items distributed randomly which you can pick up by clicking them. Pressing TAB will toggle name labels to be displayed on the map. The labels make it easier to spot items on the ground.

There are two UI previews included, one for the character sheet, and one for the inventory. You can toggle the character sheet by pressing 'c', and the inventory by pressing 'i'.
	
The character sheet is displays most stats correcty, but you can't alter the basic stats like intelligence, strength, dexterity and wisdom yet. You can alter the other stats by wielding and taking off magical equipment, though.

The inventory should be fully functional.

Project site:
http://sourceforge.net/projects/jewelhunt/

Secret hint: Outside the village map, pressing the right mouse button will cast a frost nova spell.

Keyboard Shortcuts
-------------------

I: Toggle inventory dialog
C: Toggle character sheet
TAB: Toggle map item labels
ESC: Quit

Known Bugs
-----------

- The light effect sometimes stays after picking up a rainbow jewel.

