//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.net;

import jewelhunt.game.map.Map;

/**
 * Maybe there will be a multiplayer expansion some day ...
 * 
 * @author Hj. Malthaner
 */
public interface Server 
{
    public void shutdown();
    
    /** 
     * Move player to destination coordinates.
     * 
     * @param x Player x position on map
     * @param y Player y position on map 
     */
    public void moveTo(int playerKey, int x, int y);
    
    /** 
     * Player move had to stop at (x, y).
     * Server must sync player on all clients to (x,y)
     * 
     * @param x Player x position on map
     * @param y Player y position on map 
     */
    public void arriveAt(int playerKey, int x, int y);
    
    public void runJobs();

    /**
     * Lod named map from server
     * @param mapName The name of the map to load
     * @return 
     */
    public Map loadMap(int playerKey, String mapName);
}
