//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.net.client;

import java.nio.ByteBuffer;
import rlgamekit.pathfinding.Path;

/**
 * Maybe there will be a multiplayer extension some day ...
 * 
 * @author Hj. Malthaner
 */
public class ClientCommand 
{
    public final static byte MOVE_TO = 1;
    public final static byte ARRIVE_AT = 2;

    public static ClientCommand moveTo(int mobId, Path path) 
    {
        ByteBuffer buffer = ByteBuffer.allocate(1 + 4 + 4 * path.length());
        buffer.put(MOVE_TO);
        buffer.putInt(mobId);
        
        Path.Node node;
        
        while((node = path.currentStep()) != null)
        {
            buffer.putShort((short)node.x);
            buffer.putShort((short)node.y);
            path.advance();
        }
        
        return new ClientCommand(buffer);
    }

    public static ClientCommand arriveAt(int mobId, int x, int y) 
    {
        ByteBuffer buffer = ByteBuffer.allocate(1 + 4 + 4);
        buffer.put(ARRIVE_AT);
        
        buffer.putInt(mobId);
        buffer.putShort((short)x);
        buffer.putShort((short)y);

        return new ClientCommand(buffer);
    }

    public final ByteBuffer buffer;

    private ClientCommand(ByteBuffer data)
    {
        this.buffer = data;
    }
    
    public byte getCommand()
    {
        return buffer.get(0);
    }
}
