//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.net.client;

import java.nio.ByteBuffer;
import jewelhunt.game.player.Player;
import rlgamekit.objects.Registry;
import rlgamekit.pathfinding.Path;

/**
 *
 * @author Hj. Malthaner
 */
public class ClientCommandProcessor 
{
    private final Registry<Player> mobs;

    
    public ClientCommandProcessor(Registry<Player> mobs)
    {
        this.mobs = mobs;
    }
    
    
    public void process(ClientCommand command)
    {
        switch(command.getCommand())
        {
            case ClientCommand.MOVE_TO:
                moveTo(command);
                break;
            case ClientCommand.ARRIVE_AT:
                arriveAt(command);
                break;
        }
    }

    private void moveTo(ClientCommand command) 
    {
        ByteBuffer buffer = command.buffer;
        buffer.rewind();
        buffer.get();       // Hajo: skip command type byte
        
        int mobId = buffer.getInt();
        
        Path path = new Path();
        
        while(buffer.position() < buffer.limit())
        {
            short x = buffer.getShort();
            short y = buffer.getShort();
            
            path.addStep(x, y);
        }
        
        Player mob = mobs.get(mobId);
        
        mob.setPath(path);
    }

    private void arriveAt(ClientCommand command) 
    {
        ByteBuffer buffer = command.buffer;
        buffer.rewind();
        buffer.get();       // Hajo: skip command type byte
        
        int mobId = buffer.getInt();
        short x = buffer.getShort();
        short y = buffer.getShort();

        Path path = new Path();
        path.addStep(x, y);
        
        Player mob = mobs.get(mobId);
        
        mob.setPath(path);
    }
}
