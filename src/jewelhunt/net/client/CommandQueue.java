//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.net.client;

import java.util.LinkedList;

/**
 *
 * @author Hj. Malthaner
 */
public class CommandQueue 
{
    private final LinkedList <ClientCommand> queue = new LinkedList<ClientCommand>();


    public synchronized void append(ClientCommand command)
    {
        queue.addLast(command);
    }
    
    public synchronized ClientCommand pop()
    {
        ClientCommand command = queue.removeFirst();
        return command;
    }
}
