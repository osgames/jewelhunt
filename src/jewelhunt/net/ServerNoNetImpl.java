//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.net;

import jewelhunt.net.client.ClientCommand;
import jewelhunt.net.client.ClientCommandProcessor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import jewelhunt.game.map.Map;
import jewelhunt.game.player.Player;
import jewelhunt.net.server.FindPath;
import jewelhunt.net.server.Job;
import jewelhunt.game.map.DefaultPathSource;
import rlgamekit.objects.Registry;

/**
 * Single player server implementation.
 * 
 * @author Hj. Malthaner
 */
public class ServerNoNetImpl implements Server
{
    private final Map gameMap;
    private final Registry<Player> mobs;
    private final ArrayList <Job> jobQueue;
    private final List <Job> runningJobs;
    private final ClientCommandProcessor processor;
    
    private final ExecutorService pool;
    
    public ServerNoNetImpl(Map gameMap, Registry<Player> mobs)
    {
        this.mobs = mobs;
        this.gameMap = gameMap;
        this.jobQueue = new ArrayList <Job> ();
        
        this.pool = Executors.newFixedThreadPool(8);
        
        this.runningJobs = Collections.synchronizedList(new ArrayList <Job>());
        this.processor = new ClientCommandProcessor(mobs);
    }
    
    public void shutdown()
    {
        pool.shutdownNow();
    }
    
    @Override
    public void moveTo(int playerKey, int x, int y) 
    {
        Player player = mobs.get(playerKey);
                
        if(playerKey == 1)
        {
            System.err.println("Moving player #" + playerKey + " from " + 
                                player.location.x + ", " + player.location.y + " to " +
                                x + ", " + y);
        }
        
        // Hajo: sanity check for destination ... don't search paths for
        // blocked destinations
        
        DefaultPathSource check = new DefaultPathSource(gameMap);
        
        if(check.isMoveAllowed(x, y, x, y))
        {
            Job findPath = new FindPath(playerKey, player.gameMap,
                                        player.location.x, player.location.y,
                                        x, y);
            jobQueue.add(findPath);
            Thread.yield();
        }
        else
        {
            // Hajo: we don't move, destination is blocked
        }
    }

    @Override
    public void arriveAt(int playerKey, int x, int y) 
    {
        ClientCommand command = ClientCommand.arriveAt(playerKey, x, y);
        processor.process(command);
    }

    @Override
    public void runJobs() 
    {
        for(Job job : runningJobs)
        {
            if(job.isDone())
            {
                if(job instanceof FindPath)
                {
                    FindPath findPath = (FindPath)job;
                    Player mob = mobs.get(findPath.mobKey);

                    // still alive?
                    if(mob != null)
                    {
                        if(findPath.ok)
                        {
                            // mob.setPath(findPath.path);
                            
                            ClientCommand command = ClientCommand.moveTo(mob.getKey(), findPath.path);
                            processor.process(command);
                        }
                        else
                        {
                            arriveAt(mob.getKey(), mob.location.x, mob.location.y);
                        }
                    }
                }
            }
        }

        // Hajo: remove finished jobs
        
        for(int i=runningJobs.size() - 1; i >= 0; i--)
        {
            Job job = runningJobs.get(i);
            if(job.isDone()) 
            {
                runningJobs.remove(i);
            }
        }
        
        
        while(jobQueue.size() > 0)
        {
            Job job = jobQueue.remove(0);
            runningJobs.add(job);
            pool.submit(job);
        }
    }

    
    /**
     * Load named map from server
     * @param mapName The name of the map to load
     * @return 
     */
    @Override
    public Map loadMap(int playerKey, String mapName)
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
