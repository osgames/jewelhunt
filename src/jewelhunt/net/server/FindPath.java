//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.net.server;

import jewelhunt.game.map.DefaultPathSource;
import jewelhunt.game.map.Map;
import rlgamekit.pathfinding.Path;
import rlgamekit.pathfinding.PathDestination;

/**
 *
 * @author Hj. Malthaner
 */
public class FindPath implements Job
{
    public final int mobKey;
    public final Path path;
    public final Map gameMap;
    
    public boolean ok;
    private boolean done;
    private int sx;
    private int sy;
    private int dx;
    private int dy;
    
    public FindPath(int mobKey, Map gameMap, int sx, int sy, int dx, int dy)
    {
        this.mobKey = mobKey;
        this.gameMap = gameMap;
        this.path = new Path();
        this.sx = sx;
        this.sy = sy;
        this.dx = dx;
        this.dy = dy;
        
        this.done = false;
        
        // path.setUseDiagonals(false);
    }

    
    
    @Override
    public void run() 
    {
        ok = path.findPath(new DefaultPathSource(gameMap), new PathDestImpl(dx, dy), sx, sy);
        done = true;
    }

    @Override
    public boolean isDone() 
    {
        return done;
    }
    
    
    private static class PathDestImpl implements PathDestination
    {
        final int x, y;
        
        public PathDestImpl(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        
        @Override
        public boolean isDestinationReached(int dx, int dy) 
        {
            return x == dx && y == dy;
        }
        
        /**
         * Give an estimation of the remaining path length. It is better to 
         * underestimate and 0 is an allowed value. Negative values are not
         * allowed results.
         * 
         * @param cx Current x coordinate
         * @param cy Current y coordinate 
         * @return 
         */
        @Override
        public int estimateRest(int cx, int cy) 
        {
            int xd = Math.abs(cx - x);
            int yd = Math.abs(cy - y);

            return Math.max(xd, yd);
        }
    }
}
