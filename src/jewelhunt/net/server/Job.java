//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.net.server;

/**
 *
 * @author Hj. Malthaner
 */
public interface Job extends Runnable
{

    public boolean isDone();
    
}
