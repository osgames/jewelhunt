//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.ai.EnemyAi;
import jewelhunt.game.ai.SimpleAi;
import jewelhunt.game.item.Item;
import jewelhunt.game.item.ItemFactory;
import jewelhunt.game.map.ClearDeco;
import jewelhunt.game.map.ClearItems;
import jewelhunt.game.map.DefaultPathSource;
import jewelhunt.game.map.Map;
import jewelhunt.game.map.MapTransitions;
import jewelhunt.game.map.RectArea;
import jewelhunt.game.map.SubdivisionGenerator;
import jewelhunt.game.player.MovementJumping;
import jewelhunt.game.player.Player;
import jewelhunt.game.player.Species;
import jewelhunt.ogl.IsoDisplay;
import jewelhunt.ogl.Light;
import jewelhunt.ui.Colors;
import rlgamekit.map.data.LayeredMap;
import rlgamekit.map.generators.Dungeon;
import rlgamekit.map.generators.MapUtils;
import rlgamekit.map.generators.WildernessGenerator;
import rlgamekit.objects.ArrayRegistry;
import rlgamekit.objects.Registry;
import rlgamekit.pathfinding.Area;

/**
 * The game world.
 * 
 * @author Hj. Malthaner
 */
public class World
{
    private static final Logger logger = Logger.getLogger(World.class.getName());
    
    private static final int MAP_LAYERS = 3;

    private static final MapTransitions mapTransitions = new MapTransitions();

    public final Registry<Player> mobs;
    public final Registry<Item> items;

    public final ItemFactory itemFactory;
    
    public World()
    {
        mobs = new ArrayRegistry<Player> (1 << 12);
        items = new ArrayRegistry<Item> (1 << 16);
        itemFactory = new ItemFactory();
    }
    
    void makeImpDungeon(TextureCache textureCache, Player player) 
    {
        Dungeon dungeon = makeDungeonMap(player);
        
        final LayeredMap lmap = dungeon.generate(50, 50);
        
        int w = lmap.getWidth();
        int h = lmap.getHeight();
        double torchChance = 0.3;
        
        // Hajo: transform map
        
        Map map = new Map(16, 16);
        map.newMap(w * Map.SUB, h*Map.SUB);
        
        int roomFloor = 64;
        int corridorFloor = 10;
        
        convertMap(textureCache, lmap, map, 534, torchChance, roomFloor, corridorFloor);
        
        // distributeMushrooms(map, 0.001);
        
        distributeItems(map, 0.0001);
        
        player.location.x = lmap.getSpawnX() * Map.SUB;
        player.location.y = lmap.getSpawnY() * Map.SUB;
        
        mobs.clear();
        mobs.put(player.getKey(), player);
        map.setMob(player.location.x, player.location.y, player.getKey());
        
        distributeMonsters(map, player);
        
        // Hajo: Place the stairs up to the village
        map.setItem(player.location.x-4, player.location.y-4, Map.F_DECO + Map.FEAT_STAIRS_UP_NARROW);
        map.setPlacementBlockedRadius(player.location.x-4, player.location.y-4, 6, true);
        map.setMovementBlockedRadius(player.location.x-4, player.location.y-4, 2, true);
        
        // Hajo: make sure there is at least one well.
        map.setItem(player.location.x+7, player.location.y+7, Map.F_DECO + Map.FEAT_SMALL_WELL);

        ArrayList <Rectangle> rooms = dungeon.getRooms();
        Rectangle bestRoom = rooms.get(0);
        int bestDist = 0;
        for(Rectangle p : rooms)
        {
            int dx = player.location.x - p.x * Map.SUB;
            int dy = player.location.y - p.y * Map.SUB;
            int d2 = dx * dx + dy * dy;
            
            if(d2 > bestDist)
            {
                bestRoom = p;
                bestDist = d2;
            }
        }
        
        int jewelX = (bestRoom.x + bestRoom.width / 2) * Map.SUB;
        int jewelY = (bestRoom.y + bestRoom.height / 2) * Map.SUB;
        
        Item item = itemFactory.createItem("violet_rainbow_jewel", items);
        map.dropItem(jewelX, jewelY, item.getRegistryKey());
        map.lights.add(new Light(jewelX, jewelY, 0, 2, 0xFFCC44FF, 2));

        RectArea rectArea = new RectArea(jewelX, jewelY, 0, 0);
        rectArea.spirallyTraverse(new ClearDeco(map), 6);

        // debug
       
        /*
        map.setMob(player.location.x, player.location.y, 0);
        player.location.x = jewelX;
        player.location.y = jewelY;
        map.setMob(player.location.x, player.location.y, player.getKey());
       */
        
        // Hajo: remove items which might overlap imp lairs        
        clearFeatureAreas(map);
        
        player.gameMap.copyFrom(map);
        player.gameMap.darkness = 0xDD000000;
    }

    void makePyramidDungeon(TextureCache textureCache, Player player) 
    {
        ArrayList <Rectangle> rooms = new ArrayList<Rectangle>(16);
        LayeredMap lmap = makePyramidMap(rooms, 50, 50);
        
        int width = lmap.getWidth();
        int height = lmap.getHeight();
        
        // Hajo: transform map
        
        Map map = new Map(16, 16);
        map.newMap(width * Map.SUB, height*Map.SUB);
        
        int roomFloor = 55;
        int corridorFloor = 58;
        int wallSet = 518;
        double torchChance = 0.3;
        
        convertMap(textureCache, lmap, map, wallSet, torchChance, roomFloor, corridorFloor);
        
        distributeItems(map, 0.0001);
        
        player.location.x = lmap.getSpawnX() * Map.SUB;
        player.location.y = lmap.getSpawnY() * Map.SUB;
        
        mobs.clear();
        mobs.put(player.getKey(), player);
        map.setMob(player.location.x, player.location.y, player.getKey());
        
        // distributeMonsters(map, player);
        
        // Hajo: Place the stairs up to the village
        map.setItem(player.location.x-4, player.location.y-4, Map.F_DECO + Map.FEAT_STAIRS_UP_NARROW);
        
        Rectangle bestRoom = rooms.get(0);
        int bestDist = 0;
        for(Rectangle p : rooms)
        {
            int dx = player.location.x - p.x * Map.SUB;
            int dy = player.location.y - p.y * Map.SUB;
            int d2 = dx * dx + dy * dy;
            
            if(d2 > bestDist)
            {
                bestRoom = p;
                bestDist = d2;
            }
        }
        
        int jewelX = (bestRoom.x + bestRoom.width / 2) * Map.SUB;
        int jewelY = (bestRoom.y + bestRoom.height / 2) * Map.SUB;
        
        Item item = itemFactory.createItem("violet_rainbow_jewel", items);
        map.dropItem(jewelX, jewelY, item.getRegistryKey());
        map.lights.add(new Light(jewelX, jewelY, 0, 2, 0xFFCC44FF, 2));

        RectArea rectArea = new RectArea(jewelX, jewelY, 0, 0);
        rectArea.spirallyTraverse(new ClearDeco(map), 6);

        
        // Hajo: remove items which might overlap imp lairs        
        clearFeatureAreas(map);
        
        player.gameMap.copyFrom(map);
        player.gameMap.darkness = 0xDD000000;
    }

    private void convertMap(TextureCache textureCache,
                            LayeredMap lmap, 
                            Map map, 
                            int wallSet, 
                            double torchChance,
                            int roomFloor,
                            int corridorFloor
                            )
    {
        int width = lmap.getWidth();
        int height = lmap.getHeight();
        
        // Hajo: convert floors and set walls
        for(int j=0; j<height; j++)
        {
            for(int i=0; i<width; i++)
            {
                int n = lmap.get(0, i, j);

                int mapI = i * Map.SUB;
                int mapJ = j * Map.SUB;
                
                char c = (char)(n & 0xFFFF);

                if(c == ':') // Hajo: room floors
                {
                    // map.setFloor(mapI, mapJ, 1 + (int)(Math.random()*3));
                    // map.setFloor(mapI, mapJ, 58 + (int)(Math.random()*3));
                    map.setFloor(mapI, mapJ, roomFloor + (int)(Math.random() * 3));
                
                    // Hajo: room back walls
                    int n1 = map.getFloor(mapI-Map.SUB, mapJ);
                    if(n1 == 0)
                    {
                        setRightWall(map, mapI, mapJ, wallSet + 1, torchChance);
                    }
                
                    int n2 = map.getFloor(mapI, mapJ-Map.SUB);
                    if(n2 == 0)
                    {
                        setLeftWall(map, mapI, mapJ, wallSet, torchChance);
                    }
                }
                else if(c == ';') // Hajo: corridor floors
                {
                    map.setFloor(mapI, mapJ, corridorFloor + (int)(Math.random()*3));
                    // map.setFloor(mapI, mapJ, 1 + (int)(Math.random()*3));

                    // Hajo: corridors, back walls
                    int n2 = map.getFloor(mapI, mapJ-Map.SUB);
                    if(n2 == 0)
                    {
                        map.setLeftWall(mapI, mapJ, wallSet);
                    }
                    
                    int n3 = lmap.get(0, i, j+1) & 0xFFFF;

                    // front wall
                    if(n3 == 46)
                    {
                        map.setLeftWall(mapI, mapJ+Map.SUB, wallSet + 3);
                    }
                    
                    // back wall
                    n2 = map.getFloor(mapI-Map.SUB, mapJ);
                    if(n2 == 0)
                    {
                        map.setRightWall(mapI, mapJ, wallSet + 1);
                        // map.setRightWall(mapI, mapJ, 19);
                    }
                    
                    n3 = lmap.get(0, i+1, j) & 0xFFFF;

                    if(n3 == 46)
                    {
                        map.setRightWall(mapI+Map.SUB, mapJ, wallSet + 2);
                    }
                }
                else if(c == '.') // Hajo: neither room nor corridor
                {
                    // Hajo: "nothing" -> front walls
                    map.setFloor(mapI, mapJ, 0);
                
                    int n1 = map.getFloor(mapI-Map.SUB, mapJ);
                    int n2 = map.getFloor(mapI, mapJ-Map.SUB);
                    
                    // Hajo: room front walls
                    if(n1 >= roomFloor && n1 <= roomFloor + 3)
                    {
                        map.setRightWall(mapI, mapJ, wallSet + 2);
                    }
                
                    if(n2 >= roomFloor && n2 <= roomFloor + 3)
                    {
                        map.setLeftWall(mapI, mapJ, wallSet + 3);
                    }
                    
                    // Hajo: tunnel front walls
                    if(n1 >= corridorFloor && n1 <= corridorFloor + 3)
                    {
                        map.setRightWall(mapI, mapJ, wallSet + 2);
                    }
                
                    if(n2 >= corridorFloor && n2 <= corridorFloor + 3)
                    {
                        map.setLeftWall(mapI, mapJ, wallSet + 3);
                    }
                }
                
                int argb = Colors.randomColor(90, 24, 24, 24);
               
                map.setColor(mapI, mapJ, argb);
                
                // Hajo: map features
                
                n = lmap.get(1, i, j) & 0xFFFF; 
                
                if(n == 'f')
                {
                    // Hajo: found a well
                    map.setItem(mapI, mapJ, Map.F_DECO + Map.FEAT_SMALL_WELL);
                    System.err.print('f');
                }
                else
                {
                    System.err.print(c);
                }
            }
            System.err.println();
        }

        addWallEndPieces(map, wallSet);
        
        map.recalculateBlockedAreas(textureCache.textures);
    }
    
    void makeStartMap2(IsoDisplay display, Player player) 
    {
        SubdivisionGenerator gen = new SubdivisionGenerator();
        LayeredMap map = gen.create(new Properties());
        int w = map.getWidth();
        int h = map.getHeight();
        
        // Hajo: transform map
        
        Map gameMap = player.gameMap;
        gameMap.newMap((w+1) * Map.SUB, (h+1)*Map.SUB);
        
        // Hajo: convert floors
        for(int j=0; j<h; j++)
        {
            for(int i=0; i<w; i++)
            {
                int n = map.get(0, i, j);

                int mapI = i * Map.SUB;
                int mapJ = j * Map.SUB;
                
                char c = (char)(n & 0xFFFF);

                if(c == '#')
                {
                    // Hajo: a room wall
                    int n1 = map.get(0, i-1, j) & 0xFF;
                    if(n1 == '.' || n1 == 0)
                    {
                        gameMap.setRightWall(mapI, mapJ, 8);
                    }
                
                    int n2 = map.get(0, i, j-1) & 0xFF;
                    if(n2 == '.' || n2 == 0)
                    {
                        gameMap.setLeftWall(mapI, mapJ, 7);
                    }
                }
                else if(c == '.')
                {
                    // Hajo: corridors
                    int n1 = map.get(0, i-1, j) & 0xFF;
                    if(n1 == '#' || n1 == 0)
                    {
                        gameMap.setRightWall(mapI, mapJ, 8);
                    }
                
                    int n2 = map.get(0, i, j-1) & 0xFF;
                    if(n2 == '#' || n2 == 0)
                    {
                        gameMap.setLeftWall(mapI, mapJ, 7);
                    }
                }
                
                int brightness = 80;
                int argb = 
                        ((brightness + (int)(Math.random() * 16)) << 16) |
                        ((brightness + (int)(Math.random() * 24)) << 8) |
                        ((brightness + (int)(Math.random() * 32)));
                
                gameMap.setColor(mapI, mapJ, argb);
                
                // Hajo: map features
                
                n = map.get(1, i, j) & 0xFFFF; 
                
                if(n == '<')
                {
                    // Hajo: found stairs up
                    gameMap.setItem(mapI, mapJ, Map.F_DECO + Map.FEAT_STAIRS_UP_NARROW);
                    System.err.print('<');
                }
                else if(n == 'f')
                {
                    // Hajo: found a well
                    gameMap.setItem(mapI, mapJ, Map.F_DECO + Map.FEAT_SMALL_WELL);
                    System.err.print('f');
                }
                else
                {
                    System.err.print(c);
                }
                
                if(j==0) gameMap.setLeftWall(mapI, mapJ, 7);
                if(i==0) gameMap.setRightWall(mapI, mapJ, 8);
                if(j==h-1)
                {
                    gameMap.setLeftWall(mapI, mapJ, 7);
                    gameMap.setRightWall(mapI, mapJ, 0);
                }
                if(i==w-1) 
                {
                    gameMap.setLeftWall(mapI, mapJ, 0);
                    gameMap.setRightWall(mapI, mapJ, 8);
                }

                if(i != w-1 && j != h-1) gameMap.setFloor(mapI, mapJ, 1+(int)(Math.random()*3));

            }
            System.err.println();
        }

        gameMap.recalculateBlockedAreas(display.textureCache.textures);
        
        distributeItems(gameMap, 0.001);
        
        player.location.x = map.getSpawnX() * Map.SUB + 1;
        player.location.y = map.getSpawnY() * Map.SUB + 1;
        
        gameMap.setMob(player.location.x, player.location.y, player.getKey());

        SimpleAi simpleAi = new SimpleAi();
        simpleAi.setHome(player.location.x+3, player.location.y+3);
        
        Player imp = new Player(this, player.location.x+3, player.location.y+3, Species.IMPS_BASE, gameMap, simpleAi, 15, new MovementJumping());
        int impKey = mobs.nextFreeKey();
        imp.setKey(impKey);
        mobs.put(imp.getKey(), imp);
        
        gameMap.setMob(imp.location.x, imp.location.y, imp.getKey());
    }

    void makeWildernessMap(TextureCache textureCache, Player player) 
    {
        Properties props = new Properties();
        
        props.setProperty("map_layers", "" + MAP_LAYERS);
        props.setProperty("grounds", ".:s");
        props.setProperty("decorations.tiles", "w:o, V:l, +:s, ~:s, f:s");
        props.setProperty("decorations.chances", "100, 100, 100, 100, 100");
        props.setProperty("clusters[0].tiles", "&:s, T:s, t:l");
        props.setProperty("clusters[0].chance", "150");
        // props.setProperty("clusters[1].tiles", "+:s, ~:s");
        // props.setProperty("clusters[1].chance", "50");
        props.setProperty("margins", "&:o, &:l, &:g");
        props.setProperty("margin.width", "3");
        props.setProperty("margin.chance", "30");
        props.setProperty("borders", "&:o, &:l, &:g");
        
        WildernessGenerator gen = new WildernessGenerator(props);
        LayeredMap map = gen.generate(50, 50);
        int w = map.getWidth();
        int h = map.getHeight();
        
        // Hajo: transform map
        
        Map gameMap = player.gameMap;
        gameMap.newMap((w+1) * Map.SUB, (h+1)*Map.SUB);
        
        // Hajho: we need a path
        
        int x = map.getSpawnX();
        for(int y=0; y<h; y++)
        {
            map.set(0, x, y, ',');
            map.set(1, x, y, 0);
        }
        
        // Hajo: convert floors
        for(int j=0; j<h; j++)
        {
            for(int i=0; i<w; i++)
            {
                int n = map.get(0, i, j);

                int mapI = i * Map.SUB;
                int mapJ = j * Map.SUB;
                
                char c = (char)(n & 0xFFFF);

                if(i != w-1 && j != h-1) 
                {
                    if(c == ',')
                    {
                        gameMap.setFloor(mapI, mapJ, Map.FLOOR_SAND + (int)(Math.random()*3));
                    }
                    else
                    {
                        gameMap.setFloor(mapI, mapJ, Map.FLOOR_DARK_GRASS + (int)(Math.random()*2));
                    }
                }
                
                int brightness = 112;
                int argb = Colors.randomColor(brightness, 16, 24, 32);
                gameMap.setColor(mapI, mapJ, argb);
                
                // Hajo: map features
                
                n = map.get(1, i, j) & 0xFFFF; 
                
                int xoff = 5 + (int)(Map.SUB * Math.random() * 0.5);
                int yoff = 5 + (int)(Map.SUB * Math.random() * 0.5);
                
                if(n == '&')
                {
                    gameMap.setItem(mapI+xoff, mapJ+yoff, Map.F_DECO + Map.FEAT_BIG_GREEN_TREE);
                }
                else if(n == 't')
                {
                    gameMap.setItem(mapI+xoff, mapJ+yoff, Map.F_DECO + Map.FEAT_SMALL_GREEN_TREE);
                }
                else if(n == 'T')
                {
                    gameMap.setItem(mapI+xoff, mapJ+yoff, Map.F_DECO + Map.FEAT_MEDIUM_GREEN_TREE);
                }
                else if(n == 'V')
                {
                    gameMap.setItem(mapI+xoff, mapJ+yoff, Map.F_DECO + Map.FEAT_ORANGE_TREE);
                }
                else if(n == 'w')
                {
                    gameMap.setItem(mapI+xoff, mapJ+yoff, Map.F_DECO + Map.FEAT_SMALL_WEEDS);
                }
                else if(n == '+')
                {
                    gameMap.setItem(mapI+xoff, mapJ+yoff, Map.F_DECO + Map.FEAT_DECO_GRASS_1);
                }
                else if(n == '~')
                {
                    gameMap.setItem(mapI+xoff, mapJ+yoff, Map.F_DECO + Map.FEAT_DECO_GRASS_2);
                }
                else if(n == 'f')
                {
                    // Hajo: found a well
                    gameMap.setItem(mapI, mapJ, Map.F_DECO + Map.FEAT_SMALL_WELL);
                }
                else
                {
                    System.err.print((char)n);
                }
                
                if(j==0) gameMap.setLeftWall(mapI, mapJ, 7);
                if(i==0) gameMap.setRightWall(mapI, mapJ, 8);
                if(j==h-1)
                {
                    gameMap.setLeftWall(mapI, mapJ, 7);
                    gameMap.setRightWall(mapI, mapJ, 0);
                }
                if(i==w-1) 
                {
                    gameMap.setLeftWall(mapI, mapJ, 0);
                    gameMap.setRightWall(mapI, mapJ, 8);
                }
            }
            System.err.println();
        }

        gameMap.recalculateBlockedAreas(textureCache.textures);
        
        distributeItems(gameMap, 0.00006);
        
        player.location.x = map.getSpawnX() * Map.SUB + 1;
        player.location.y = map.getSpawnY() * Map.SUB + 1;
        

        mobs.clear();
        mobs.put(player.getKey(), player);
        gameMap.setMob(player.location.x, player.location.y, player.getKey());
        
        SimpleAi simpleAi = new SimpleAi();
        simpleAi.setHome(player.location.x+3, player.location.y+3);
        Player imp = new Player(this, player.location.x+3, player.location.y+3, Species.IMPS_BASE, gameMap, simpleAi, 15, new MovementJumping());
        int impKey = mobs.nextFreeKey();
        imp.setKey(impKey);
        mobs.put(imp.getKey(), imp);
        
        gameMap.setMob(imp.location.x, imp.location.y, imp.getKey());
    
        gameMap.setItem(player.location.x+4, 8, Map.F_DECO + Map.FEAT_ARROW_WEST);
        gameMap.darkness = 0;
    }

    public void checkMapChange(TextureCache textureCache, Player player, int x, int y)
    {
        MapTransitions.MapId oldMapId = mapTransitions.getCurrentMap();
        MapTransitions.MapId newMapId = mapTransitions.transition(player, 0, x, y);
    
        if(newMapId != oldMapId)
        {
            changeMap(textureCache, player, oldMapId, newMapId, 0);
        }
    }
    
    public void changeMapOnFeature(TextureCache textureCache, Player player, int feature)
    {
        logger.log(Level.INFO, "Player uses feature #{0}", feature);

        MapTransitions.MapId oldMapId = mapTransitions.getCurrentMap();
        MapTransitions.MapId newMapId = mapTransitions.transition(player, feature, -1, -1);
    
        changeMap(textureCache, player, oldMapId, newMapId, feature);
    }        
        
    private void changeMap(TextureCache textureCache,
                           Player player, 
                           MapTransitions.MapId oldMapId, 
                           MapTransitions.MapId newMapId, 
                           int feature)
    {
        try
        {
            if(newMapId == MapTransitions.MapId.VILLAGE_1)
            {
                logger.log(Level.INFO, "Player changes to village map 1.");
                clearMapItemsFromRegistry(player.gameMap);
                makeVillageMap(player, oldMapId, feature);
            } 
            else if(newMapId == MapTransitions.MapId.TEMPLE)
            {
                logger.log(Level.INFO, "Player changes to temple map.");
                clearMapItemsFromRegistry(player.gameMap);
                InputStream in  = Class.class.getResourceAsStream("/jewelhunt/resources/temple.map");
                player.gameMap.load(in);
                player.gameMap.darkness = 0;
                
                if(feature == Map.FEAT_ARROW_WEST)
                {
                    player.location.x = 78;
                    player.location.y = 232;
                }
                
                player.gameMap.setMob(player.location.x, player.location.y, player.getKey());
                
                mobs.clear();
                mobs.put(player.getKey(), player);
                
            } 
            else if(newMapId == MapTransitions.MapId.PYRAMID)
            {
                logger.log(Level.INFO, "Player changes to pyramid map.");
                clearMapItemsFromRegistry(player.gameMap);
                InputStream in  = Class.class.getResourceAsStream("/jewelhunt/resources/uncolored_pyramid.map");
                player.gameMap.load(in);
                player.gameMap.darkness = 0;
                
                if(oldMapId == MapTransitions.MapId.VILLAGE_1)
                {
                    player.location.x = 129;
                    player.location.y = 16;
                }
                else if(oldMapId == MapTransitions.MapId.PYRAMID_DUNGEON_1)
                {
                    player.location.x = 127;
                    player.location.y = 140;
                }
                
                player.gameMap.setMob(player.location.x, player.location.y, player.getKey());
                
                mobs.clear();
                mobs.put(player.getKey(), player);
            } 
            else if(newMapId == MapTransitions.MapId.PYRAMID_DUNGEON_1)
            {
                logger.log(Level.INFO, "Player changes to pyramid dungeon map 1.");
                clearMapItemsFromRegistry(player.gameMap);
                makePyramidDungeon(textureCache, player);
            }
            else if(newMapId == MapTransitions.MapId.DUNGEON_1)
            {
                logger.log(Level.INFO, "Player changes to dungeon map 1.");
                clearMapItemsFromRegistry(player.gameMap);
                makeImpDungeon(textureCache, player);
            }
            else if(newMapId == MapTransitions.MapId.WILDERNESS_1)
            {
                logger.log(Level.INFO, "Player changes to wilderness map 1.");
                clearMapItemsFromRegistry(player.gameMap);
                makeWildernessMap(textureCache, player);
            }
        }
        catch (IOException ex)
        {
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    
    private Dungeon makeDungeonMap(Player player)
    {
        Properties props = new Properties();
        props.setProperty("map_layers", "" + MAP_LAYERS);

        props.setProperty("room_width_min", "3");
        props.setProperty("room_height_min", "3");
        props.setProperty("room_width_max", "4");
        props.setProperty("room_height_max", "4");
        
        props.setProperty("roomGapHoriz", "5");
        props.setProperty("roomGapVert", "5");
        // props.setProperty("door", "+:u");
        props.setProperty("walls", "#:s");
        props.setProperty("hidden_wall", "#:s");
        props.setProperty("floors", ".:s");
        props.setProperty("room_floors", "::s");
        props.setProperty("corridor_floors", ";:s");
        props.setProperty("room_features", "f:s");
        
        // We don't want the default stairs
        props.setProperty("stairs_up", "<:s");
        props.setProperty("damaged_floor_chance", "0");

        Dungeon dungeon = new Dungeon("", props);
        return dungeon;
    }

    private LayeredMap makePyramidMap(ArrayList <Rectangle> rooms, int width, int height)
    {
        LayeredMap lmap = new LayeredMap(MAP_LAYERS, width, height);
    
        MapUtils mapUtils = new MapUtils();
        mapUtils.fillArea(lmap, 0, 0, 0, width, height, new int [] {'.'});
        
        int roomX = width - 5;
        int roomY = height / 2;
        
        Rectangle startRoom = new Rectangle(roomX-2, roomY-2, 3, 3);
        mapUtils.fillArea(lmap, 0, startRoom.x, startRoom.y, startRoom.width, startRoom.height, new int [] {':'});
        
        makeRoomLine(lmap, rooms, -1, 0, startRoom.x, startRoom.y+1, 3, 0, 5);
        
        ArrayList <Rectangle> roomLine = new ArrayList<Rectangle>(rooms);
        for(Rectangle room : roomLine)
        {
            makeRoomLine(lmap, rooms, 0, -1, room.x + room.width/2, room.y-1, 1, 3, 3);
            makeRoomLine(lmap, rooms, 0, 1, room.x + room.width/2, room.y+room.height, 1, 3, 3);
        }
        
        rooms.add(startRoom);
        lmap.setSpawnX(startRoom.x + startRoom.width/2);
        lmap.setSpawnY(startRoom.y + startRoom.height/2);
        
        return lmap;
    }

    private void makeRoomLine(LayeredMap lmap, ArrayList <Rectangle> rooms, int dx, int dy, int x, int y, int minLength, int lengthRand, int count)
    {
        MapUtils mapUtils = new MapUtils();
        
        for(int i=0; i < count; i++)
        {
            // Hajo: First, a corridor
            int length = minLength + (int)(Math.random() * lengthRand);

            fillArea(mapUtils, lmap, x, y, x + length*dx, y + length*dy, new int [] {';'});
            
            x += length*dx;
            y += length*dy;
            
            // Hajo: then, a room

            int size = 1 + (int)(Math.random() * 2);
            
            int centerX = x + dx * size;
            int centerY = y + dy * size;
            int x1 = centerX - (dx + dy) * size;
            int x2 = centerX + (dx + dy) * size;
            
            int y1 = centerY - (dx + dy) * size;
            int y2 = centerY + (dx + dy) * size;

            if(x1 > x2)
            {
                int t = x2;
                x2 = x1;
                x1 = t;
            }
            
            if(y1 > y2)
            {
                int t = y2;
                y2 = y1;
                y1 = t;
            }
        
            Rectangle room = new Rectangle(x1, y1, x2-x1+1, y2-y1+1);
            mapUtils.fillArea(lmap, 0, room.x, room.y, room.width, room.height, new int [] {':'});
            rooms.add(room);
        
            x += dx * (2*size+1);
            y += dy * (2*size+1);
        }
    }
    
    private void fillArea(MapUtils mapUtils, LayeredMap lmap, int x1, int y1, int x2, int y2, int [] values)
    {
        if(x1 > x2)
        {
            int t = x2;
            x2 = x1;
            x1 = t;
        }

        if(y1 > y2)
        {
            int t = y2;
            y2 = y1;
            y1 = t;
        }
        
        mapUtils.fillArea(lmap, 0, x1, y1, x2-x1+1, y2-y1+1, values);
    }
    
    private void distributeItems(Map map, double chance)
    {
        int width = map.getWidth();
        int height = map.getHeight();
        
        for(int j=0; j<width; j+=Map.SUB)
        {
            for(int i=0; i<height; i+=Map.SUB)
            {
                if(map.getFloor(i, j) > 0)
                {
                    for(int jj=0; jj<Map.SUB; jj++)
                    {
                        for(int ii=0; ii<Map.SUB; ii++)
                        {
                            int mapI = i + ii;
                            int mapJ = j + jj;

                            if(!map.isMovementBlocked(mapI, mapJ) && 
                               !map.isPlacementBlocked(mapI, mapJ) &&
                               Math.random() < chance)
                            {
                                itemFactory.dropItem(items, 
                                                     "tier_1", null, null, 1.0, 
                                                     map, mapI, mapJ);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void distributeMonsters(Map map, Player player)
    {
        int count = 50;
        
        logger.log(Level.INFO, "Scanning area.");
        
        Area area = new Area();
        area.findArea(new DefaultPathSource(map), player.location.x+1, player.location.y+1);
        
        ArrayList <Point> areaPoints = area.getArea();
        
        logger.log(Level.INFO, "Found {0} locations in area.", areaPoints.size());
        
        if(areaPoints.size() > 2 * count)
        {
            for(int i=0; i<count; i++)
            {
                int n = (int)(areaPoints.size() * Math.random());
                Point point = areaPoints.get(n);

                // Hajo: place no monsters direcly besides the player
                int xdiff = point.x - player.location.x;
                int ydiff = point.y - player.location.y;
                int dist2 = xdiff * xdiff + ydiff * ydiff;
                int rad2 = 12*12; 
                if(dist2 > rad2)
                {
                    int radius = 3; // Hajo: lair size -> table ?
                    
                    if(!map.isPlacementBlockedRadius(point.x, point.y, radius + 1)) // lair size + space
                    {
                        // SimpleAi ai = new SimpleAi();
                        EnemyAi ai = new EnemyAi(this);
                        ai.setHome(point.x, point.y);
                        Player imp = new Player(this, point.x, point.y, Species.IMPS_BASE, player.gameMap, ai, 15, new MovementJumping());
                        int impKey = mobs.nextFreeKey();
                        imp.setKey(impKey);
                        mobs.put(imp.getKey(), imp);

                        imp.visuals.color = Colors.randomColor(255-32, 32, 32, 32);

                        // Todo: species life should be part of some table
                        imp.stats.setCurrent(Player.I_VIT, 3 + (int)(Math.random() * 3));

                        map.setMob(imp.location.x, imp.location.y, imp.getKey());

                        map.setItem(imp.location.x, imp.location.y, Map.F_DECO + Map.FEAT_IMP_LAIR);
                        map.setPlacementBlockedRadius(point.x, point.y, radius, true);
                        areaPoints.remove(n);
                    }
                }
            }
        }
    }
    
    private void distributeMushrooms(Map map, double chance)
    {
        int width = map.getWidth();
        int height = map.getHeight();
        
        for(int j=0; j<width; j+=Map.SUB)
        {
            for(int i=0; i<height; i+=Map.SUB)
            {
                if(map.getFloor(i, j) > 0)
                {
                    for(int jj=0; jj<Map.SUB; jj++)
                    {
                        for(int ii=0; ii<Map.SUB; ii++)
                        {
                            int mapI = i + ii;
                            int mapJ = j + jj;

                            if(!map.isMovementBlocked(mapI, mapJ))
                            {    
                                if(Math.random() < 0.001)
                                {
                                    map.setItem(mapI, mapJ, Map.DECO_MOSSY_PATCH);
                                }
                                if(Math.random() < 0.001)
                                {
                                    map.setItem(mapI, mapJ, Map.DECO_DARK_SPOTS);
                                }
                                if(Math.random() < 0.001)
                                {
                                    map.setItem(mapI, mapJ, Map.DECO_VERY_DARK_SPOTS);
                                }
                                if(Math.random() < 0.001)
                                {
                                    map.setItem(mapI, mapJ, Map.DECO_CRACK_VERY_DARK);
                                }
                                
                                if(Math.random() < chance)
                                {
                                    map.setItem(mapI, mapJ, Map.DECO_IMP_MUSHROOM);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void step()
    {
        
    }

    public MapTransitions.MapId getCurrentMap()
    {
        return mapTransitions.getCurrentMap();
    }


    private void clearFeatureAreas(Map map)
    {
        RectArea rectArea = new RectArea(0, 0, 1, 1);
        
        int w = map.getWidth();
        int h = map.getHeight();
        
        for(int j=0; j<h; j++)
        {
            for(int i=0; i<w; i++)
            {
                int n = map.getItem(i, j);
                
                if((n & Map.F_DECO) != 0)
                {
                    rectArea.area.x = i;
                    rectArea.area.y = j;
                    rectArea.spirallyTraverse(new ClearItems(map), 6);
                    
                    map.setItem(i, j, n);
                }
            }
        }
    }

    public void makeVillageMap(Player player, MapTransitions.MapId oldMapId, int feature) 
    {
        InputStream in  = Class.class.getResourceAsStream("/jewelhunt/resources/village_12.map");
        try 
        {
            player.gameMap.load(in);
        }
        catch (IOException ex) 
        {
            Logger.getLogger(World.class.getName()).log(Level.SEVERE, null, ex);
        }

        if(feature == Map.FEAT_ARROW_WEST)
        {
            player.location.x = 126;
            player.location.y = 225;
        }
        else if(feature == Map.FEAT_ARROW_EAST)
        {
            player.location.x = 66;
            player.location.y = 7;
        }
        else if(feature == Map.FEAT_STAIRS_UP_NARROW)
        {
            player.location.x = 129;
            player.location.y = 75;
        }
        else if(oldMapId == MapTransitions.MapId.PYRAMID)
        {
            player.location.x = 126;
            player.location.y = 225;
        }
        else
        {
            player.location.x = 110;
            player.location.y = 130;
        }                

        player.gameMap.setMob(player.location.x, player.location.y, player.getKey());

        mobs.clear();
        mobs.put(player.getKey(), player);
        player.gameMap.darkness = 0;
    }

    private void setRightWall(Map map, int i, int j, int wallType, double lightChance)
    {
        map.setRightWall(i, j, wallType);
        if(Math.random() < lightChance)
        {
            Light light = new Light(i, j + Map.SUB/2 - 1, 60, 3, 0xDDFFCC99, 0.7);
            map.lights.add(light);
            
            map.setItem(i, j + Map.SUB/2, Map.F_DECO + 5);
        }
    }

    private void setLeftWall(Map map, int i, int j, int wallType, double lightChance)
    {
        map.setLeftWall(i, j, wallType);
        if(Math.random() < lightChance)
        {
            Light light = new Light(i + Map.SUB/2 - 1, j, 60, 3, 0xDDFFCC99, 0.7);
            map.lights.add(light);

            map.setItem(i + Map.SUB/2, j, Map.F_DECO + 4);
        }
    }
    
    private void addWallEndPieces(Map map, int wallSet) 
    {
        int h = map.getHeight();
        int w = map.getWidth();

        // left back, right back, left front,right front,
        // left half pillar, left full pillar, right full pillar, right half pillar
        
        for(int j=0; j<h; j+=Map.SUB)
        {
            for(int i=0; i<w; i+=Map.SUB)
            {
                int n = map.getLeftWall(i, j);
                
                if(n == wallSet)
                {
                    // Hajo: There is a wall here, is it an wall end?
                    int n1 = map.getLeftWall(i+Map.SUB, j);
                    if(n1 != n)
                    {
                        // down end, check for incoming wall
                        n1 = map.getLeftWall(i + Map.SUB, j - Map.SUB);
                        if(n1 == 0)
                        {
                            map.setItem(i + Map.SUB-1, j, Map.F_DECO + wallSet + 5);
                        }
                    }
                    
                    n1 = map.getLeftWall(i-Map.SUB, j);
                    if(n1 != n)
                    {
                        // up end - check for right wall incoming
                        n1 = map.getRightWall(i, j);
                        if(n1 == 0)
                        {
                            // no wall, must be end
                            map.setItem(i, j, Map.F_DECO + wallSet + 7);
                        }
                    }
                }

                n = map.getRightWall(i, j);
                if(n == wallSet + 1)
                {
                    // Hajo: There is a wall here, is it an wall end?
                    int n1 = map.getRightWall(i, j+Map.SUB);
                    if(n1 != n)
                    {
                        // down end, check for incoming wall
                        n1 = map.getLeftWall(i - Map.SUB, j + Map.SUB);
                        if(n1 == 0)
                        {
                            map.setItem(i, j + Map.SUB-1, Map.F_DECO + wallSet + 6);
                        }
                    }
                    n1 = map.getRightWall(i, j-Map.SUB);
                    if(n1 != n)
                    {
                        // up end - check for left wall incoming
                        n1 = map.getLeftWall(i, j);
                        if(n1 == 0)
                        {
                            // no wall, must be end
                            map.setItem(i, j, Map.F_DECO + wallSet + 4);
                        }
                    }
                }
            }
        }
    }

    private void clearMapItemsFromRegistry(Map map) 
    {
        for(int j=0; j<map.getHeight(); j++)
        {
            for(int i=0; i<map.getWidth(); i++)
            {
                int key = map.getItem(i, j);
                
                if(key > 0 && key < Map.F_DECO)
                {
                    items.remove(key);
                    // Hajo: just to be sure no one uses the key
                    // accidentally anymore ...
                    map.setItem(i, j, 0);
                }
            }
        }
    }
}
