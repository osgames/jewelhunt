//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.item;

import java.io.BufferedReader;
import java.io.IOException;
import rlgamekit.item.IndividualItem;
import rlgamekit.item.ItemCatalog;
import rlgamekit.item.Triplet;

/**
 * Extends IndividualItem to implement jewelhunt specific item properties.
 * 
 * @author Hj. Malthaner
 */
public class Item extends IndividualItem
{
    private final static int S_NAME = 0;
    private final static int S_DESC = 1;
    private final static int S_CLASS = 2;
    private final static int S_TAGS = 3;

    private final static int I_CURR_DURABILITY = 100;
    private final static int I_REG_KEY = 101;
    private final static int I_DAMAGE_PERCENT = 102;
    private final static int MAGIC_START = I_DAMAGE_PERCENT;
    private final static int I_DEFENCE_PERCENT = 103;
    private final static int I_RES_PHYSICAL_PERCENT = 104;
    private final static int I_RES_FIRE_PERCENT = 105;
    private final static int I_RES_ICE_PERCENT = 106;
    private final static int I_RES_LIGHT_PERCENT = 107;
    private final static int I_RES_POISON_PERCENT = 108;
    private final static int I_RES_FEAR_PERCENT = 109;
    private final static int I_RES_CHAOS_PERCENT = 110;
    private final static int I_ADD_LIFE = 111;
    private final static int I_ADD_MANA = 112;
    private final static int I_ADD_STEALTH = 113;
    private final static int I_ADD_STRENGTH = 114;
    private final static int I_ADD_DEX = 115;
    private final static int I_ADD_WISDOM = 116;
    private final static int I_ADD_INT = 117;
    private final static int I_ADD_DODGE = 118;
    private final static int MAGIC_END = I_ADD_DODGE;

    private final static int I_IDENTFIED = 150;
    private final static int S_IDENTFIED_NAME = 151;
    private final static int I_CURR_STACKSIZE = 152;
    private final static int I_HUE = 153;

    private final static int I_SOCKETS = 200;       // counter  
    private final static int I_SOCKET_LINKS = 201;  // bitfield!
    
    private final static int I_INVENTORY_IMAGE = 0;
    private final static int I_MAP_IMAGE = 1;
    private final static int I_PRICE = 2;
    private final static int I_BASE_DURABILITY = 3;
    private final static int I_DEFENSE = 4;
    private final static int I_WEIGHT = 5;
    private final static int I_MAX_STACKSIZE = 6;
    public final static int I_RARITY = 7;
    public final static int I_BLOCK = 8;

    
    private final static int T_INVENTORY_SIZE = 0;
    private final static int T_DAMAGE = 1;
    private final static int T_UID = 2;
    
    
    private final Item [] socketedItems = new Item [8];
    
    public int countMods()
    {
        int count = 0;
        
        for(int i=MAGIC_START; i<=MAGIC_END; i++)
        {
            if(getInt(i) > 0) count ++;
        }
 
        return count;
    }
    
    /**
     * Registry key is only valid within one session
     * @return the key
     */
    public int getRegistryKey()
    {
        return getInt(I_REG_KEY);
    }
    
    /**
     * Registry key is only valid within one session
     * @param regKey The item's key
     */
    public void setRegistryKey(int regKey)
    {
        setInt(I_REG_KEY, regKey);
    }
    
    /**
     * Each item should have a unique ID ... if one bothered to set it.
     * @return The id
     */
    public long getID()
    {
        Triplet t = getTriplet(T_UID);
        
        return ((long)t.get(0)) << 32 | ((long)t.get(1));
    }
    
    /**
     * Set the id for this item. The id should be unique since
     * parts of the code use it to identify items in collections.
     * @param id Unique id for this item
     */
    public void setID(long id)
    {
        Triplet t = new Triplet((int)(id >>> 32), (int)(id & 0xFFFFFFFF), 0);
        setTriplet(T_UID, t);
    }
    
    public String getName()
    {
        return getString(S_NAME);
    }

    public void setName(String name)
    {
        setString(S_NAME, name);
    }

    public String getTags()
    {
        return getString(S_TAGS);
    }
    
    public String getIdentifiedName()
    {
        return getString(S_IDENTFIED_NAME);
    }

    public void setIdentifiedName(String name)
    {
        setString(S_IDENTFIED_NAME, name);
    }

    public int getIdentified()
    {
        return getInt(I_IDENTFIED);
    }
    
    public void setIdentified(int level)
    {
        setInt(I_IDENTFIED, level);
    }
    
    public int getSocketCount()
    {
        return getInt(I_SOCKETS);
    }
    
    public void setSocketCount(int count)
    {
        setInt(I_SOCKETS, count);
    }
    
    public Item getSocketItem(int socket)
    {
        // Hajo: do some bounds checking
        if(socket >= 0 && socket <= getSocketCount())
        {
            return socketedItems[socket];
        }
        else
        {
            return null;
        }
    }
    
    public void setSocketItem(int socket, Item item)
    {
        // Hajo: do some bounds checking
        if(socket >= 0 && socket <= getSocketCount())
        {
            socketedItems[socket] = item;
        }
    }
    
    
    public int getDefense()
    {
        return getInt(I_DEFENSE);
    }

    public int getWeight()
    {
        return getInt(I_WEIGHT);
    }

    public int getStackWeight()
    {
        return getWeight() * getCurrentStackSize();
    }

    public int getBaseDurability()
    {
        return getInt(I_BASE_DURABILITY);
    }
    
    public int getCurrentDurability()
    {
        return getInt(I_CURR_DURABILITY);
    }
    
    public void setCurrentDurability(int durability)
    {
        setInt(I_CURR_DURABILITY, durability);
    }
    
    public int getCurrentStackSize()
    {
        return getInt(I_CURR_STACKSIZE);
    }
    
    public void setCurrentStackSize(int count)
    {
        setInt(I_CURR_STACKSIZE, count);
    }
    
    public int getMaxStackSize()
    {
        return getInt(I_MAX_STACKSIZE);
    }
    
    public int getAddStr()
    {
        return getInt(I_ADD_STRENGTH);
    }
    
    public void setAddStr(int howmuch)
    {
        setInt(I_ADD_STRENGTH, howmuch);
    }

    public int getAddDex()
    {
        return getInt(I_ADD_DEX);
    }
    
    public void setAddDex(int howmuch)
    {
        setInt(I_ADD_DEX, howmuch);
    }

    public int getAddWis()
    {
        return getInt(I_ADD_WISDOM);
    }
    
    public void setAddWis(int howmuch)
    {
        setInt(I_ADD_WISDOM, howmuch);
    }

    public int getAddInt()
    {
        return getInt(I_ADD_INT);
    }
    
    public void setAddInt(int howmuch)
    {
        setInt(I_ADD_INT, howmuch);
    }
    
    public void setAddLife(int amount)
    {
        setInt(I_ADD_LIFE, amount);
    }

    public int getAddLife()
    {
        return getInt(I_ADD_LIFE);
    }
    
    public void setAddMana(int amount)
    {
        setInt(I_ADD_MANA, amount);
    }

    public int getAddMana()
    {
        return getInt(I_ADD_MANA);
    }
    
    public int getAddStealth()
    {
        return getInt(I_ADD_STEALTH);
    }

    public int getAddDodge()
    {
        return getInt(I_ADD_DODGE);
    }

    public void setAddDamagePercent(int percent)
    {
        setInt(I_DAMAGE_PERCENT, percent);
    }

    public int getAddDamagePercent()
    {
        return getInt(I_DAMAGE_PERCENT);
    }
    
    public void setAddDefencePercent(int percent)
    {
        setInt(I_DEFENCE_PERCENT, percent);
    }

    public int getAddDefencePercent()
    {
        return getInt(I_DEFENCE_PERCENT);
    }

    public int getResPhysicalPercent()
    {
        return getInt(I_RES_PHYSICAL_PERCENT);
    }
    
    public void setResFirePercent(int percent)
    {
        setInt(I_RES_FIRE_PERCENT, percent);
    }
    
    public int getResFirePercent()
    {
        return getInt(I_RES_FIRE_PERCENT);
    }
    
    public void setResIcePercent(int percent)
    {
        setInt(I_RES_ICE_PERCENT, percent);
    }
    
    public int getResIcePercent()
    {
        return getInt(I_RES_ICE_PERCENT);
    }

    public void setResLightPercent(int percent)
    {
        setInt(I_RES_LIGHT_PERCENT, percent);
    }
    
    public int getResLightPercent()
    {
        return getInt(I_RES_LIGHT_PERCENT);
    }

    public void setResPoisonPercent(int percent)
    {
        setInt(I_RES_POISON_PERCENT, percent);
    }
    
    public int getResUndefPercent()
    {
        return getInt(I_RES_POISON_PERCENT);
    }

    public void setResFearPercent(int percent)
    {
        setInt(I_RES_FEAR_PERCENT, percent);
    }
    
    public int getResFearPercent()
    {
        return getInt(I_RES_FEAR_PERCENT);
    }

    public void setResChaosPercent(int percent)
    {
        setInt(I_RES_CHAOS_PERCENT, percent);
    }
    
    public int getResChaosPercent()
    {
        return getInt(I_RES_CHAOS_PERCENT);
    }

    public void setHue(int hue)
    {
        setInt(I_HUE, hue);
    }
    
    public int getHue()
    {
        return getInt(I_HUE);
    }

    public int getInventoryImageIndex()
    {
        return getInt(I_INVENTORY_IMAGE);
    }

    public int getInventoryWidth()
    {
        return getTriplet(T_INVENTORY_SIZE).get(0);
    }

    public int getInventoryHeight()
    {
        return getTriplet(T_INVENTORY_SIZE).get(1);
    }
    
    public int getMapImageIndex()
    {
        return getInt(I_MAP_IMAGE);
    }

    public String countAndName() 
    {
        if(getCurrentStackSize() > 1)
        {
            return "" + getCurrentStackSize() + " " + pluralName();
        }
        else
        {
            return calcDisplayName();
        }
    }
    
    public String aCountAndName() 
    {
        if(getCurrentStackSize() > 1)
        {
            return "" + getCurrentStackSize() + " " + pluralName();
        }
        else
        {
            return aName();
        }
    }

    public String pluralName()
    {
        String name = calcDisplayName();
        if(!name.endsWith("s"))
        {
            name = name + "s";
        }
        
        return name;
    }

    public String aName()
    {
        String s = calcDisplayName();
        String a = "aeiou".indexOf(s.charAt(0)) < 0 ? "a " : "an ";
        s = a + s.replace("%", "").toLowerCase();
        return s;
    }

    public String theName()
    {
        String s = calcDisplayName();
        s = "the " + s.replace("%", "").toLowerCase();
        return s;
    }

    public String plural()
    {
        String s = calcDisplayName();
        s = s.replace("%", "s").toLowerCase();
        return s;
    }

    public Item(String key, ItemCatalog itemCatalog)
    {
        super(key, itemCatalog);
    }

    public Item(BufferedReader reader, ItemCatalog itemCatalog)
            throws IOException
    {
        super(reader, itemCatalog);
    }

    public Triplet getDamage()
    {
        return getTriplet(T_DAMAGE);
    }

    public String getItemClass()
    {
        return getString(S_CLASS);
    }

    public int getPrice()
    {
        return getInt(I_PRICE);
    }
    
    public String calcDisplayName()
    {
        if(getIdentified() > 0)
        {
            return getIdentifiedName();
        }
        else
        {
            return getName();
        }
    }
    
    public int calcTotalDamageLow()
    {
        double state = calcItemState();
        double dam = getDamage().get(0);
        double addPercent = 100 + getAddDamagePercent();
        
        return (int)(0.5 + state * dam * addPercent / 100.0);
    }

    public int calcTotalDamageHigh()
    {
        double state = calcItemState();
        double dam = getDamage().get(1);
        double addPercent = 100 + getAddDamagePercent();
        
        return (int)(0.5 + state * dam * addPercent / 100.0);
    }

    public int calcTotalDefense()
    {
        double state = calcItemState();
        
        double def = getDefense();
        double addPercent = 100 + getAddDefencePercent();
        
        return (int)(0.5 + state * def * addPercent / 100.0);
    }

    /**
     * If durability drops below 50%, efficiency will be lowered
     * @return The current item state
     */
    public double calcItemState()
    {
        return Math.min(1.0, getCurrentDurability() / (getBaseDurability() * 0.5));
    }

    public int calcSalesPrice(double merchantFactor)
    {
        int basePrice = getPrice();

        int result = basePrice * getCurrentStackSize();
    
        // Hajo: take magic mods into account
        int n;
        
        // Hajo: mods raise the price two ways:
        // - a contant amount per mod level
        // - a percent amount of the base price per mod level
        
        n = getAddDamagePercent();
        if(n > 0)
        {
            result += 1 * n;
            result += basePrice * 10 * n / 100; 
        }
        
        n = getAddDefencePercent();
        if(n > 0)
        {
            result += 1 * n;
            result += basePrice * 10 * n / 100; 
        }

        n = getAddLife();
        if(n > 0)
        {
            result += 5 * n;
            result += basePrice * 5 * n / 100; 
        }

        n = getAddMana();
        if(n > 0)
        {
            result += 5 * n;
            result += basePrice * 5 * n / 100; 
        }

        n = getResFirePercent();
        if(n > 0)
        {
            result += 5*n*n;
            result += basePrice * 2 * n * n / 100; 
        }
        
        n = getResIcePercent();
        if(n > 0)
        {
            result += 5*n*n;
            result += basePrice * 2 * n * n / 100; 
        }

        n = getResLightPercent();
        if(n > 0)
        {
            result += 5*n*n;
            result += basePrice * 2 * n * n / 100; 
        }

        n = getResUndefPercent();
        if(n > 0)
        {
            result += 5*n*n;
            result += basePrice * 2 * n * n / 100; 
        }

        n = getResFearPercent();
        if(n > 0)
        {
            result += 5*n*n;
            result += basePrice * 2 * n * n / 100; 
        }

        n = getResChaosPercent();
        if(n > 0)
        {
            result += 5*n*n;
            result += basePrice * 2 * n * n / 100; 
        }
        
        // Hajo: traders will round the price ...
        result = (int)(result * merchantFactor);
        
        if(result > 1000)
        {
            // round to 50's
            result = (result + 25) / 50 * 50;
        }
        else if(result > 100)
        {
            // round to 5's
            result = (result + 2) / 5 * 5;
        }
            
        
        return result;
    }
    
    
    @Override
    public String toString()
    {
        return super.toString() + " - " + getIdentifiedName();
    }

    public double calcSellUsedFactor()
    {
        double factor = 1.0;

        // selling used items
        if(getBaseDurability() > 0)
        {
            factor = 0.75 * getCurrentDurability() / getBaseDurability();
        }
        
        return factor;
    }

    public int getBurden() 
    {
        int weight = getStackWeight();
        int size = getInventoryHeight() * getInventoryWidth();
        
        return weight * size + 129 >> 7;
    }
}
