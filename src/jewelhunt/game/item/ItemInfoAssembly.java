//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.item;

import jewelhunt.uikit.StringUtils;

/**
 *
 * @author Hj. Malthaner
 */
public class ItemInfoAssembly 
{
    private static final int ADD_STR = 1;
    private static final int ADD_DEX = 2;
    private static final int ADD_INT = 3;
    private static final int ADD_WIS = 4;
    
    private static final int ADD_FIRE = 5;
    private static final int ADD_COLD = 6;
    private static final int ADD_ELEC = 7;
    
    private static final int ADD_DAMAGE = 8;
    private static final int ADD_DEFENSE = 9;
    
    private static final int RES_FIRE = 10;
    private static final int RES_COLD = 11;
    private static final int RES_ELEC = 12;

    private static String [] modNames = new String []
    {
        " Error", 
    
        " to Strength", 
        " to Dexterity",
        " to Intelligence",
        " to Wisdom",
    
        " Fire Damage",
        " Cold Damage",
        " Electricity Damage",
        
        " Damage", 
        " Defense", 
    
        " to Fire Resistence", 
        " to Cold Resistence", 
        " to Elec. Resistence", 
    
    };
    
    private static int [] modBases = new int []
    {
        0, // " Error", 
    
        1, // " to Strength", 
        1, // " to Dexterity",
        1, // " to Intelligence",
        1, // " to Wisdom",
    
        1, // " Fire Damage",
        1, // " Cold Damage",
        1, // " Electricity Damage",
        
        1, // " Damage", 
        2, // " Defense", 
    
        5, // " to Fire Resistence", 
        5, // " to Cold Resistence", 
        5, // " to Elec. Resistence", 
    
    };
    
    private static final int GEM_RED        = 0;
    private static final int GEM_VIOLET     = 1;
    private static final int GEM_PURPLE     = 2;
    private static final int GEM_CYAN       = 3;
    private static final int GEM_GREEN      = 4;
    private static final int GEM_CHARTREUSE = 5;
    private static final int GEM_ORANGE     = 6;
    private static final int GEM_BRIGHT      = 7;
    private static final int GEM_DARK       = 8;
    
    private static final int SLOT_WEAPON     = 0;
    private static final int SLOT_ARMOR      = 1;
    private static final int SLOT_HELMSHIELD = 2;
    private static final int SLOT_RINGAMULET = 3;
    private static final int SLOT_BELT       = 4;
    
    private static String [] slotNames = new String []
    {
        "Weapon: Adds ", "Armor: Adds ", "Helm: Adds ", "Shield: Adds ", "Ring/Amulet: Adds ", "Belt: Adds "
    };
    
    
    private static final int [] GEM_MOD_TABLE [] = new int[][]
    {
        //         weapon       armor       helm        shield      ring/amulet belt
        new int[] {ADD_FIRE,    ADD_STR,    RES_FIRE,   RES_FIRE,   ADD_STR,    ADD_STR},   // red
        new int[] {ADD_FIRE,    ADD_STR,    RES_FIRE,   ADD_STR,    ADD_STR,    ADD_STR},   // violet
        new int[] {ADD_COLD,    RES_COLD,   ADD_WIS,    RES_COLD,   ADD_WIS,    RES_COLD},  // purple
        new int[] {ADD_ELEC,    RES_ELEC,   ADD_INT,    RES_ELEC,   ADD_INT,   RES_ELEC},  // cyan
        new int[] {ADD_FIRE,    ADD_STR,    RES_FIRE,   ADD_STR,    ADD_STR,    ADD_STR},  // green
        new int[] {ADD_ELEC,    RES_ELEC,   ADD_WIS,    RES_ELEC,   ADD_WIS,    RES_ELEC},  // chartreuse
        new int[] {ADD_DAMAGE,  ADD_DEFENSE, ADD_DEFENSE, ADD_DEFENSE, ADD_DAMAGE, ADD_DAMAGE},  // orange
        new int[] {ADD_FIRE,    ADD_STR,    RES_FIRE,   ADD_STR,    ADD_STR,    ADD_STR},  // bright
        new int[] {ADD_FIRE,    ADD_STR,    RES_FIRE,   ADD_STR,    ADD_STR,    ADD_STR},  // dark
    };
    
    public static void fillDefaultItemInfo(Item item, String [] lines, int [] colors, int [] lineRef,
                                    int defaultColor, int stateColor)
    {
        int n = item.getBurden();
        colors[lineRef[0]] = defaultColor;
        lines[lineRef[0]++] = "Burden: " + StringUtils.calcBurdenString(n);
        
        n = item.getBaseDurability();
        if(n > 0)
        {
            colors[lineRef[0]] = stateColor;
            lines[lineRef[0]++] = "Condition: " + item.getCurrentDurability() + " of " + n;
        }

        
        n = item.getMaxStackSize();
        if(n > 1)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Max. stack size: " + n;
        }
        
        
        if((n = item.calcTotalDefense()) > 0)
        {
            colors[lineRef[0]] = stateColor;
            lines[lineRef[0]++] = "Defense: " + n;
        }
        
        n = item.calcTotalDamageHigh();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Damage: " + item.calcTotalDamageLow() + "-" + n;
        }
        

        n = item.getAddDamagePercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Damage +" + n + "%";
            lineRef[1] ++;
        }
        if(n < 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Damage reduced by " + (-n) + "%";
            lineRef[1] ++;
        }
        
        n = item.getAddDefencePercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Defense +" + n + "%";
            lineRef[1] ++;
        }
        if(n < 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Defense " + n + "%";
            lineRef[1] ++;
        }

        n = item.getAddLife();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Life +" + n;
            lineRef[1] ++;
        }

        n = item.getAddMana();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Mana +" + n;
            lineRef[1] ++;
        }

        n = item.getAddStr();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Strength +" + n;
            lineRef[1] ++;
        }

        n = item.getAddDex();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Dexterity +" + n;
            lineRef[1] ++;
        }

        n = item.getAddInt();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Intelligence +" + n;
            lineRef[1] ++;
        }

        n = item.getAddWis();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Wisdom +" + n;
            lineRef[1] ++;
        }

        n = item.getResFirePercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Resist fire +" + n + "%";
            lineRef[1] ++;
        }
        
        n = item.getResIcePercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Resist ice +" + n + "%";
            lineRef[1] ++;
        }

        n = item.getResLightPercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Resist lightning +" + n + "%";
            lineRef[1] ++;
        }

        n = item.getResUndefPercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Resist poison +" + n + "%";
            lineRef[1] ++;
        }

        n = item.getResFearPercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Resist fear +" + n + "%";
            lineRef[1] ++;
        }

        n = item.getResChaosPercent();
        if(n > 0)
        {
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = "Resist chaos +" + n + "%";
            lineRef[1] ++;
        }
    }
    
    public static void fillGemItemInfo(Item item, String [] lines, int [] colors, int [] lineRef,
                            int defaultColor, int stateColor)
    {
        int n = item.getStackWeight();
        colors[lineRef[0]] = defaultColor;
        lines[lineRef[0]++] = "Weight: " + n + " g";
        
        String key = item.getKey();
        int p = key.lastIndexOf('_');
        int gemQuality = Integer.parseInt(key.substring(p+1));
        
        // colors[lineRef[0]] = defaultColor;
        // lines[lineRef[0]++] = "Quality: " + gemQuality;
        
        int gemType;
        
        switch(key.charAt(0))
        {
            case 'r':
                gemType = GEM_RED;
                break;
            case 'v':
                gemType = GEM_VIOLET;
                break;
            case 'p':
                gemType = GEM_PURPLE;
                break;
            case 'c':
                if(key.charAt(1) == 'y') gemType = GEM_CYAN; else gemType = GEM_CHARTREUSE;
                break;
            case 'g':
                gemType = GEM_GREEN;
                break;
            case 'o':
                gemType = GEM_ORANGE;
                break;
            case 'b':
                gemType = GEM_BRIGHT;
                break;
            case 'd':
                gemType = GEM_DARK;
                break;
            default:
                System.err.println("unknown gem type: " + key);
                gemType = GEM_RED;
        }
        
        for(int i=0; i<slotNames.length; i++)
        {
            int mod = GEM_MOD_TABLE[gemType][i];
            
            int modPower = modBases[mod] * gemQuality;
            
            colors[lineRef[0]] = defaultColor;
            lines[lineRef[0]++] = slotNames[i] + modPower + modNames[mod];
        }
        
    }    
}
