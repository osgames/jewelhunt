//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.item;

import impcity.game.Features;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import jewelhunt.game.Texture;
import jewelhunt.ogl.IsoDisplay;
import rlgamekit.item.ItemCatalog;

/**
 * Item wrapper for inventory handling.
 * 
 * @author Hj. Malthaner
 */
public class InventoryItem 
{
    public final Item item;
    public final Rectangle area;
    
    public InventoryItem(Item item) 
    {
        this.item = item;
        this.area = new Rectangle(0, 0, 
                                  item.getInventoryWidth(), item.getInventoryHeight());
    }

    public InventoryItem(BufferedReader reader, ItemCatalog catalog) throws IOException 
    {
        String line;
        
        line = reader.readLine();
        int x = Integer.parseInt(line.substring(2));

        line = reader.readLine();
        int y = Integer.parseInt(line.substring(2));
        
        item = new Item(reader, catalog);
        
        this.area = new Rectangle(x, y, 
                                  item.getInventoryWidth(), item.getInventoryHeight());
        
    }
    
    public void displayItemStack(IsoDisplay display, int x, int y, int imageIndex)
    {
        int count = item.getCurrentStackSize();
        Texture tex = display.textureCache.getStackTexture(imageIndex, count);
        IsoDisplay.drawTile(tex,
                            x + (area.width * 32 - tex.image.getWidth())/2, 
                            y + (area.height * 32 - tex.image.getHeight())/2,
                            item.getHue());
        
        drawSockets(display, x, y);
    }

    private void drawSockets(IsoDisplay display, int x, int y) 
    {
        int sockets = item.getSocketCount();
        Texture socketTex = display.textureCache.textures[Features.SOCKET];

        int w = Math.min(sockets, area.width);
        int h = (sockets + area.width / 2) / area.width;

        int xoff = (area.width * 32 - w * 32) / 2;
        int yoff = (area.height * 32 - h * 32) / 2;
        
        int margin = (32 - socketTex.image.getWidth()) / 2;
        
        for(int i=0; i<sockets; i++)
        {
            int col = i % area.width;
            int row = i / area.width;
            
            // Hajo: draw socket itself
            IsoDisplay.drawTile(socketTex, 
                                x + xoff + col * 32 + margin, 
                                y + yoff + row * 32 + margin,
                                0x60FFFFFF);
            
            // Hajo: Draw item in Socket
            Item socketedItem = item.getSocketItem(i);
            if(socketedItem != null)
            {
                int itemTexIndex = socketedItem.getInventoryImageIndex();
                Texture itemTex = display.textureCache.textures[itemTexIndex];

                IsoDisplay.drawTile(itemTex, 
                                    x + xoff + col * 32 + (32 - itemTex.image.getWidth()) / 2, 
                                    y + yoff + row * 32 + (32 - itemTex.image.getHeight()) / 2,
                                    0xFFFFFFFF);
            }
        }
    }

    /**
     * Calculate socket index for a position inside the item display
     * @param x x position
     * @param y y position
     * @return socket index or -1 of no socket matched
     */
    public int calcSocketIndex(int x, int y) 
    {
        int sockets = item.getSocketCount();

        int w = Math.min(sockets, area.width);
        int h = (sockets + area.width / 2) / area.width;

        int xoff = (area.width * 32 - w * 32) / 2;
        int yoff = (area.height * 32 - h * 32) / 2;
        
        for(int i=0; i<sockets; i++)
        {
            int col = i % area.width;
            int row = i / area.width;
            
            int sockX = xoff + col * 32;
            int sockY = yoff + row * 32;

            if(sockX <= x && sockY <= y && sockX + 32 > x && sockY + 32 > y)
            {
                return i;
            }
        }
        
        return -1;
    }

    public void write(Writer writer) throws IOException
    {
        writer.write("x=" + area.x + "\n");
        writer.write("y=" + area.y + "\n");
        item.write(writer);
    }
}
