//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.item;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.map.Map;
import jewelhunt.ui.Colors;
import rlgamekit.item.AbstractItem;
import rlgamekit.item.ItemCatalog;
import rlgamekit.item.ItemConfiguration;
import rlgamekit.item.Triplet;
import rlgamekit.objects.Registry;

/**
 * Item creation is encapsulated here.
 * 
 * @author Hj. Malthaner
 */
public class ItemFactory
{
    private static final Logger logger = Logger.getLogger(ItemFactory.class.getName());
    
    public final ItemCatalog itemCatalog;
    private final ItemCatalog prefixes;
    private final ItemCatalog postfixes;
    private final AbstractItem [] allItems;
    private final AbstractItem [] allPrefixes;
    private final AbstractItem [] allPostfixes;
    private final ArrayList <AbstractItem> allCreatableItems;
   
    public ItemFactory()
    {
        this.itemCatalog = loadItemCatalog("item_catalog.icat");
        this.prefixes = loadItemCatalog("prefixes.icat");
        this.postfixes = loadItemCatalog("postfixes.icat");
        allItems = itemCatalog.getAllItemsSorted();
        allPrefixes = prefixes.getAllItemsSorted();
        allPostfixes = postfixes.getAllItemsSorted();

        allCreatableItems = filterForCreatable(allItems);
    
        // dumpCatalogInfo();
    }

    public void dropItem(Registry<Item> items, 
                         String tier, String itemClass, String material, double magic,
                         Map map, int mapI, int mapJ)
    {
        Item item = createRandomItem(items, tier, itemClass, material, magic);

        if("money".equals(item.getItemClass()))
        {
            item.setCurrentStackSize(1 + (int)(Math.random() * 10));
        }

        map.dropItem(mapI, mapJ, item.getRegistryKey());
    }
    
    
    
    public Item createItem(String itemKey, Registry<Item> items)
    {
        Item item = new Item(itemKey, itemCatalog);

        long uid = createUid();
        
        item.setID(uid);
        
        item.setIdentifiedName(item.getName());
        item.setIdentified(1);
        
        int durability = item.getBaseDurability();
        int currentDurability = 2 + (int)(Math.random() * durability + Math.random() * durability) / 2;
        
        item.setCurrentDurability(currentDurability);
        item.setHue(0xFFFFFFFF);

        item.setCurrentStackSize(1);
        
        int regKey = items.nextFreeKey();
        item.setRegistryKey(regKey);
        items.put(regKey, item);
        
        return item;
    }

    public Item createRandomItem(Registry<Item> items, String itemClass)
    {
        // Todo: make it more efficient
        
        Item item;
        
        do
        {
            int index = (int)(Math.random() * allCreatableItems.size());
            item = createItem(allCreatableItems.get(index).getKey(), items);
        }
        while(!item.getItemClass().contains(itemClass));
        
        double magicLevel = Math.random();
        addMagic(item, magicLevel);
        
        // Hajo: todo - reasonable socket limits
        addSockets(item, 6);
        
        return item;
    }
    
    /**
     * This method considers item rarity!
     * 
     * @param items Item catalog
     * @param treasureClass Only create items from this treasure class. Pass null to switch off test.
     * @param itemClass Only create items from this item class. Pass null to switch off test.
     * @param material Only create items with this material. Pass null to switch off test.
     * @param magic How magic should the item be? Pass 0 for non-magic items
     * 
     * @return The created item. Can be null if nothing matches.
     */
    public Item createRandomItem(Registry<Item> items, 
                                 String treasureClass, 
                                 String itemClass, 
                                 String material,
                                 double magic)
    {
        ArrayList <AbstractItem> possibleItems = new ArrayList<AbstractItem>();
        
        for(AbstractItem item : allCreatableItems)
        {
            int rarity = item.getInt(7);
            
            if(rarity > 0)
            {
                boolean matches = true;

                if(itemClass != null)
                {
                    matches &= item.getString(2).contains(itemClass);
                }
                
                if(!matches)
                {
                    // System.err.println("item=" + item.getString(0) + " fails on itemClass=" +  itemClass);
                    continue;
                }     

                if(treasureClass != null)
                {
                    matches &= item.getString(4).contains(treasureClass);
                }
                
                if(!matches)
                {
                    // System.err.println("item=" + item.getString(0) + " fails on treasureClass=" + treasureClass);
                    continue;
                }     
                
                if(material != null)
                {
                    matches &= item.getString(5).contains(material);
                }
                
                if(!matches)
                {
                    // System.err.println("item=" + item.getString(0) + " fails on material=" + material);
                    continue;
                }     
                
                if(matches)
                {
                    possibleItems.add(item);
                }
            }
        }
        
        Item result = null;

        logger.log(Level.INFO, "Got {3} items for itemClass={0}, treasureClass={1} and material={2}", new Object[]{itemClass, treasureClass, material, possibleItems.size()});
        if(possibleItems.size() > 0)
        {
            double [] weights = new double[possibleItems.size()];
            double sum = 0.0;
            for(int i=0; i<weights.length; i++)
            {
                sum += 1.0 / possibleItems.get(i).getInt(7); // rarity
                weights[i] = sum;
            }

            double index = Math.random() * sum;

            for(int i=0; i<weights.length; i++)
            {
                if(weights[i] >= index)
                {
                    result = createItem(possibleItems.get(i).getKey(), items);
                    break;
                }
            }

            if(result == null)
            {
                logger.log(Level.SEVERE, "No item in weighted list found for weight={0} sum={1}", new Object[]{index, sum});
            }
            if(magic > 0)
            {
                double magicLevel = Math.random() * magic;
                addMagic(result, magicLevel);
            }
        }

        return result;
    }

    public Item createRandomItem(Registry<Item> items)
    {
        int index = (int)(Math.random() * allCreatableItems.size());

        Item item = createItem(allCreatableItems.get(index).getKey(), items);

        double magicLevel = Math.random();
        addMagic(item, magicLevel);
        
        return item;
    }
    
    private void addMagic(Item item, double magicLevel)
    {
        if(magicLevel > 0.97)
        {
            makeRareItem(item);
        }
        else if(magicLevel > 0.91)
        {
            addPostfix(item);
            addPrefix(item);
        }
        else if(magicLevel > 0.80)
        {
            if(Math.random() < 0.5)
            {
                addPrefix(item);
            }
            else
            {
                addPostfix(item);
            }
        }
    }

    private static ItemCatalog loadItemCatalog(String filename)
    {
        ItemConfiguration itemConfiguration = new ItemConfiguration();
        ItemCatalog itemCatalog = null;

        try 
        {
            InputStream istream;
            BufferedReader reader;
                        
            itemCatalog = new ItemCatalog(itemConfiguration);

            istream = Class.class.getResourceAsStream("/jewelhunt/resources/" + filename);
            reader = new BufferedReader(new InputStreamReader(istream));

            itemCatalog.read(reader, false);
        }
        catch (IOException ex) 
        {
            logger.log(Level.SEVERE, null, ex);
        }
        
        return itemCatalog;
    }

    private void addPrefix(Item item)
    {
        ArrayList <AbstractItem> affixList = getFilteredAffixList(allPrefixes, item);

        if(affixList.size() > 0)
        {
            AbstractItem affix = setRandomItemAffix(affixList, item);
            
            String name = affix.getString(0) + " " + item.getIdentifiedName();
            item.setIdentifiedName(name);
        }
    }

    private void addPostfix(Item item)
    {
        ArrayList <AbstractItem> affixList = getFilteredAffixList(allPostfixes, item);
        
        if(affixList.size() > 0)
        {
            AbstractItem affix = setRandomItemAffix(affixList, item);

            String name = item.getIdentifiedName() + " " + affix.getString(0);
            item.setIdentifiedName(name);
        }
    }

    private void makeRareItem(Item item) 
    {
        ArrayList <AbstractItem> affixList;
        
        affixList = getFilteredAffixList(allPostfixes, item);
        addAffixes(item, affixList, 3);
        affixList = getFilteredAffixList(allPrefixes, item);
        addAffixes(item, affixList, 4);
        
        if(item.countMods() > 0)
        {
            String name = "Rare " + item.getName();
            item.setIdentifiedName(name);
            item.setHue(Colors.MAGENTA.getRGB());
        }
    }

    private void addAffixes(Item item, ArrayList <AbstractItem> affixList, int n)
    {
        for(int i=0; i<n; i++)
        {
            if(affixList.size() > 0)
            {
                AbstractItem affix = setRandomItemAffix(affixList, item);
                affixList.remove(affix);
            }
        }
    }
    
    private static ArrayList <AbstractItem> getFilteredAffixList(AbstractItem [] affixes, Item item)
    {
        ArrayList <AbstractItem> affixList = new ArrayList<AbstractItem>();
        String itemClass = item.getItemClass();
        
        if(!itemClass.isEmpty())
        {
            for(AbstractItem affix : affixes)
            {
                if(affix.getString(2).contains(itemClass))
                {
                    affixList.add(affix);
                }
            }
        }
        
        return affixList;
    }

    private static AbstractItem setRandomItemAffix(ArrayList <AbstractItem> affixList, Item item)
    {
        int index = (int)(Math.random() * affixList.size());
        AbstractItem affix = affixList.get(index);

        String stat = affix.getString(1);
        String colorString = affix.getString(2);
        
        // System.err.println("stat=" + stat);
        
        Color hue = Colors.WHITE;
        if(colorString.length() == 6)
        {
            int rgb = Integer.parseInt(colorString, 16);
            hue = new Color(rgb);
        }
                
        if("life".equals(stat)) 
        {
            int amount = randomFromRange(affix.getTriplet(0));
            item.setAddLife(amount);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(amount*8, 256)));
        }
        if("mana".equals(stat)) 
        {
            int amount = randomFromRange(affix.getTriplet(0));
            item.setAddMana(amount);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(amount*8, 256)));
        }
        if("dmg".equals(stat)) 
        {
            int dam = randomFromRange(affix.getTriplet(0));
            item.setAddDamagePercent(dam);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(dam*4, 256)));
        }
        if("def".equals(stat))
        {
            int def = randomFromRange(affix.getTriplet(0));
            item.setAddDefencePercent(def);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(def*4, 256)));
        }
        if("str".equals(stat))
        {
            int v = randomFromRange(affix.getTriplet(0));
            item.setAddStr(v);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(v*4, 256)));
        }
        if("dex".equals(stat))
        {
            int v = randomFromRange(affix.getTriplet(0));
            item.setAddDex(v);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(v*4, 256)));
        }
        if("int".equals(stat))
        {
            int v = randomFromRange(affix.getTriplet(0));
            item.setAddInt(v);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(v*4, 256)));
        }
        if("wis".equals(stat))
        {
            int v = randomFromRange(affix.getTriplet(0));
            item.setAddWis(v);

            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(v*4, 256)));
        }
        if("res_fire".equals(stat))
        {
            int resFire = randomFromRange(affix.getTriplet(0));
            item.setResFirePercent(resFire);
            
            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(resFire*8, 256)));
        }
        if("res_ice".equals(stat))
        {
            int amount = randomFromRange(affix.getTriplet(0));
            item.setResIcePercent(amount);
            
            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(amount*8, 256)));
        }
        if("res_light".equals(stat))
        {
            int amount = randomFromRange(affix.getTriplet(0));
            item.setResLightPercent(amount);
            
            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(amount*8, 256)));
        }
        if("res_poison".equals(stat))
        {
            int amount = randomFromRange(affix.getTriplet(0));
            item.setResPoisonPercent(amount);
            
            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(amount*8, 256)));
        }
        if("res_fear".equals(stat))
        {
            int amount = randomFromRange(affix.getTriplet(0));
            item.setResFearPercent(amount);
            
            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(amount*8, 256)));
        }
        if("res_conf".equals(stat))
        {
            int amount = randomFromRange(affix.getTriplet(0));
            item.setResChaosPercent(amount);
            
            item.setHue(Colors.mix(hue, Colors.WHITE, Math.min(amount*8, 256)));
        }
        
        return affix;
    }
            
    private static int randomFromRange(Triplet triplet)
    {
        int min = triplet.get(0);
        int range = triplet.get(1) - min;
        
        return min + (int)(Math.random() * range);
    }

    private ArrayList<AbstractItem> filterForCreatable(AbstractItem[] allItems)
    {
        ArrayList<AbstractItem> result = new ArrayList<AbstractItem>(allItems.length);
        
        for(AbstractItem item : allItems)
        {
            if(!item.getString(3).contains("nocreate"))
            {
                result.add(item);
            }
        }
        
        return result;
    }

    private String makeRareItemName(String itemClass)
    {
        // helms
        // head, caput, noggin, nut, bonce, skull, cranium
        
        return "Nut Protector";
    }

    public static void main(String [] args)
    {
        ItemFactory factory = new ItemFactory();
        factory.dumpCatalogInfo();
    }
    
    private void dumpCatalogInfo() 
    {
        HashMap<String, Integer> itemClasses = new HashMap<String, Integer>();
        
        for(AbstractItem item : allCreatableItems)
        {
            String itemClass = item.getString(2);
            
            // System.out.println(": " + itemClass);
            
            Integer count = itemClasses.get(itemClass);
            
            if(count == null) 
            {
                count = 1;
            }
            else
            {
                count += 1;
            }
            
            itemClasses.put(itemClass, count);
        }

        Set<String> classNames = itemClasses.keySet();
        
        for(String itemClass : classNames)
        {
            System.out.println("Class " + itemClass + " has " + itemClasses.get(itemClass) + " entries.");
        }    
        
        // Hajo: check if affixes match the classes? Todo
        
        
        // Hajo: dump affix lists
        
        System.out.println("\nPrefixes:\n");
        
        AbstractItem [] sortedPrefixes = prefixes.getAllItemsSorted();
        
        for(AbstractItem affix : sortedPrefixes)
        {
            dumpAffix(affix, classNames);
        }
        
        System.out.println("\nPostfixes:\n");
        
        AbstractItem [] sortedPostfixes = postfixes.getAllItemsSorted();
        
        for(AbstractItem affix : sortedPostfixes)
        {
            dumpAffix(affix, classNames);
        }
    }

    private void dumpAffix(AbstractItem affix, Set<String> classNames) 
    {
        Triplet range = affix.getTriplet(0);
        System.out.print("" + affix.getKey() + "\t'" + affix.getString(0) + "'\t+" + 
                affix.getString(1) + "\t" + range.get(0) + "-" + range.get(1));

        if(range.get(2) != 0)
        {
            System.out.print(" (+" + range.get(2) + ")");
        }

        System.out.println("\t" + affix.getString(2));
        
        checkClassMatch(affix, classNames);
    }

    private void checkClassMatch(AbstractItem affix, Set<String> classNames)
    {
        String [] classes = affix.getString(2).split(",");
        
        for(String className : classes)
        {
            className = className.trim();
            if(!classNames.contains(className))
            {
                System.out.println("Affix class=" + className + " does not match any item.");
            }
        }
        
/*        
        for(String itemClass : classNames)
        {
            System.out.println("Class " + itemClass + " has " + itemClasses.get(itemClass) + " entries.");
        }    
*/        
    }

    public void addSockets(Item item, int limit) 
    {
        // Hajo: adjust limit by item class
        String itemClass = item.getItemClass();
        
        if(itemClass.contains("gem"))
        {
            limit = 0;
        }
        else if(itemClass.contains("flask"))
        {
            limit = 0;
        } 
        
        
        int max = item.getInventoryWidth() * item.getInventoryHeight();
        
        max = Math.min(max, limit);
        
        int sockets = (int)(Math.random() * max);
        
        item.setSocketCount(sockets);
    }

    private long createUid() 
    {
        long time = System.currentTimeMillis();
        try 
        {
            Thread.sleep(2);
        } catch (InterruptedException ex) {
            Logger.getLogger(ItemFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return time;
    }
}
