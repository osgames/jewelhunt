//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game;

/**
 *
 * @author Hj. Malthaner
 */
public class InventoryGrid
{
    public static final int WIDTH = 14;
    public static final int HEIGHT = 7;

    public static final int MERCHANT_WIDTH = 14;
    public static final int MERCHANT_HEIGHT = 20;
}
