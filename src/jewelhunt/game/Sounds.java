//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game;

/**
 * Sound sample indexes as constants.
 * 
 * @author Hj. Malthaner
 */
public class Sounds
{
    public static final int ITEM_DROP = 0;
    public static final int ITEM_PICKUP = 1;
}
