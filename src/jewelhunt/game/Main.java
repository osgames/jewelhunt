//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class implements game lifecycle tasks.
 * 
 * @author Hj. Malthaner
 */
public class Main 
{
    public static final Logger logger = Logger.getLogger(Main.class.getName());
    
    public static void main(String[] args)
    {
        Game game = null;
        try
        {
            game = new Game();
            game.run();
        } 
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, ex.toString(), ex);
        } 
        finally
        {
            if(game != null)
            {
                try
                {
                    game.destroy();
                }
                catch (Exception ex)
                {
                    logger.log(Level.SEVERE, ex.toString(), ex);
                } 
            }
        }
    }
}
