//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game;

/**
 *
 * @author Hj. Malthaner
 */
public interface GameInterface
{
    public World getWorld();

    public int getPlayerKey();
}
