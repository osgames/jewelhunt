//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.ai;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import jewelhunt.game.World;
import jewelhunt.game.animation.Animation;
import jewelhunt.game.animation.MeleeAttackAnimation;
import jewelhunt.game.combat.Damage;
import jewelhunt.game.combat.DamageAttack;
import jewelhunt.game.map.DefaultPathSource;
import jewelhunt.game.map.LocationPathDestination;
import jewelhunt.game.player.Player;
import jewelhunt.net.Server;
import rlgamekit.pathfinding.Path;

/**
 *
 * @author Hj. Malthaner
 */
public class EnemyAi implements AI
{
    private static enum Goal
    {
        WATCH_FOR_PLAYER,
        FIND_PATH_TO_PLAYER,
        WAIT_FOR_PATH_TO_PLAYER,
        GO_TO_PLAYER,
        ATTACK_PLAYER,
    }
        
    private final Point home;
    private final World world;
    private Goal goal;
    private long nextActionTime;
    
    public EnemyAi(World world) 
    {
        this.world = world;
        this.home = new Point();
        goal = Goal.WATCH_FOR_PLAYER;
        nextActionTime = System.currentTimeMillis();
    }
    
    public void setHome(int x, int y)
    {
        home.x = x;
        home.y = y;
    }
    
    
    /**
     * This method is called each frame while the 
     * creature has no path (path == null).
     * 
     * @param mob The creature which needs to think
     */
    @Override
    public void think(Player mob)
    {
        long time = System.currentTimeMillis();
     
        // Debug states
        // mob.visuals.setMessage(goal.toString(), 0xFFFFFFFF);
        
        if(time > nextActionTime)
        {
            if(goal == Goal.WATCH_FOR_PLAYER)
            {
                int awarenessRange = 20 + (int)(Math.random() * 20);
                int distance = calcDistanceToPlayer(mob);
                if(distance <= awarenessRange)
                {
                    goal = Goal.FIND_PATH_TO_PLAYER;
                }
            }
            else if(goal == Goal.ATTACK_PLAYER)
            {
                int distance = calcDistanceToPlayer(mob);
                int weaponRange = 6; // Todo: find real value

                if(distance <= weaponRange)
                {
                    attackPlayer(mob);
                }
                else
                {
                    goal = Goal.FIND_PATH_TO_PLAYER;
                }
            }
            else if(goal == Goal.GO_TO_PLAYER)
            {
                int distance = calcDistanceToPlayer(mob);
                int weaponRange = 6; // Todo: find real value

                if(distance <= weaponRange)
                {
                    attackPlayer(mob);
                }
                else
                {
                    goal = Goal.FIND_PATH_TO_PLAYER;
                }
            }
        }
    }
    
    /**
     * This method is called each frame while the 
     * creature has no path (path == null). It is called
     * right after the think() method.
     * 
     * @param server
     * @param mob The creature which needs to think
     */
    @Override
    public void findNewPath(Server server, Player mob)
    {
        if(goal == Goal.FIND_PATH_TO_PLAYER)
        {
            Player player = findPlayer();
            
            Path path = new Path();
            
            boolean ok = 
                path.findPath(new DefaultPathSource(mob.gameMap), 
                              new LocationPathDestination(mob.gameMap, 
                                                          player.location.x, 
                                                          player.location.y,
                                                          2), 
                              mob.location.x, mob.location.y);
            
            if(ok)
            {
                // Hajo: only walk near the player, not into them.
                int length = path.length();
                Path.Node node = path.getStep(length-2);

                if(node != null)
                {
                    int x = node.x;
                    int y = node.y;

                    mob.walkTo(server, x, y);
                    // mob.visuals.setBubble(Features.BUBBLE_GO_WATER);
                    goal = Goal.WAIT_FOR_PATH_TO_PLAYER;
                }
                else
                {
                    // Hajo: already near the player
                    goal = Goal.ATTACK_PLAYER;
                    mob.setPath(null);
                }
            }
            
        }
    }

    /**
     * This method is called after each step on the path
     * 
     * @param mob The creature which needs to think
     */
    @Override
    public void thinkAfterStep(Player mob)
    {
        if(goal == Goal.WAIT_FOR_PATH_TO_PLAYER)
        {
            goal = Goal.GO_TO_PLAYER;
        }
        
        if(goal == Goal.GO_TO_PLAYER)
        {
            int distance = calcDistanceToPlayer(mob);
            int weaponRange = 6; // Todo: find real value
            
            if(distance <= weaponRange)
            {
                goal = Goal.ATTACK_PLAYER;
                mob.setPath(null); // No more walk.
            }
        }
    }

    @Override
    public void write(Writer writer) throws IOException
    {
    }
    
    @Override
    public void read(BufferedReader reader) throws IOException
    {
        
    }


    private void attackPlayer(Player mob) 
    {
        // Damage damage = calculateTotalDamage();
        Damage damage = new Damage(mob, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        DamageAttack attack = new DamageAttack(world, mob, damage);
        Player player = findPlayer();
        
        Animation animation = new MeleeAttackAnimation(mob, attack, player);
        mob.visuals.animation = animation;
        
        nextActionTime = System.currentTimeMillis() + 1000;
    }
    
    private int calcDistanceToPlayer(Player mob)
    {
        Player player = findPlayer();

        int dx = player.location.x - mob.location.x;
        int dy = player.location.y - mob.location.y;
        
        return Math.abs(dx) + Math.abs(dy);
    }
    
    private Player findPlayer()
    {
        // Hajo: this is a hack - there should be a real
        // method to find the keys of players among all 
        // mobs.
        int playerKey = 1;
        Player player = world.mobs.get(playerKey);
        return player;
    }
}
