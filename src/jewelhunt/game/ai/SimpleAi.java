//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.ai;

import impcity.game.Features;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import jewelhunt.game.Clock;
import jewelhunt.game.player.Player;
import jewelhunt.game.map.DefaultPathSource;
import jewelhunt.game.map.FeaturePathDestination;
import jewelhunt.game.map.Map;
import jewelhunt.net.Server;
import rlgamekit.pathfinding.Path;
import rlgamekit.pathfinding.Path.Node;

/**
 *
 * @author Hj. Malthaner
 */
public class SimpleAi implements AI
{

    public enum Goal
    {
        FIND_WELL, GO_DRINK, GO_SLEEP, SLEEPING;
    }
    
    private final Point home;
    public Goal goal;
    private long sleepTime;
    
    public SimpleAi()
    {
        this.home = new Point();
        this.goal = Goal.GO_SLEEP;
    }
    
    public void setHome(int x, int y)
    {
        home.x = x;
        home.y = y;
    }
    
    @Override
    public void think(Player mob)
    {
        if(goal == Goal.GO_SLEEP)
        {
            if(mob.location.equals(home))
            {
                // Hajo: arrived at the lair, now sleep a while
                sleepTime = Clock.time() + (3 + (long)(Math.random() * 20)) * 1000;
                
                goal = Goal.SLEEPING;
            }
        }
        else if(goal == Goal.SLEEPING)
        {
            mob.visuals.setBubble(Features.BUBBLE_SLEEPING);
            if(Clock.time() > sleepTime)
            {
                // time to wake up?
                goal = Goal.FIND_WELL;
                mob.visuals.setBubble(Features.BUBBLE_GO_WATER);
                
                sleepTime = 0;
            }
        }
        else if(goal == Goal.FIND_WELL)
        {
            // nothing to do here
        }
        else if(goal == Goal.GO_DRINK)
        {
            // nothing to do here
        }
        else
        {
            goal = Goal.GO_SLEEP;
        }
    }

    @Override
    public void thinkAfterStep(Player mob)
    {
        if(goal == Goal.GO_DRINK)
        {
            // Are we there yet?
            Path path = mob.getPath();
            
            if(path == null)
            {
                goal = Goal.GO_SLEEP;
            }
        }
    }
    
    @Override
    public void findNewPath(Server server, Player mob)
    {
        if(goal == Goal.GO_SLEEP)
        {
            mob.walkTo(server, home.x, home.y);
            mob.visuals.setBubble(Features.BUBBLE_GO_SLEEPING);
        }
        else if(goal == Goal.FIND_WELL)
        {
            Path path = new Path();
            
            boolean ok = 
                path.findPath(new DefaultPathSource(mob.gameMap), 
                              new FeaturePathDestination(mob.gameMap, Map.FEAT_SMALL_WELL), 
                              mob.location.x, mob.location.y);
            
            if(ok)
            {
                // Hajo: only walk near the well, not into it.
                int length = path.length();
                Node node = path.getStep(length-4);

                if(node != null)
                {
                    int x = node.x;
                    int y = node.y;

                    mob.walkTo(server, x, y);
                    mob.visuals.setBubble(Features.BUBBLE_GO_WATER);
                    goal = Goal.GO_DRINK;
                }
                else
                {
                    // Hajo: already near a well .. now what?
                    goal = Goal.GO_SLEEP;
                    mob.visuals.setBubble(Features.BUBBLE_GO_SLEEPING);
                    mob.iOff = mob.jOff = 0;
                }
            }
            else
            {
                System.err.println("Mob #" + mob.getKey() + " can't find a well.");
            }
        }
    }
    @Override
    public void write(Writer writer) throws IOException
    {
    }
    
    @Override
    public void read(BufferedReader reader) throws IOException
    {
    }
}
