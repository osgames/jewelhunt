//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.ai;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import jewelhunt.game.player.Player;
import jewelhunt.net.Server;

/**
 *
 * @author Hj. Malthaner
 */
public interface AI
{
    /**
     * This method is called each frame while the 
     * creature has no path (path == null).
     * 
     * @param mob The creature which needs to think
     */
    public void think(Player mob);
    
    /**
     * This method is called each frame while the 
     * creature has no path (path == null). It is called
     * right after the think() method.
     * 
     * @param mob The creature which needs to think
     */
    public void findNewPath(Server server, Player mob);

    /**
     * This method is called after each step on the path
     * 
     * @param mob The creature which needs to think
     */
    public void thinkAfterStep(Player mob);

    public void write(Writer writer) throws IOException;
    
    public void read(BufferedReader reader) throws IOException;

}
