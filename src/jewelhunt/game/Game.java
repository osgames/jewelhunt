//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game;

import jewelhunt.ogl.GlTextureCache;
import jewelhunt.ui.GameMouseHandler;
import jewelhunt.ui.GameKeyHandler;
import jewelhunt.ui.GameDisplay;
import jewelhunt.game.player.Species;
import jewelhunt.game.player.Player;
import jewelhunt.game.item.Item;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.ogl.IsoDisplay;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.map.Map;
import jewelhunt.game.map.MapTransitions;
import jewelhunt.game.player.MovementJumping;
import jewelhunt.game.player.PlayerArrivalHandler;
import jewelhunt.net.Server;
import jewelhunt.net.ServerNoNetImpl;
import jewelhunt.oal.SoundPlayer;
import jewelhunt.ui.PostRenderHook;
import org.lwjgl.LWJGLException;
import rlgamekit.objects.Cardinal;


/**
 *
 * @author Hj. Malthaner
 */
public class Game implements PostRenderHook, GameInterface
{
    private static final Logger logger = Logger.getLogger(Game.class.getName());
    private static final String nameVersion = "The Hunt For The Lost Rainbow Jewels, v0.21";
    private final TextureCache textureCache;
    private final IsoDisplay display;
    private final SoundPlayer soundPlayer;
    
    public int mouseI, mouseJ;

    private final int playerKey;
    
    private final Server server;;
    private final World world;
    private final GameDisplay gameDisplay;
    
    public Game() throws LWJGLException, IOException
    {
        Clock.init(System.currentTimeMillis());
        
        mouseI = -1;
        mouseJ = -1;

        world = new World();
        textureCache = new GlTextureCache();

        Player player = new Player(world, 10, 10, Species.GLOBOS_BASE, new Map(16, 16), null, 45, new MovementJumping());
        playerKey = world.mobs.nextFreeKey();
        world.mobs.put(playerKey, player);
        player.setKey(playerKey);
        player.playerArrivalHandler = new PlayerArrivalHandler(world, textureCache);
        
        equipPlayer(player);

        display = new IsoDisplay(world.mobs, world.items, textureCache);
        display.create();
        
        final Texture intro = textureCache.loadTexture("/jewelhunt/resources/ts/61,48,rainbow_temple.png", false);
        
        textureCache.initialize(new TextureCache.LoaderCallback() 
        {
            @Override
            public void update(String msg)
            {
                splash(intro, msg);
            }
        });
        
        world.makeVillageMap(player, MapTransitions.MapId.NONE, 0);
        display.map = player.gameMap;
        
        display.map.recalculateBlockedAreas(display.textureCache.textures);
        display.centerOn(player);
        display.setTitle(nameVersion);
        
        server = new ServerNoNetImpl(display.map, world.mobs);

        gameDisplay = new GameDisplay(this, display);
        
        
        soundPlayer = new SoundPlayer();
        soundPlayer.init();
        
        // Load the wav data.
        String PATH = "/jewelhunt/resources/sfx/";
        String [] sampleFiles = new String []
        {
            PATH + "handle_small_leather.wav",
            PATH + "handle_belt_1.wav"
        };

        if(!soundPlayer.loadSamples(sampleFiles))
        {
            System.err.println("Error loading sound data.");
        }
        
        display.setDecoDisplayName(Map.DECO_ARMORY_MERCHANT, "Blacksmith");
        display.setDecoDisplayName(Map.DECO_GENERAL_MERCHANT, "General Store");
    }

    @Override
    public World getWorld()
    {
        return world;
    }

    @Override
    public int getPlayerKey()
    {
        return playerKey;
    }
    
    public void run()
    {
        display.postRenderHook = this;
        display.mouseHandler = new GameMouseHandler(this, gameDisplay, display, soundPlayer);
        display.keyHandler = new GameKeyHandler(this, display, gameDisplay);
        display.run();
    }
    
    public void destroy()
    {
        display.destroy();
        soundPlayer.destroy();
        server.shutdown();
    }
    
    @Override
    public void displayMore() 
    {
        gameDisplay.displayMore();

        // display.font.drawStringScaled("i=" + mouseI + " j=" + mouseJ, 0xBFFF20, 10, IsoDisplay.DISPLAY_HEIGHT - 120, 0.8);
        
        update();
    }
    
    private void update()
    {
        try
        {
            Set<Cardinal> keys = world.mobs.keySet();

            for(Cardinal key : keys)
            {
                Player mob = world.mobs.get(key.intValue());

                if(mob.getPath() != null)
                {
                    mob.advance(soundPlayer);

                    if(key.intValue() == playerKey)
                    {
                        display.centerOn(mob);
                    }
                }

                mob.update(server);
                
                /*
                if(mob.path == null 
                        && key.intValue() != playerKey
                        && mob.getMode() != Player.Mode.IDLE)
                {
                    // Hajo: dummy test code

                    int x = mob.location.x;
                    int y = mob.location.y;

                    // System.err.println("Imp is at " + x + ", " + y);

                    x += (int)(Math.random()*20 - 10);
                    y += (int)(Math.random()*20 - 10);

                    server.moveTo(key.intValue(), x, y);

                    // System.err.println("Imp want to move to " + x + ", " + y);
                }
                */
            }
        } 
        catch(ConcurrentModificationException cmex)
        {
            // Hajo: this can happen while loading the map ...
            // Todo: find a way to handle this cleanly.
            logger.log(Level.INFO, cmex.getMessage());
        }

        server.runJobs();
    }
    
    public void walkPlayer() 
    {
        Player player = world.mobs.get(playerKey);
        player.walkTo(server, mouseI, mouseJ);
    }

    private void equipPlayer(Player player) 
    {
        int life = player.calculateMaxLife();
        player.stats.setMax(Player.I_VIT, life);
        player.stats.setCurrent(Player.I_VIT, life << 8);
        
        int mana = player.calculateMaxMana();
        player.stats.setMax(Player.I_MANA, mana);
        player.stats.setCurrent(Player.I_MANA, mana << 8);

        Item item;
        
        /*
        item = world.itemFactory.createItem("small_shield", world.items);
        player.equipment.setItem(Player.SLOT_SECOND_HAND, item);
        */
        
        item = world.itemFactory.createItem("mogrisphere", world.items);
        player.addToInventory(new InventoryItem(item), InventoryGrid.WIDTH, InventoryGrid.HEIGHT);

        /*
        item = world.itemFactory.createItem("violet_rainbow_jewel", world.items);
        player.addToInventory(new InventoryItem(item), InventoryGrid.WIDTH, InventoryGrid.HEIGHT);
        */
        
        item = world.itemFactory.createItem("wooden_club", world.items);
        player.equipment.setItem(Player.SLOT_FIRST_HAND, item);

        item = world.itemFactory.createItem("shirt_1", world.items);
        player.equipment.setItem(Player.SLOT_BODY, item);

        item = world.itemFactory.createItem("pants_1", world.items);
        player.equipment.setItem(Player.SLOT_LEGS, item);

        /*
        item = world.itemFactory.createItem("wooden_shield", world.items);
        InventoryItem shield = new InventoryItem(item);
        player.addToInventory(shield, InventoryGrid.WIDTH, InventoryGrid.HEIGHT);
        */
        
        // some money

        for(int i=0; i<15; i++)
        {
            item = world.itemFactory.createItem("coins_copper", world.items);
            InventoryItem invItem = new InventoryItem(item);        
            player.addToInventory(invItem, InventoryGrid.WIDTH, InventoryGrid.HEIGHT);
        }
        
        /*
        for(int i=0; i<16; i++)
        {
            item = world.itemFactory.createRandomItem(world.items);
            InventoryItem invItem = new InventoryItem(item);        
            player.addToInventory(invItem, InventoryGrid.WIDTH, InventoryGrid.HEIGHT);            
        }
        */
    }
    
    private void splash(Texture intro, String msg)
    {
        display.clear();
        IsoDisplay.drawTile(intro, 1200 - intro.image.getWidth()-200, 70);
        display.font.drawString(nameVersion, 0xFFFFFF, 120, 580);
        display.font.drawStringScaled(msg, 0xDDDDDD, 120, 540, 0.8);
        display.update();
    }
}
