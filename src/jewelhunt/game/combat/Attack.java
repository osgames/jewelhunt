//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.combat;

import jewelhunt.game.player.Player;
import jewelhunt.game.World;

/**
 * Models an attack in combat.
 * 
 * @author Hj. Malthaner
 */
public abstract class Attack 
{
    protected final World world;
    protected final Player attacker;
    
    public Attack(World world, Player attacker)
    {
        this.world = world;
        this.attacker = attacker;
    }

    /**
     * The attack hits the defender. Implementations of this
     * method are supposed to handle all effects.
     * @param defender The defnind mob, i.e. the one which was hit.
     */
    abstract public void hit(Player defender);
}
