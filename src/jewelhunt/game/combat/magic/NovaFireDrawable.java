//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.combat.magic;

import impcity.game.Features;
import jewelhunt.game.Texture;
import jewelhunt.ogl.Drawable;
import jewelhunt.ogl.IsoDisplay;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;

/**
 *
 * @author Hj. Malthaner
 */
public class NovaFireDrawable implements Drawable
{

    @Override
    public void display(IsoDisplay display, int x, int y) 
    {
        // Hajo: Multiply
        // glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);

        // Hajo: Additive
        glBlendFunc(GL_ONE, GL_ONE);            

        // Hajo: Additive with alpha-multiply
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);

        Texture tex = display.textureCache.textures[Features.P_SPELL_CLOUD_1 + (int)(Math.random() * 3)];

        IsoDisplay.drawTile(tex, x-tex.footX, y - tex.image.getHeight() + tex.footY, 
                            tex.image.getWidth(), tex.image.getHeight(),
                            0.7f, 1f, 1f, 1f);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
}
