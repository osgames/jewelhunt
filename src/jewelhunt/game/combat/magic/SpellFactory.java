//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.combat.magic;

import jewelhunt.game.World;
import jewelhunt.game.combat.Attack;
import jewelhunt.game.player.Player;
import jewelhunt.ogl.Drawable;

/**
 *
 * @author Hj. Malthaner
 */
public class SpellFactory 
{
    public static Spell makeNovaTypeSpell(World world, Player attacker, Attack attack, Drawable drawable)
    {
        Spell spell = new NovaTypeSpell(world, attacker, attack, drawable);
        return spell;
    }
}
