//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.combat.magic;

import impcity.game.Features;
import jewelhunt.game.Texture;
import jewelhunt.ogl.Drawable;
import jewelhunt.ogl.IsoDisplay;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.glBlendFunc;

/**
 *
 * @author Hj. Malthaner
 */
public class NovaFrostDrawable implements Drawable
{

    @Override
    public void display(IsoDisplay display, int xpos, int ypos) 
    {
        // Hajo: Multiply
        // glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);

        // Hajo: Additive
        glBlendFunc(GL_ONE, GL_ONE);            

        // Hajo: Additive with alpha-multiply
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);

        
        int count = 3 + (int)(Math.random() * 6);
        for(int i=0; i<count; i++)
        {
            Texture tex = display.textureCache.textures[Features.P_FROST_SPRITE_1 + (int)(Math.random() * 3)];
            int x = xpos + ((int)(Math.random() * 7) - 3) * 2;
            int y = ypos + ((int)(Math.random() * 7) - 3) * 1;
            
            IsoDisplay.drawTile(tex, x-tex.footX, y - tex.image.getHeight() + tex.footY, 
                                tex.image.getWidth(), tex.image.getHeight(),
                                0.9f, 1f, 1f, 1f);
        }
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
}
