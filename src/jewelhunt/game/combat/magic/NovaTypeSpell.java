//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.combat.magic;

import java.util.ArrayList;
import jewelhunt.game.World;
import jewelhunt.game.combat.Attack;
import jewelhunt.game.map.Map;
import jewelhunt.game.player.Player;
import jewelhunt.ogl.Drawable;
import rlgamekit.objects.Cardinal;

    
/**
 *
 * @author Hj. Malthaner
 */
public class NovaTypeSpell extends Spell
{
    private int radius;
    private final int centerI, centerJ;
    
    private final ApplyEffect applyEffect;
    private final ClearEffect clearEffect;
    
    public NovaTypeSpell(World world, Player attacker, Attack attack, Drawable drawable)
    {
        super(world, attacker, attack);
        centerI = attacker.location.x;
        centerJ = attacker.location.y;
    
        this.applyEffect = new ApplyEffect(drawable);
        this.clearEffect = new ClearEffect();
    }
    
    @Override
    public void drive()
    {
        // super.drive();
        
        driveRing(clearEffect);
        radius += 2;
        driveRing(applyEffect);
    }

    private void driveRing(Applier applier)
    {
	int f = 1 - radius;
	int ddF_x = 1;
	int ddF_y = -2 * radius;
	int x = 0;
	int y = radius;

	applier.applyAt(0, +radius);
	applier.applyAt(0, -radius);
	applier.applyAt(+radius, 0);
	applier.applyAt(-radius, 0);

	while(x < y) 
        {
            // ddF_x == 2 * x + 1;
            // ddF_y == -2 * y;
            // f == x*x + y*y - radius*radius + 2*x - y + 1;
            if(f >= 0) 
            {
                y--;
                ddF_y += 2;
                f += ddF_y;
            }

            x++;
            ddF_x += 2;
            f += ddF_x;

            applier.applyAt(+x, +y);
            applier.applyAt(-x, +y);
            applier.applyAt(+x, -y);
            applier.applyAt(-x, -y);
            applier.applyAt(+y, +x);
            applier.applyAt(-y, +x);
            applier.applyAt(+y, -x);
            applier.applyAt(-y, -x);
	}
    }


    /**
     * Casting cost in mana point
     * @return The amount of mana needed for this spell
     */
    @Override
    public int cost()
    {
        return 6;
    }
    
    @Override
    public boolean isExpired()
    {
        return radius > 30;
    }
    
    @Override
    public void end() 
    {
        driveRing(clearEffect);
    }
    
    
    private interface Applier
    {
        public void applyAt(int i, int j);
    }
    
    private class ApplyEffect implements Applier
    {
        private final Drawable drawable;
        private final ArrayList<Cardinal> keys = new ArrayList<Cardinal>(32);
        
        public ApplyEffect(Drawable drawable)
        {
            this.drawable = drawable;
        }
        
        @Override
        public void applyAt(int i, int j)
        {
            Map gameMap = attacker.gameMap;

            int x = centerI + i/2; 
            int y = centerJ + j/2; 
            // gameMap.setItem(x, y, Features.I_COPPER_ORE);

            gameMap.setEffect(x, y, drawable);

            keys.clear();
            keys.addAll(world.mobs.keySet());
            for(Cardinal key : keys)
            {
                Player mob = world.mobs.get(key.intValue());
                
                int xd = mob.location.x - x;
                int yd = mob.location.y - y;
                int catchment = 2;
                if(Math.abs(xd) < catchment && Math.abs(yd) < catchment)
                {
                    if(mob != attacker)
                    {
                        attack.hit(mob);
                    }
                }
            }
        }
    }

    private class ClearEffect implements Applier
    {
        @Override
        public void applyAt(int i, int j)
        {
            Map gameMap = attacker.gameMap;

            int x = centerI + i/2; 
            int y = centerJ + j/2; 
            
            gameMap.setEffect(x, y, null);
        }
    }
}
