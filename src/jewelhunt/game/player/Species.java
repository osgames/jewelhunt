//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.player;

import java.util.HashMap;

/**
 * Constants for all species in the game
 * 
 * @author Hj. Malthaner
 */
public class Species
{
    public static final int CONIANS_BASE = 25;
    public static final int GLOBOS_BASE = 49;
    public static final int IMPS_BASE = 1;
    public static final int POWERSNAILS_BASE = 57;
    public static final int KILLERBEETLES_BASE = 65;
    public static final int WYVERNS_BASE = 73;
    public static final int BOOKWORMS_BASE = 81;
    public static final int MOSQUITOES_BASE = 89;
    public static final int GLOBOS_RC_BASE = 97;
    public static final int BOOKWORMS_RC_BASE = 105;

    public static final HashMap<Integer, SpeciesDescription> speciesTable = new HashMap<Integer, SpeciesDescription>();

    static
    {
        SpeciesDescription globos = 
                new SpeciesDescription("Globo", 
                                       3,                 // size
                                       GLOBOS_BASE,       // base
                                       398,               // lair
                                       2,                 // lair size
                                       new MovementJumping(),
                                       15,                // speed
                                       2,                 // melee range 
                                       1,                 // min dam
                                       3);                // max dam
        
        SpeciesDescription globosRc = 
                new SpeciesDescription("Globo", 
                                       3,                 // size
                                       GLOBOS_RC_BASE,    // base
                                       398,               // lair
                                       2,                 // lair size
                                       new MovementJumping(),
                                       15,                // speed
                                       2,                 // melee range 
                                       1,                 // min dam
                                       3);                // max dam

        SpeciesDescription conians = 
                new SpeciesDescription("Conian", 
                                       3, 
                                       CONIANS_BASE,
                                       398,
                                       2,                 // lair size
                                       new MovementJumping(),
                                       15,                // speed
                                       2, 
                                       1,
                                       3);
        
        SpeciesDescription imps = 
                new SpeciesDescription("Imp", 
                                       3,
                                       IMPS_BASE, 
                                       397, 
                                       2,                 // lair size
                                       new MovementJumping(),
                                       15,                // speed
                                       2, 
                                       1, 
                                       3);
        
        SpeciesDescription powersnails = 
                new SpeciesDescription("Powersnail", 
                                       3, 
                                       POWERSNAILS_BASE, 
                                       396,
                                       3,                 // lair size
                                       new MovementGliding(),
                                       5,                // speed
                                       2, 
                                       1, 
                                       3);
        
        SpeciesDescription killerbeetles = 
                new SpeciesDescription("Killer Beetle", 
                                       4,                 // size 
                                       KILLERBEETLES_BASE,
                                       395,               // lair
                                       4,                 // lair size
                                       new MovementJitter(7, 1),
                                       7,                // speed
                                       2, 
                                       1, 
                                       3);

        SpeciesDescription wyverns = 
                new SpeciesDescription("Wyvern", 
                                       3,                 // size 
                                       WYVERNS_BASE,
                                       394,               // lair
                                       3,                 // lair size
                                       new MovementJitter(7, 2 << 16),
                                       20,                // speed
                                       2, 
                                       1, 
                                       3);
        
        speciesTable.put(GLOBOS_BASE, globos);
        speciesTable.put(GLOBOS_RC_BASE, globosRc);
        speciesTable.put(CONIANS_BASE, conians);
        speciesTable.put(IMPS_BASE, imps);
        speciesTable.put(POWERSNAILS_BASE, powersnails);
        speciesTable.put(KILLERBEETLES_BASE, killerbeetles);
        speciesTable.put(WYVERNS_BASE, wyverns);
    }
    
    
    static boolean isPlayerSpecies(int species)
    {
        return species == CONIANS_BASE ||
               species == GLOBOS_BASE ||
               species == GLOBOS_RC_BASE;
    }
    
}
