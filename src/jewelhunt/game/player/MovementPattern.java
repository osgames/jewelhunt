//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.player;

/**
 *
 * @author Hj. Malthaner
 */
public interface MovementPattern
{
    public void calculateMove(Player mob, int deltaT);
}
