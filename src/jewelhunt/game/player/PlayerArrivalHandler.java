//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.player;

import java.util.logging.Level;
import jewelhunt.game.InventoryGrid;
import jewelhunt.game.Sounds;
import jewelhunt.game.TextureCache;
import jewelhunt.game.World;
import jewelhunt.game.animation.Animation;
import jewelhunt.game.animation.MeleeAttackAnimation;
import jewelhunt.game.combat.Attack;
import jewelhunt.game.combat.Damage;
import jewelhunt.game.combat.DamageAttack;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.map.LocationCallback;
import jewelhunt.game.map.Map;
import jewelhunt.game.map.RectArea;
import static jewelhunt.game.player.Player.logger;
import jewelhunt.oal.SoundPlayer;

/**
 * Game specific action of what to do for player arriving at a location
 * go into this class (and subclasses of this class)
 * 
 * @author Hj. Malthaner
 */
public class PlayerArrivalHandler 
{
    private final World world;
    private final TextureCache textureCache;

    public PlayerArrivalHandler(World world, TextureCache textureCache) 
    {
        this.world = world;
        this.textureCache = textureCache;
    }
    
    
    public void handleArrival(final Player player, final SoundPlayer soundPlayer, int x, int y) 
    {
        world.checkMapChange(textureCache, player, x, y);
        
        
        RectArea area = new RectArea(x, y, 0, 0);
        
        area.spirallyTraverse(new LocationCallback() 
        {
            @Override
            public boolean visit(int x, int y) 
            {
                int mobKey = player.gameMap.getMob(x, y);
                
                if(mobKey != 0 && mobKey != player.getKey())
                {
                    Player mob = world.mobs.get(mobKey);

                    if(mob != null)
                    {
                        // Hajo: only attack if still living
                        if(!mob.isDying)
                        {
                            Damage damage = player.calculateTotalDamage();
                            Attack attack = new DamageAttack(world, player, damage);
                            Animation animation = new MeleeAttackAnimation(player, attack, mob);
                            player.visuals.animation = animation;
                        }
                    }
                    else
                    {
                        logger.log(Level.WARNING, "Mob #{0} is not registered.", mobKey);
                        // gameMap.setMob(x, y, 0);
                    }
                }                
                
                int n = player.gameMap.getItem(x, y);
                if((n & Map.F_FLOOR_DECO) == 0)
                {
                    int itemKey = n & Map.F_ITEM_MASK;
                    if(itemKey != 0)
                    {
                        if((n & Map.F_DECO) != 0)
                        {
                            if(itemKey == Map.FEAT_STAIRS_UP_NARROW ||
                               itemKey == Map.FEAT_STAIRS_DOWN_NARROW ||
                               itemKey == Map.FEAT_ARROW_EAST ||
                               itemKey == Map.FEAT_ARROW_WEST)
                            {
                                world.changeMapOnFeature(textureCache, player, itemKey);
                                return true;
                            }
                        }
                        else
                        {
                            Item item = world.items.get(itemKey);
                            InventoryItem inv = new InventoryItem(item);

                            if(player.addToInventory(inv, InventoryGrid.WIDTH, InventoryGrid.HEIGHT))
                            {
                                soundPlayer.play(Sounds.ITEM_PICKUP, 1.0f);
                                player.gameMap.setItem(x, y, 0);
                                String tags = item.getTags();
                                if(tags.contains("light"))
                                {
                                    player.gameMap.removeLight(x, y);
                                }
                                return true;
                            }
                            else
                            {
                                player.visuals.setMessage("Inventory is full.", 0xFF8833);
                                logger.warning("Inventory is full.");
                            }
                        }
                    }                
                }
                return false;
            }
        }, 
        5);
        
    }
}
