//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.player;

/**
 * Description of the abilities of a monster or animal species
 * 
 * @author Hj. Malthaner
 */
public class SpeciesDescription
{
    public final String name;
    /** Size on the map, measured in sub-grid units */
    public final int size;
    public final int baseImage;
    public final int lair;
    public final int lairSize;

    public final MovementPattern move;
    public final int speed;

    // Other species specific data here

    public final int meleeRange;
    public final int physDamLow;
    public final int physDamHigh;


    public SpeciesDescription(String name,
                              int size,
                              int baseImage,
                              int lair,
                              int lairSize,
                              MovementPattern move,
                              int speed,
                              int meleeRange,
                              int physDamLow,
                              int physDamHigh)
    {
            this.name = name;
            this.size = size;
            this.baseImage = baseImage;
            this.lair = lair;
            this.lairSize = lairSize;
            this.move = move;
            this.speed = speed;
            this.meleeRange = meleeRange;
            this.physDamLow = physDamLow;
            this.physDamHigh = physDamHigh;
    }	
}