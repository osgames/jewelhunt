//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.player;

import java.awt.Point;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jewelhunt.game.Direction;
import jewelhunt.game.World;
import jewelhunt.game.ai.AI;
import jewelhunt.game.combat.Damage;
import jewelhunt.game.item.InventoryItem;
import jewelhunt.game.item.Item;
import jewelhunt.game.map.Map;
import jewelhunt.net.Server;
import jewelhunt.oal.SoundPlayer;
import rlgamekit.equipment.Equipment;
import rlgamekit.equipment.EquipmentListener;
import rlgamekit.item.ItemCatalog;
import rlgamekit.map.data.LayeredMap;
import rlgamekit.pathfinding.Path;
import rlgamekit.stats.Stats;

/**
 * Player data.
 * 
 * @author Hj. Malthaner
 */
public class Player
{
    public static final Logger logger = Logger.getLogger(Player.class.getName());
    
    public static final int I_STR = 0;
    public static final int I_DEX = 1;
    public static final int I_INT = 2;
    public static final int I_WIS = 3;
    public static final int I_VIT = 4;
    public static final int I_MANA = 5;

    public static final int SLOT_HEAD = 0;
    public static final int SLOT_BODY = 1;
    public static final int SLOT_LEGS = 2;
    public static final int SLOT_WAIST = 3;
    public static final int SLOT_HANDS = 4;
    public static final int SLOT_FEET = 5;
    public static final int SLOT_FIRST_HAND = 6;
    public static final int SLOT_SECOND_HAND = 7;
    public static final int SLOT_FIRST_RING = 8;
    public static final int SLOT_SECOND_RING = 9;
    public static final int SLOT_NECK = 10;
    
    private Path path;
    public String name;
    public MobVisuals visuals;
    public MovementPattern pattern = new MovementJumping();
    public PlayerArrivalHandler playerArrivalHandler = null;
    
    private final World world;
    private AI ai;
    public boolean isDying;

    public AI getAi() 
    {
        return ai;
    }

    public void setAi(AI ai) 
    {
        this.ai = ai;
    }
    
    /**
     * Current location of the player
     */
    public final Point location;

    /**
     * The map data that is shown to the player (will not show
     * hidden features or changes to "remembered" areas).
     */
    public LayeredMap displayMap;


    private long lastActionTime;
    
    private int stepsPerSecond;
    
    /** i-axis offset in 1/(1<<16) of a square */
    public int iOff;
    
    /** j-axis offset in 1/(1<<16) of a square */
    public int jOff;

    /** z-axis offset in 1/(1<<16) of a pixel */
    public int zOff;

    /** up/down speed as 16 bit fixed point integer */
    public int zSpeed;
    
    private int species;
    private int key;
    
    /**
     * The actual game map. All game functions should use this.
     */
    public final Map gameMap;
    
    
    /**
     * The players inventory
     */
    public final ArrayList <InventoryItem> inventory;

    /**
     * Equipment list
     */
    public final Equipment <Item> equipment;

    public final Stats stats;

    public int getSpecies()
    {
        return species;
    }

    public int getKey()
    {
        return key;
    }

    public void setKey(int key)
    {
        this.key = key;
    }
                
    public Player(World world, int playerX, int playerY, int species, Map gameMap, AI ai, int speed, MovementPattern pattern)
    {
        this.visuals = new MobVisuals();
        this.world = world;
        this.species = species;
        this.gameMap = gameMap;
        this.location = new Point(playerX, playerY);
        
        this.pattern = pattern;
        this.stepsPerSecond = speed;
        this.ai = ai;
        
        inventory = new ArrayList<InventoryItem>();
        
        String [] slots = 
        {
            "Helm",
            "Body armor",
            "Leg armor",
            "Belt",
            "Gloves",
            "Boots",
            "Weapon",
            "Shield",
            "Ring 1",
            "Ring 2",
            "Amulet",
        };
        
        equipment = new Equipment<Item>(slots);

        stats = new Stats(new String []
        {
            "Str", "Dex", "Int", "Wis", "Vit", "Ext1", "Ext2", "Ext3", "Ext4", "Ext5"
        });

        
        stats.setCurrent(I_STR, 20);
        stats.setCurrent(I_DEX, 20);
        stats.setCurrent(I_INT, 20);
        stats.setCurrent(I_WIS, 20);
        stats.setCurrent(I_VIT, 20);
        
        equipment.addListener(new EquipmentVisualizer());
        
        visuals.setDisplayCode(species);
        
        this.lastActionTime = System.currentTimeMillis();
    }
    
    public int calculateTotalStr() 
    {
        int result = stats.getCurrent(Player.I_STR);
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) result += item.getAddStr();
        }
        return result;
    }
    
    public int calculateTotalDex() 
    {
        int result = stats.getCurrent(Player.I_DEX);
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) result += item.getAddDex();
        }
        return result;
    }
    
    public int calculateTotalWis() 
    {
        int result = stats.getCurrent(Player.I_WIS);
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) result += item.getAddWis();
        }
        return result;
    }
    
    public int calculateTotalInt() 
    {
        int result = stats.getCurrent(Player.I_INT);
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) result += item.getAddInt();
        }
        return result;
    }
    
    public int calculateMaxLife() 
    {
        int life = 0;
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) life += item.getAddLife();
        }
        
        life += stats.getCurrent(I_STR);
        life += stats.getCurrent(I_DEX);
        
        return life;
    }
    
    public int calculateMaxMana() 
    {
        int mana = 0;
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) mana += item.getAddMana();
        }
        
        mana += stats.getCurrent(I_INT);
        mana += stats.getCurrent(I_WIS);
        
        return mana;
    }

    public int calculateStealth() 
    {
        int stealth = 0;
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) stealth += item.getAddStealth();
        }
        
        stealth += stats.getCurrent(I_INT);
        stealth += stats.getCurrent(I_DEX);
        
        return stealth;
    }

    public int calculateBaseRes() 
    {
        int res = 0;
        res += stats.getCurrent(I_STR);
        res += stats.getCurrent(I_WIS);
        
        return res / 2;
    }

    public int calculateDeviceSkill()
    {
        int skill = 0;
        skill += stats.getCurrent(I_DEX);
        skill += stats.getCurrent(I_INT);
        
        return skill / 2;
    }

    public int calcMoney()
    {
        int result = 0;
        for(InventoryItem inventoryItem : inventory)
        {
            if("money".equals(inventoryItem.item.getItemClass()))
            {
                // Hajo: Todo: check coin type (gold, silver ...)
                result += inventoryItem.item.getCurrentStackSize();
            }
        }
        return result;
    }
    
    public void addMoney(int price)
    {
        for(InventoryItem inventoryItem : inventory)
        {
            if("money".equals(inventoryItem.item.getItemClass()))
            {
                // Hajo: Todo: check coin type (gold, silver ...)
                Item item = inventoryItem.item;
                item.setCurrentStackSize(item.getCurrentStackSize() + price);
                return;
            }
        }
    }
    
    public void subMoney(int price)
    {
        for(InventoryItem inventoryItem : inventory)
        {
            if("money".equals(inventoryItem.item.getItemClass()))
            {
                // Hajo: Todo: check coin type (gold, silver ...)
                Item item = inventoryItem.item;
                price = price - item.getCurrentStackSize();
                
                if(price < 0)
                {
                    // Hajo: overpay, some coins are left
                    item.setCurrentStackSize(-price);
                }
                else
                {
                    // Hajo: how to kill this stack?
                }
            }
        }
    }
    
    public void walkTo(Server server, int mouseI, int mouseJ) 
    {
        setPath(null);
        server.moveTo(getKey(), mouseI, mouseJ);
    }

    public void setPath(Path path) 
    {
        this.path = path;

        if(path != null)
        {
            Path.Node node = path.currentStep();

            if(node != null && node.x == location.x && node.y == location.y)
            {
                path.advance();
            }
        }
        
        lastActionTime = System.currentTimeMillis();        
    }
    
    public Path getPath()
    {
        return path;
    }

    public boolean advance(SoundPlayer soundPlayer) 
    {
        long time = System.currentTimeMillis();
        
        int deltaT = (int)(time - lastActionTime);
        boolean lastStep = false;
        
        if(path != null)
        {
            Path.Node node = path.currentStep();
            
            if(node != null)
            {                
                int steps = deltaT * stepsPerSecond * (1 << 16) / 1000;

                // System.err.println("Move: " + time + " steps=" + steps);
                
                int dx = node.x - location.x;
                int dy = node.y - location.y;

                assert(dx >= -1 && dx <= 1);
                assert(dy >= -1 && dy <= 1);
                
                iOff += dx * steps;
                jOff += dy * steps;
            
                // System.err.println("Mob #" + key + " steps=" + steps);
                
                // System.err.println("Mob #" + key + " ioff=" + (iOff/(double)(1<<16)) + " jOff=" + (jOff/(double)(1<<16)));

                boolean needHop;
                needHop = false;

                if(iOff > (1<<15)) 
                {
                    needHop = true;
                }
                if(iOff < -(1<<15)) 
                {
                    needHop = true;
                }

                if(jOff > (1<<15)) 
                {
                    needHop = true;
                }
                if(jOff < -(1<<15)) 
                {
                    needHop = true;
                }

                if(needHop && path != null)
                {
                    hop(soundPlayer);
                    iOff = dx * -(1<<15);
                    jOff = dy * -(1<<15);
                }
                
                assert(iOff >= -(1<<15) && iOff <= (1<<15));
                assert(jOff >= -(1<<15) && jOff <= (1<<15));
                
                pattern.calculateMove(this, deltaT);
            }
            else
            {
                zOff = iOff = jOff = 0;
                lastStep = true;
                setPath(null);

                
                // Hajo: is there something to do here?

                if(ai == null && playerArrivalHandler != null)
                {
                    playerArrivalHandler.handleArrival(this, soundPlayer, location.x, location.y);
                }
            }
        }

        lastActionTime = time;
        
        if(ai != null)
        {
            ai.thinkAfterStep(this);
        }
        
        return lastStep;
    }
    
    /**
     * Take a hop from one square to the next on the path.
     */
    private void hop(SoundPlayer soundPlayer) 
    {
        Path.Node node = path.currentStep();
        if(node != null)
        {
            gameMap.setMob(location.x, location.y, 0);

            int dx = node.x - location.x;
            int dy = node.y - location.y;

            location.x = node.x;
            location.y = node.y;

            // System.err.println("Mob #" + key + " hops to " + location.x + ", " + location.y);

            gameMap.setMob(location.x, location.y, key);

            path.advance();

            visuals.setDisplayCode(species + Direction.dirFromVector(dx, dy));
        }
        else
        {
            zOff = iOff = jOff = 0;
            
            setPath(null);

            // Hajo: is there something to do here?

            if(ai == null && playerArrivalHandler != null)
            {
                playerArrivalHandler.handleArrival(this, soundPlayer, location.x, location.y);
            }
        }
    }
    
    public void update(Server server) 
    {
        // Hajo: Avoid invisible monsters (can happen if the first of a "stack" dies
        gameMap.setMob(location.x, location.y, getKey());

        // Hajo: TODO: recovery should depend on real time
        // 
        // Hajo: stored with 8 precision bits.
        int mana = stats.getCurrent(I_MANA);
        mana += 5;
        if(stats.getMax(I_MANA) >= mana >> 8)
        {
            stats.setCurrent(I_MANA, mana);
        }
              
        if(path == null && ai != null)
        {
            // Hajo: we need a new path ...
            // ... but only if we are still alive
            if(stats.getCurrent(I_VIT) >= 0)
            {
                ai.think(this);
                ai.findNewPath(server, this);
            }
        }
    }
    
    public int calculateTotalBurden() 
    {
        int burden = 0;

        for(InventoryItem item : inventory) 
        {
            burden += item.item.getBurden();
        }
        
        for(int i=0; i < equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) burden += item.getBurden() / 2;
        }
        
        return burden;
    }
    
    public int calculateTotalDef() 
    {
        int def = 0;
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) def += item.calcTotalDefense();
        }
        
        def += stats.getCurrent(I_STR) - 18;
        def += stats.getCurrent(I_DEX) - 17;
        
        return def;
    }
    
    public int calculateTotalDodge() 
    {
        int bonus = 0;
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) bonus += item.getAddDodge();
        }
        
        int dodge = stats.getCurrent(I_DEX) * 2;
        dodge += stats.getCurrent(I_INT);
        dodge -= 55;
        
        dodge = dodge * (100 + bonus) / 100;
        
        return dodge;
    }
    
    /**
     * Calculate total chance to block an attack.
     * @return Value in [0..75] as blocking percent
     */
    public int calculateTotalBlock() 
    {
        int block = sumEquipmentInt(Item.I_BLOCK);
        return Math.min(Math.max(block, 0), 75);
    }

    public Damage calculateTotalDamage() 
    {
        int physicalL = 0;
        int physicalH = 0;
        
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null)
            {
                physicalL += item.calcTotalDamageLow();
                physicalH += item.calcTotalDamageHigh();
            }
        }
        
        physicalL += 1;
        physicalH += stats.getCurrent(I_STR) - 18;
        physicalH += stats.getCurrent(I_DEX) - 18;
        
        return new Damage(this,
                          physicalL, physicalH, 
                          0, 0, 
                          0, 0, 
                          0, 0, 
                          0, 0,
                          0,
                          0);
    }

    public int calculateTotalAttackRating()
    {
        int ar = 50;
        ar += stats.getCurrent(I_STR) - 18;
        ar += stats.getCurrent(I_DEX) - 18;      
        
        return ar;
    }
    
    public int[] calculateTotalResistances() 
    {
        int [] resistences = new int [8];
        int base = calculateBaseRes();
        
        for(int i=0; i<resistences.length; i++)
        {
            resistences[i] = base;
        }
        
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null)
            {
                resistences[0] += item.getResPhysicalPercent();
                resistences[1] += item.getResFirePercent();
                resistences[2] += item.getResIcePercent();
                resistences[3] += item.getResLightPercent();
                resistences[4] += item.getResUndefPercent();
                resistences[5] += item.getResFearPercent();
                resistences[6] += item.getResChaosPercent();
            }
        }

        return resistences;
    }

    public int getItemCountByName(String itemName)
    {
        int n = 0;

        for(InventoryItem item : inventory) 
        {
            if(itemName.equalsIgnoreCase(item.item.getName())) {
                n++;
            }
        }

        return n;
    }

    /**
     * Add item to an empty inventory spot of sufficient size
     * @param inv the item to add
     * @param gridWidth Inventory width in cells
     * @param gridHeight Inventory height in cells
     * 
     * @return true if there was space, false if there was no suitable space
     */
    public boolean addToInventory(InventoryItem inv, int gridWidth, int gridHeight)
    {
        // Hajo: first, try to stack up with an existing item
        for(int gridJ=0; gridJ <= gridHeight-inv.area.height; gridJ++)
        {
            for(int gridI=0; gridI <= gridWidth - inv.area.width; gridI++)
            {
                inv.area.x = gridI;
                inv.area.y = gridJ;

                for(InventoryItem item : inventory)
                {
                    if(item.area.intersects(inv.area))
                    {
                        // Hajo: found a stack? try to add
                        if(item.item.getIdentifiedName().equals(inv.item.getIdentifiedName()) &&
                           item.item.getMaxStackSize() > item.item.getCurrentStackSize() + 1)
                        {
                            int count = item.item.getCurrentStackSize();
                            count += inv.item.getCurrentStackSize();
                            item.item.setCurrentStackSize(count);
                            return true;
                        }
                    }
                }
            }
        }
        
        
        // Hajo: it didn't stack, now look for free space
        for(int gridJ=0; gridJ <= gridHeight-inv.area.height; gridJ++)
        {
            for(int gridI=0; gridI <= gridWidth - inv.area.width; gridI++)
            {
                inv.area.x = gridI;
                inv.area.y = gridJ;

                int intersections = 0;
                
                for(InventoryItem item : inventory)
                {
                    if(item.area.intersects(inv.area))
                    {
                        intersections ++;
                    }
                }

                if(intersections == 0)
                {
                    inventory.add(inv);
                    return true;
                }
            }
        }
        
        return false;
    }

    private class EquipmentVisualizer implements EquipmentListener
    {
        public EquipmentVisualizer() 
        {
        }

        @Override
        public void equipmentChanged(Equipment equipment, int changedSlot) 
        {
            // Weapon
            if(changedSlot == SLOT_FIRST_HAND)
            {
                Object something = equipment.getItem(changedSlot);
                if(something != null)
                {
                    visuals.equipmentOverlays[MobVisuals.OVERLAY_WEAPON] = 385; // Todo: numeric constants
                }
                else
                {
                    visuals.equipmentOverlays[MobVisuals.OVERLAY_WEAPON] = 0;
                }
            }
            // Shield
            if(changedSlot == SLOT_SECOND_HAND)
            {
                Object something = equipment.getItem(changedSlot);
                if(something != null)
                {
                    visuals.equipmentOverlays[MobVisuals.OVERLAY_SHIELD] = 393; // Todo: numeric constants
                }
                else
                {
                    visuals.equipmentOverlays[MobVisuals.OVERLAY_SHIELD] = 0;
                }
            }
        }
      
    }

    public void write(final Writer writer)  throws IOException
    {
        writer.write("Inventory start\n");
        writer.write("v.1\n");
        for(InventoryItem ii : inventory) 
        {
            writeItem(ii.item, writer);
            writer.write("x=" + ii.area.x + "\n");
            writer.write("y=" + ii.area.y + "\n");
            writer.write("w=" + ii.area.width + "\n");
            writer.write("h=" + ii.area.height + "\n");
        }

        writer.write("Inventory end\n");

        writer.write("Equipment start\n");
        writer.write("v.1\n");
        // Equipment <Item> equipment = equipment;

        for(int i=0; i<equipment.slotNames.length; i++) 
        {
            writeItem(equipment.getItem(i), writer);
        }

        writer.write("Equipment end\n");

        writer.write("Player data start\n");
        
        writer.write("name=" + name + "\n");
        writer.write("rkey=" + key + "\n");
        writer.write("xpos=" + location.x + "\n");
        writer.write("ypos=" + location.y + "\n");

        writer.write("spec=" + species + "\n");
        writer.write("ioff=" + iOff + "\n");
        writer.write("joff=" + jOff + "\n");
        writer.write("sped=" + stepsPerSecond + "\n");
        writer.write("Player data end\n");

        writer.write("Path data start\n");
        if(path == null)
        {
            writer.write("pl=<null>\n");            
        }
        else
        {
            writer.write("pl=" + path.length() + "\n");
            for(int i=0; i<path.length(); i++)
            {
                Path.Node node = path.getStep(i);
                writer.write("px=" + node.x + "\n");
                writer.write("py=" + node.y + "\n");
            }
            writer.write("pi=" + path.getCurrentStepIndex() + "\n");
        }
        
        writer.write("Path data end\n");
        
        writer.write("Stats data start\n");
        for(int i=0; i<stats.size(); i++)
        {
            writer.write("min=" + stats.getMin(i) + "\n");
            writer.write("max=" + stats.getMax(i) + "\n");
            writer.write("cur=" + stats.getCurrent(i) + "\n");
        }        
        writer.write("Stats data end\n");
    }

    public void save(File folder) throws IOException
    {        
        boolean ok = folder.exists();
        
        if(!ok)
        {
            folder.mkdirs();
        }
        
        if(ok)
        {
            File file = new File(folder, "player.dat");
            FileWriter writer = new FileWriter(file);
            write(writer);
            writer.close();

            System.err.println("Game saved.\n");
        }
        else
        {
            logger.log(Level.SEVERE, "Could not create directory: {0}", folder);
        }
    }

    private void writeItem(final Item item, final Writer writer)
        throws IOException
    {
        if(item == null) 
        {
            writer.write("<null>\n");
        }
        else 
        {
            writer.write(item.getKey() + "\n");
            item.write(writer);
        }
    }

    public void read(final BufferedReader reader,
                     final ItemCatalog itemCatalog)
                     throws IOException
    {
        String line;

        line = reader.readLine();

        if("Inventory start".equals(line) == false) 
        {
            throw new IOException("Missing: Inventory start, got: " + line);
        }

        line = reader.readLine();

        if("v.1".equals(line) == false) 
        {
            throw new IOException("Missing: v.1 for inventory");
        }

        inventory.clear();
        while("Inventory end".equals(line) == false && line != null) 
        {
            line = reader.readLine();
            if("Inventory end".equals(line) == false) 
            {
                Item item;
                if("<null>".equals(line)) 
                {
                    item = null;
                }
                else 
                {
                    item = new Item(reader, itemCatalog);
                }
                
                InventoryItem ii = new InventoryItem(item);
                
                line = reader.readLine();
                ii.area.x = Integer.parseInt(line.substring(2));
                line = reader.readLine();
                ii.area.y = Integer.parseInt(line.substring(2));
                line = reader.readLine();
                ii.area.width = Integer.parseInt(line.substring(2));
                line = reader.readLine();
                ii.area.height = Integer.parseInt(line.substring(2));
                
                inventory.add(ii);
            }
        }

        line = reader.readLine();

        if("Equipment start".equals(line) == false) 
        {
            throw new IOException("Missing: Equipment start");
        }

        line = reader.readLine();

        if("v.1".equals(line) == false) 
        {
            throw new IOException("Missing: v.1 for equipment");
        }

        int index = 0;
        while("Equipment end".equals(line) == false && line != null) 
        {
            line = reader.readLine();
            if("Equipment end".equals(line) == false) 
            {
                if("<null>".equals(line)) 
                {
                    equipment.setItem(index++, null);
                }
                else 
                {
                    Item item = new Item(reader, itemCatalog);
                    equipment.setItem(index++, item);
                }
            }
        }
        line = reader.readLine();

        if("Player data start".equals(line) == false) 
        {
            throw new IOException("Missing: Player data start");
        }

        line = reader.readLine();
        name = line.substring(5);

        line = reader.readLine();
        key = Integer.parseInt(line.substring(5));
        
        line = reader.readLine();
        location.x = Integer.parseInt(line.substring(5));
        line = reader.readLine();
        location.y = Integer.parseInt(line.substring(5));

        line = reader.readLine();
        species = Integer.parseInt(line.substring(5));
        line = reader.readLine();
        iOff = Integer.parseInt(line.substring(5));
        line = reader.readLine();
        jOff = Integer.parseInt(line.substring(5));
        line = reader.readLine();
        stepsPerSecond = Integer.parseInt(line.substring(5));

        line = reader.readLine();
    
        if("Player data end".equals(line) == false) 
        {
            throw new IOException("Missing: Player data end for mob=" + key);
        }

        line = reader.readLine();
    
        if("Path data start".equals(line) == false) 
        {
            throw new IOException("Missing: Path data start");
        }
        
        line = reader.readLine();
        
        if("pl=<null>".equals(line))
        {
            path = null;
        }
        else
        {
            int pathLength = Integer.parseInt(line.substring(3));
            path = new Path();
            for(int i=0; i<pathLength; i++)
            {
                line = reader.readLine();
                int px = Integer.parseInt(line.substring(3));
                line = reader.readLine();
                int py = Integer.parseInt(line.substring(3));
                path.addStep(px, py);
            }
            line = reader.readLine();
            int pi = Integer.parseInt(line.substring(3));
            for(int i=0; i<pi; i++)
            {
                path.advance();
            }
        }
        
        line = reader.readLine();

        if("Path data end".equals(line) == false) 
        {
            throw new IOException("Missing: Path data end for mob=" + key);
        }
        
        
        line = reader.readLine();
        if("Stats data start".equals(line) == false) 
        {
            throw new IOException("Missing: Stats data start for mob=" + key + " got '" + line + "' instead");
        }
        for(int i=0; i<stats.size(); i++)
        {
            int n;
            line = reader.readLine();
            n = Integer.parseInt(line.substring(4));
            stats.setMin(i, n);
            line = reader.readLine();
            n = Integer.parseInt(line.substring(4));
            stats.setMax(i, n);
            line = reader.readLine();
            n = Integer.parseInt(line.substring(4));
            stats.setCurrent(i, n);
        }        
        line = reader.readLine();
        if("Stats data end".equals(line) == false) 
        {
            throw new IOException("Missing: Stats data end for mob=" + key + " got '" + line + "' instead");
        }
        
        visuals.setDisplayCode(species);
    }
    
    public void load(File folder, final ItemCatalog itemCatalog) throws IOException
    {
        File file = new File(folder, "player.dat");
        FileReader fread = new FileReader(file);
        BufferedReader reader = new BufferedReader(fread);

        read(reader, itemCatalog);
        reader.close();
    }

    private int sumEquipmentInt(int attribute) 
    {
        int sum = 0;
        for(int i=0; i<equipment.slotNames.length; i++)
        {
            Item item = equipment.getItem(i);
            if(item != null) 
            {
                sum += item.getInt(attribute);
                
                // check socketed items
                int sockets = item.getSocketCount();
                
                for(int n = 0; n<sockets; n++)
                {
                    Item socketItem = item.getSocketItem(n);
                    
                    if(socketItem != null)
                    {
                        sum += calculateSocketItemEffect(socketItem, attribute);
                    }
                }                
            }
        }
        
        return sum;
    }
    
    private int calculateSocketItemEffect(Item socketItem, int attribute)
    {
        int result = 0;
        
        // Hajo: todo
        
        return result;
    }
}
