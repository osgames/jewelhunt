//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.particles;

import static jewelhunt.game.particles.ParticleDriver.LIFE;
import static jewelhunt.game.particles.ParticleDriver.MAXLIFE;
import static jewelhunt.game.particles.ParticleDriver.XPOS;
import static jewelhunt.game.particles.ParticleDriver.XSPEED;
import static jewelhunt.game.particles.ParticleDriver.YPOS;
import static jewelhunt.game.particles.ParticleDriver.YSPEED;

/**
 *
 * @author Hj. Malthaner
 */
public class ParticleLinear implements ParticleMovement
{

    @Override
    public boolean drive(int[] particles, int base) 
    {
        // end of life reached?
        if(particles[base + LIFE] > particles[base+MAXLIFE])
        {
            return true;
        }
        else
        {
            particles[base + LIFE] ++;
            particles[base + XPOS] += particles[base + XSPEED];
            particles[base + YPOS] += particles[base + YSPEED];
            return false;
        }
    }
}
