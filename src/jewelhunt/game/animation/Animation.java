//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.animation;

/**
 *
 * @author Hj. Malthaner
 */
public interface Animation 
{
    public boolean isFinished();
    
    public void play();
    
}
