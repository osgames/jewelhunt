//
// This file is part of the Jewelhunt project:
// https://sourceforge.net/projects/jewelhunt/
//

package jewelhunt.game.animation;

import jewelhunt.game.Clock;
import jewelhunt.game.combat.Attack;
import jewelhunt.game.player.Player;

/**
 *
 * @author Hj. Malthaner
 */
public class MeleeAttackAnimation implements Animation
{
    private final long startTime;
    private boolean finished;
    private final Player player;
    private final Attack attack;
    private final Player defender;
    private final int startDirection;
    
    public MeleeAttackAnimation(Player player, Attack attack, Player defender) 
    {
        this.player = player;
        this.attack = attack;
        this.defender = defender;
        
        this.startTime = Clock.time();
        this.finished = false;
        this.startDirection = player.visuals.getDisplayCode() - player.getSpecies();
    }
    
    @Override
    public boolean isFinished()
    {
        return finished;
    }
    
    @Override
    public void play()
    {
        int step = (int)(Clock.time() - startTime) >> 5;
        
        if(step < 8)
        {
            int dir = (startDirection + step) & 7;
            int frame = player.getSpecies() + dir;
            player.visuals.setDisplayCode(frame);
        }
        else
        {
            player.visuals.setDisplayCode(player.getSpecies() + startDirection);
            finished = true;
            attack.hit(defender);
        }
    }
}
