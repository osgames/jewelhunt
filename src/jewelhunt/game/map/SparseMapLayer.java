package jewelhunt.game.map;

import com.frostwire.util.SparseArray;
import java.util.ArrayList;

/**
 *
 * @author Hj. Malthaner
 * @param <E> Type of the objects in this map layer
 */
public class SparseMapLayer <E>
{
    private final ArrayList <SparseArray<E>> rows;

    public SparseMapLayer(int width, int height)
    {
        rows = new ArrayList <SparseArray<E>>(height);
        
        for(int i=0; i<height; i++)
        {
            // Hajo: assuming 1 of 16 elements gets used ...
            rows.add(new SparseArray<E>(width/16));
        }
    }

    public E get(int x, int y)
    {
        SparseArray<E> row = rows.get(y);
        return row.get(x);
    }

    public void remove(int x, int y)
    {
        SparseArray<E> row = rows.get(y);
        row.delete(x);
    }
    
    public void set(int x, int y, E value)
    {
        SparseArray<E> row = rows.get(y);
        row.put(x, value);
    }

    void resize(int width, int height) 
    {
        rows.clear();
        
        for(int i=0; i<height; i++)
        {
            // Hajo: assuming 1 of 16 elements gets used ...
            rows.add(new SparseArray<E>(width/16));
        }
    }
}
