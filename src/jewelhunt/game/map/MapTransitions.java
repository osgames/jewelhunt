package jewelhunt.game.map;

import jewelhunt.game.player.Player;

/**
 *
 * @author Hj. Malthaner
 */
public class MapTransitions
{
    public static enum MapId
    {
        NONE,
        DUNGEON_1,
        VILLAGE_1, 
        WILDERNESS_1,
        TEMPLE,
        PYRAMID,
        PYRAMID_DUNGEON_1
    }
    
    private static MapId currentMap = MapId.VILLAGE_1;
    
    
    public MapId getCurrentMap()
    {
        return currentMap;
    }

    /* feature should be removed in future and only coordinates 
     * should be checked here
     */
    public MapId transition(Player player, int feature, int x, int y)
    {
        if(currentMap == MapId.DUNGEON_1 && feature == Map.FEAT_STAIRS_UP_NARROW)
        {
            currentMap = MapId.VILLAGE_1;
        }
        else if(currentMap == MapId.VILLAGE_1 && feature == Map.FEAT_STAIRS_DOWN_NARROW)
        {
            currentMap = MapId.DUNGEON_1;
        }
        else if(currentMap == MapId.VILLAGE_1 && feature == Map.FEAT_ARROW_EAST)
        {
            // currentMap = MapId.WILDERNESS_1;
            currentMap = MapId.PYRAMID;
        }
        else if(currentMap == MapId.VILLAGE_1 && feature == Map.FEAT_ARROW_WEST)
        {
            currentMap = MapId.TEMPLE;
        }
        else if(currentMap == MapId.TEMPLE && feature == Map.FEAT_ARROW_EAST)
        {
            currentMap = MapId.VILLAGE_1;
        }
        else if(currentMap == MapId.WILDERNESS_1 && feature == Map.FEAT_ARROW_WEST)
        {
            currentMap = MapId.VILLAGE_1;
        }
        else if(currentMap == MapId.PYRAMID)
        {
            if(manhattenDistance(x, y, 122, 10) < 10)
            {
                currentMap = MapId.VILLAGE_1;
            }
            else if(manhattenDistance(x, y, 122, 139) < 10)
            {
                currentMap = MapId.PYRAMID_DUNGEON_1;
            }
        }
        else if(currentMap == MapId.PYRAMID_DUNGEON_1)
        {
            if(feature == Map.FEAT_STAIRS_UP_NARROW)
            {
                currentMap = MapId.PYRAMID;
            }
        }
        
        return currentMap;
    }

    int manhattenDistance(int sx, int sy, int dx, int dy)
    {
        int xd = Math.abs(dx - sx);
        int yd = Math.abs(dy - sy);
        
        return Math.max(xd, yd);
    }
}
