package jewelhunt.game.map;

/**
 *
 * @author Hj. Malthaner
 */
public interface LocationCallback 
{
    public boolean visit(int x, int y);
}
